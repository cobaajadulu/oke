var MZPlus = MZPlus || {};
MZPlus.PrimeParallax = MZPlus.PrimeParallax || {};
//================================================================================
/*
* Anisoft Plugin - MZPlusPrimeParallax
* MZPlusPrimeParallax.js
* 10/7/2020 - 1.0.6 - added an "All" value for reflections. You can now disable all reflections, or pick to disable all
* reflections for just followers or events.
*                   - fixed a known bug with balloon animations not showing when reflections were disabled.
* 9/16/2020 - 1.0.5 - fixed using self and event trigger
*                   - fixed defaults for parallax layers being incorrect such as tint
* 9/13/2020 - 1.0.4 - fixed change PARAMS from const to var
* 9/12/2020 - 1.0.3 - fixed change layer custom/manual image path loading
*                   - fixed offsetY not being able to be negative
* 9/11/2020 - 1.0.2 - fixed regionTiles not updating settings unless the gamecharacter moves
*                   - fixed the -1 defaults for reflection opacity amount and offsetY
*                   - fixed reflections from moving with character when jumping
*                   - added made the fade more noticable
*                   - added regionTile enabling reflection if default is off
*
* 8/25/2020 - 1.0.1 - Fixed a default -1 for region Id and region reflection offsetY
* 8/24/2020 - 1.0.0 - Finished plugin.
*/
//================================================================================
var Imported = Imported || {};
Imported.PrimeParallax = "MZPlusPrimeParallax";
//================================================================================
/*:
* @target MZ
* @plugindesc <MZPlusPrimeParallax> This plugin allows for layered parallax maps and
* infinite fog layers.
* @author Anisoft || Version 1.0.3
* @base MZPlus
* @orderAfter MZPlus
* @help
*
* ================================================================================
* Plugin Parameters:
* ================================================================================
*
* This plugin requires MZPlus
*
* Folder Name: This is the folder to put your parallax images. By default this
* is called 'parallaxMaps'.
*
* Organized Sub Folders: If you wish to have your parallax's seperated out
* into individuals folders by layer name set this to true.
*
* Ex:
*   img/parallaxMaps/reflectionMap
*   img/parallaxMaps/groundMap
*   img/parallaxMaps/parallaxMap
*   img/parallaxMaps/lightMap
*   img/parallaxMaps/shadowMap
*
* Auto Default Layers:
*
* This plugin allows you to config the default layers to show up every map.
* You can select as few or as many layers as you want. If for example you
* only wanted the ground and parallax layer, toggle the reflectionLayer,
* shadowLayer, and lightLayer off.
*
* --------------------------------------------------------------------------------
* Auto Default Layers Settings:
* --------------------------------------------------------------------------------
*
*   Activate Layer: If the default auto layer is on or off.
*   Layer Name: The name for the layer, this is used to determine the
*   organized files folder name too.
*
* --------------------------------------------------------------------------------
* Layer Settings: Each layer consists of multiply properties that you have
* complete control of changing to get the look you want.
* --------------------------------------------------------------------------------
*
*   Image: The image bitmap for the layer. (This isn't settable for the auto
*   layers.)
*
*   ZIndex: The z position of the layer. (depth) The default uses floats (decimals) so
*   not to destrupt the default z layering of objects.
*
*
*   [Default MZ Z]                              [Default Layers Z]
*   0 : Lower tiles                            -3   : Reflection Layer
*   1 : Lower characters                       -1   : Ground Layer
*   3 : Normal characters                       5.1 : Parallax Layer
*   4 : Upper tiles                             5.2 : Shadow Layer
*   5 : Upper characters                        5.6 : Light Layer
*   6 : Airship shadow
*   7 : Balloon
*   8 : Animation
*   9 : Destination
*
*   So for example if you wanted to place a layer above all the characters.
*   Set it to 5.1. Below the tilemap tiles you could do -2.
*
*   BlendMode: The type of blending for the image.
*
*   Normal: No blending
*   ADD: Additively blending, good for lights
*   MULTIPLY: A darkening effect, good for shadows
*   OVERLAY: A alternative way to lighting stuff. (A softer approuch)
*
*   Opacity: The transparency of the layer from 0-255
*
*   Tint: The color tint of the layer. This is in hex form.
*   (0xffffff || #ffffff)
*
*   OffsetX and OffsetY: A offset position for the layer.
*
*   Position Type: If screen the fog will set itself to the size of the window
*   and follow the window. If map the fog will use the settings below it to
*   set its position and size.
*
*       Map Position X: The map y position of the fog in tiles.
*       Map Position Y: The map y position of the fog in tiles.
*       Map Width: The fogs width in pixels.
*       Map Height: The fogs height in pixels.
*
*
*   TilingX and TilingY: The tiling of fog layers. (Does not apply to parallax
*   maps)
*
*   ScrollingX and SCrollingY: The scrolling of fog layers. (Does not apply to
*   parallax maps)
*
*
*
*   Advanced: All parallax properties except for Image are able to be set to a
*   variable/switch To do this set the variable to v[x] (variable) or s[x]
*   (switch) x being the number of the variable/switch you want.
*
*
* --------------------------------------------------------------------------------
* Character Reflections: Global character reflection properties. (true/false)
* Set this to false to disable reflections.
* --------------------------------------------------------------------------------
*
*   Default Reflection Visible: If the reflections are to be visible by
*   default without the need to call the plugin command.
*
*   Reflection Blur: If the reflections should have a blur effect applied
*
*       Quality: The quality of the blur. (Lower can help performance)
*       (LOW, MEDIUM, HIGH, MAX)
*
*   Character Fade: If the reflections should fade out near the bottom
*   (true/false
*
* --------------------------------------------------------------------------------
* Fog Layers Preset Config
* --------------------------------------------------------------------------------
* Set up preset fog configeration that can be called via ID based on their
* position in the list.
*
* If a fog was positioned at the very top of the list, it's ID would be 0
*
* --------------------------------------------------------------------------------
* General Settings
* --------------------------------------------------------------------------------
*
* Disable Auto Shadows: Disable Auto Shadows from displaying on screen.
*
* Region Interaction:  Allows you to set multiple interaction behavior per region such as block/allow/bush/ and reflection settings.
*
*   ID: The ID of which region you want to effect
*
*   Block: Block the character type specified from moving on this tile.
*   (All, Player, Event)
*
*   Allow: Allow the character type specified from moving on this tile.
*   (All, Player, Event)
*
*   Bush: Is this a bush region? (true/false)
*
*   Change Reflection: Settings to effect reflections based on regions.
*   (All, Player, Event)
*
*   Disable Reflection: Disable the reflection? (true/false)
*
*   Opacity Amount: The opacity of the reflection on this tile
*
*   Offset Y: The offset of the reflection in the y position.
*
*
*
*
* ================================================================================
* Plugin Commands:
* ================================================================================
*
* Trigger Type: This is a general command present in all plugin commands. This
* allows you to change the trigger type of command.
*
* Page: Will trigger the plugin command on page change/start.
* EventTrigger: Will trigger as normal on event trigger.
*
* --------------------------------------------------------------------------------
* Toggle Layers:
* --------------------------------------------------------------------------------
*
* This is a helper command that lets u just toggle a bunch of parallax maps.
*
* --------------------------------------------------------------------------------
* Change Layer:
* --------------------------------------------------------------------------------
*
* Parallax Layer: Chose which layer you want to change.
*
* Show Layer: Sets the layer to visible or hidden.
*
* Parallax Settings: Settings to config for this layer. Leaving this blank
* won't change the parallax. You can also leave values blank to leave it as the
* previous/default settings.
*
* --------------------------------------------------------------------------------
* Change Fog
* --------------------------------------------------------------------------------
*
* Fog Index: The index of the fog. Setting this to different values will allow you
* layer different fogs.
*
* For example if you had ID 0 fog, and a ID 1 fog, you can then have two fog
* layers displayed at once.
*
* Show Fog: Sets the fog to visible or hidden.
*
*
* Fog Change Settings: Change the fog settings. You can pick between using
* your a custom FogSettings or using a PresetSettings based on ID config.
*
* FogSettings: Settings to config for this fog. Leaving this blank won't
* change the parallax. You can also leave values blank to leave it as the
* previous/default settings.
*
* PresetSettings: ID of the preset config settings set in the plugin
* parameters Fog Presets Config.
*
* --------------------------------------------------------------------------------
* Reset Layer
* --------------------------------------------------------------------------------
*
* Parallax Layer: Chose which layer you want to change.
*
*
* --------------------------------------------------------------------------------
* Reflection
* --------------------------------------------------------------------------------
*
* Target: The target character to make changes to.
*
* FollowerID: If follower is set as the target, picks which follower to target.
*
* EventID: If event is set as the target, picks which event to target.
*
* Visible: Set the reflection to be visible or not.
*
* Opacity: set the transparency of the reflection
*
* OffsetY: Set the offsetY of the reflection from the player.
*
*
*
*
*
* @param folderName
* @text Folder Name
* @desc Sets the folder for the organized sub folders to go into.
* @default parallaxMaps
*
* @param organizedSubFolders
* @text Organized Sub Folders
* @desc Uses different folders for each layer instead of the default 'parallaxes' This will use the layer names for the folders.
* @type boolean
* @parent folderName
* @default false
*
* @param autoDefaultLayers
* @text Default Layers
* @desc Per Map Layer Settings to be used when toggling layers or when no override change layer is set.
*
* @param reflectionLayer
* @text Reflection Layer
* @desc Reflection Layer Setup properties and settings
* @parent autoDefaultLayers
* @type struct<parallaxSetup>
* @default {"parallaxSetup":"true","layerName":"reflectionMap","layerSettings":"{\"zIndex\":\"-3\",\"blendMode\":\"NORMAL\",\"opacity\":\"255\",\"tint\":\"0xffffff\",\"offsetX\":\"0\",\"offsetY\":\"0\",\"novaLighting\":\"\",\"lightMode\":\"NONE\"}"}
*
* @param groundLayer
* @text Ground Layer
* @desc Ground Layer Setup properties and settings
* @parent autoDefaultLayers
* @type struct<parallaxSetup>
* @default {"parallaxSetup":"true","layerName":"groundMap","layerSettings":"{\"zIndex\":\"-1\",\"blendMode\":\"NORMAL\",\"opacity\":\"255\",\"tint\":\"0xffffff\",\"offsetX\":\"0\",\"offsetY\":\"0\",\"novaLighting\":\"\",\"lightMode\":\"NONE\"}"}
*
* @param parallaxLayer
* @text Parallax Layer
* @desc Parallax Layer Setup properties and settings
* @parent autoDefaultLayers
* @type struct<parallaxSetup>
* @default {"parallaxSetup":"true","layerName":"parallaxMap","layerSettings":"{\"zIndex\":\"5.1\",\"blendMode\":\"NORMAL\",\"opacity\":\"255\",\"tint\":\"0xffffff\",\"offsetX\":\"0\",\"offsetY\":\"0\",\"novaLighting\":\"\",\"lightMode\":\"NONE\"}"}
*
* @param shadowLayer
* @text Shadow Layer
* @desc Shadow Layer Setup properties and settings
* @parent autoDefaultLayers
* @type struct<parallaxSetup>
* @default {"parallaxSetup":"true","layerName":"shadowMap","layerSettings":"{\"zIndex\":\"5.2\",\"blendMode\":\"MULTIPLY\",\"opacity\":\"255\",\"tint\":\"0xffffff\",\"offsetX\":\"0\",\"offsetY\":\"0\",\"novaLighting\":\"\",\"lightMode\":\"NONE\"}"}
*
* @param lightLayer
* @text Light Layer
* @desc Light Layer Setup properties and settings
* @parent autoDefaultLayers
* @type struct<parallaxSetup>
* @default {"parallaxSetup":"true","layerName":"lightMap","layerSettings":"{\"zIndex\":\"5.6\",\"blendMode\":\"ADD\",\"opacity\":\"255\",\"tint\":\"0xffffff\",\"offsetX\":\"0\",\"offsetY\":\"0\",\"novaLighting\":\"\",\"lightMode\":\"NONE\"}"}
*
* @param characterReflections
* @text Character Reflections
* @desc Enables to use Character (Player/Follower/Event) reflections. This disables all reflections stuff period.
* @type boolean
* @default true
*
* @param characterReflectionsVisible
* @text Default Visibility
* @desc Makes reflections visible by default.
* @parent characterReflections
* @type boolean
* @default true
*
* @param characterReflectionBlur
* @text Reflection Blur
* @desc
* @parent characterReflections
* @type boolean
* @default true
*
* @param characterReflectionQuality
* @text Quality
* @desc sets the quality (resolution) of the character reflections for performance. Resolution: LOW: 25%, MEDIUM: 50%, HIGH: 75%, MAX: 100%
* @parent characterReflectionBlur
* @type select
* @default MAX
*
* @option LOW
* @option MEDIUM
* @option HIGH
* @option MAX
*
* @param characterReflectionFade
* @text Character Fade
* @desc
* @parent characterReflections
* @type boolean
* @default true
*
* @param characterReflectionOpacity
* @text Character Opacity
* @desc Global opacity, applied to all the reflections at once.
* @parent characterReflections
* @type number
* @default 255
*
* @param fogLayersConfig
* @text Fog Layers Preset Config
* @desc Preset fog settings you can set for the ChangeFog command. The ID must match the position the item is in this list.
* @type struct<parallaxLayer>[]
* @default
*
* @param generalSettings
* @text General Settings
* @desc General settings not specific to parallaxes.
*
* @param disableAutoShadows
* @text Disable Auto Shadows
* @desc Disables the default MZ auto shadows
* @parent generalSettings
* @type boolean
* @default false
*
*
* @param disableDefaultParallax
* @text Disable Default Parallax
* @desc Disables the default map parallax. Useful if you just want to see the map in editor for eventing.
* @parent generalSettings
* @type boolean
* @default false
*
* @param regionInteraction
* @text Region Interaction
* @desc Allows you to set passability, bush, and reflection settings per region tile.
* @parent generalSettings
* @type struct<region>[]
* @default []
*
*
* @command toggleLayers
* @text Toggle Layers
* @desc Toggles the parallax layer on or off
*
* @arg triggerType
* @text Trigger Type
* @desc Page will trigger on page change. EventTrigger will change on plugin command fire.
* @type select
* @default Page
*
* @option Page
* @option EventTrigger
*
* @arg
* @text
*
* @arg reflectionEnable
* @text Reflection Enable
* @desc Enable the reflection layer
* @type boolean
* @default true
*
* @arg groundEnable
* @text Ground Enable
* @desc Enable the ground layer
* @type boolean
* @default true
*
* @arg parallaxEnable
* @text Parallax Enable
* @desc Enable the parallax layer
* @type boolean
* @default true
*
* @arg shadowEnable
* @text Shadow Enable
* @desc Enable the shadow layer
* @type boolean
* @default true
*
* @arg lightEnable
* @text Light Enable
* @desc Enable the light layer
* @type boolean
* @default true
*
*
* @command changeLayer
* @text Change Layer
* @desc Change the parallax Image
*
* @arg triggerType
* @text Trigger Type
* @desc Page will trigger on page change. EventTrigger will change on plugin command fire.
* @type select
* @default Page
*
* @option Page
* @option EventTrigger
*
* @arg
* @text
*
* @arg parallaxLayer
* @text Parallax Layer
* @type select
*
* @option Reflection
* @value REFLECTION
* @option Ground
* @value GROUND
* @option Parallax
* @value PARALLAX
* @option Light
* @value LIGHT
* @option Shadow
* @value SHADOW
*
* @arg showLayer
* @text Show Layer
* @type boolean
* @default true
*
* @arg parallaxSettings
* @text Parallax Settings
* @desc Settings to override the fog with
* @type struct<parallaxLayer>
* @default
*
*
*
* @command changeFog
* @text Change Fog
* @desc Change the fog Image
*
* @arg triggerType
* @text Trigger Type
* @desc Page will trigger on page change. EventTrigger will change on plugin command fire.
* @type select
* @default Page
*
* @option Page
* @option EventTrigger
*
* @arg
* @text
*
* @arg fogIndex
* @text Fog Index
* @type number
* @default 0
*
* @arg showFog
* @text Show Fog
* @type boolean
* @default true
*
* @arg fogChangeSettings
* @text Fog Change Settings
* @desc Set to either default settings or set fog settings for new.
* @type select
* @default fogSettings
*
* @option FogSettings
* @value fogSettings
*
* @option PresetSettings
* @value presetSettingsID
*
* @arg fogSettings
* @text Fog Settings
* @desc Custom Settings to set the fog to.
* @parent fogChangeSettings
* @type struct<parallaxLayer>
* @default
*
* @arg presetSettingsID
* @text PresetSettingsID
* @desc Preset settings ID to set the fog to from the plugin settings fog config.
* @parent fogChangeSettings
* @type number
* @default 0
*
* @command resetLayer
* @text Reset Layer
* @desc Change parallax layer to defaults
*
* @arg triggerType
* @text Trigger Type
* @desc Page will trigger on page change. EventTrigger will change on plugin command fire.
* @type select
* @default Page
*
* @option Page
* @option EventTrigger
*
* @arg
* @text
*
*
* @arg parallaxLayer
* @text Parallax Layer
* @type select
*
* @option Reflection
* @value REFLECTION
* @option Ground
* @value GROUND
* @option Parallax
* @value PARALLAX
* @option Light
* @value LIGHT
* @option Shadow
* @value SHADOW
*
*
*
* @command reflection
* @text Reflection
* @desc Reflection Settings for each character.
*
* @arg triggerType
* @text Trigger Type
* @desc Page will trigger on page change. EventTrigger will change on plugin command fire.
* @type select
* @default Page
*
* @option Page
* @option EventTrigger
*
* @arg
* @text
*
*
* @arg target
* @text Target
* @desc The character to target for changing reflections
* @type select
* @default self
*
* @option Self
* @value self
*
* @option Player
* @value player
*
* @option Follower
* @value follower
*
* @option Event
* @value event
*
* @option All
* @value all
*
* @arg followerID
* @parent target
* @text FollowerID
* @desc The ID for which follower to use. (Default: 0) (Can also use 'all')
* @type string
* @default 0
*
*
* @arg eventID
* @parent target
* @text EventID
* @desc The ID for which event to use. (Default: 1) (Can also use 'all')
* @type string
* @default 1
*
*
* @arg visible
* @text Visible
* @desc Visibility of the reflection true/false (Default: true)
* @type boolean
* @default true
*
* @arg opacity
* @text Opacity
* @desc Opacity of the reflection. 0-255 (Default: 0)
* @type number
* @default 255
*
* @arg offsetY
* @text OffsetY
* @desc Offset of the reflection. (Default: 0)
* @default 0
*/

/*~struct~parallaxSetup:
*
* @param parallaxSetup
* @text Activate Layer
* @desc Activate the parallax?
* @type boolean
* @default true
*
* @param layerName
* @text Layer Name
* @desc The auto map load filename format for auto loading files on map start. Example: reflectionMap would be reflectionMap_0
* @type string
* @default
*
* @param layerSettings
* @text Settings
* @desc Settings for the reflection layer auto setup.
* @type struct<parallax>
* @default
*
*
*
* */

/*~struct~parallax:
*
* @param zIndex
* @text Z Index
* @desc The layer of the image. Can be negative or positive float. decimal/whole number. (Default is 0)
* @default 0
*
* @param blendMode
* @text BlendMode
* @desc Default blendMode (Default is NORMAL)
* @type select
* @default NORMAL
*
* @option NORMAL
* @option ADD
* @option MULTIPLY
* @option SCREEN
*
* @param opacity
* @text Opacity (Transperency)
* @desc The Opacity (Transperency) of the layer. 0 - 255 (Default is 0)
* @type number
* @default 255
*
* @param tint
* @text Tint (Color)
* @desc The Tint (Color) of the layer in hex 0xffffff or #ffffff. (Default is 0xffffff) (white = no tint)
* @type string
* @default 0xffffff
*
* @param offsetX
* @text OffsetX
* @desc Default offset x of the plugin in pixels. (Default is 0)
* @default 0
*
* @param offsetY
* @text OffsetY
* @desc Default offset y of the plugin in pixels. (Default is 0)
* @default 0
*
* @param novaLighting
* @test Nova Lighting
*
* @param lightMode
* @text Light Mode
* @desc Sets the parallax layer to a light layer for the Nova Lighting System. (Requires Nova Lighting)
* @parent novaLighting
* @type select
* @default NONE
*
* @option NONE
* @value NONE
* @option SHADOW
* @value SHADOW
* @option LIGHT
* @value LIGHT
* */

/*~struct~parallaxLayer:
*
* @param imagePath
* @text Image
* @desc The image to set the parallax to.
* @type file
* @dir img
*
* @param zIndex
* @text Z Index
* @desc The layer of the image. Can be negative or positive float. decimal/whole number. (Default is 0)
*
* @param blendMode
* @text BlendMode
* @desc Default blendMode (Default is NORMAL)
* @type select
*
* @option NORMAL
* @option ADD
* @option MULTIPLY
* @option OVERLAY
*
* @param opacity
* @text Opacity (Transperency)
* @desc The Opacity (Transperency) of the layer. 0 - 255
* @type number
*
* @param tint
* @text Tint (Color)
* @desc The Tint (Color) of the layer in hex 0xffffff or #ffffff. (Default is 0xffffff) (white = no tint)
*
*
*
* @param offsetX
* @text OffsetX
* @desc Default offset x of the plugin in pixels. (Default is 0)
*
* @param offsetY
* @text OffsetY
* @desc Default offset y of the plugin in pixels. (Default is 0)
*
* @param fogProperties
* @text Fog Properties
* @desc Properties specific to fog and its tiling sprite settings.
*
* @param positionType
* @text Position Type
* @desc If screen, the fog will follow the camera and set its size to the display (normal behavior).
* If map, the fog will bind to the map position and set its height based on values.
* @parent fogProperties
* @type select
* @default screen
*
* @option Screen
* @value screen
* @option Map
* @value map
*
* @param mapX
* @text Map Position X
* @desc Map Position X in tiles. You can use floats (decimals) to set positions in between tiles. (Default: 0)
* @parent positionType
* @default 0
*
* @param mapY
* @text Map Position Y
* @desc Map Position Y in tiles. You can use floats (decimals) to set positions in between tiles. (Default: 0)
* @parent positionType
* @default 0
*
* @param width
* @text Map Width
* @desc Width of the fog layer in pixels.  (Default: 500)
* @parent positionType
* @default 500
*
* @param height
* @text Map Height
* @desc Height of the fog layer in pixels. (Default: 500)
* @parent positionType
* @default 500
*
* @param tilingX
* @text TilingScaleX
* @desc Tiling Scale X of the parallax. Only applies to fog layers. (Default is 1)
* @parent fogProperties
*
* @param tilingY
* @text TilingScaleY
* @desc Tiling Scale X of the parallax. Only applies to fog layers. (Default is 1)
* @parent fogProperties
*
* @param scrollingX
* @text ScrollingX
* @desc Scrolling speed X of the parallax (useful for fog) number/decimal. Only applies to fog layers. (Default is 0)
* @parent fogProperties
*
* @param scrollingY
* @text ScrollingY
* @desc Scrolling speed Y of the parallax (useful for fog) number/decimal. Only applies to fog layers. (Default is 0)
* @parent fogProperties
*
*
* @param novaLighting
* @test Nova Lighting
*
* @param lightMode
* @text Light Mode
* @desc Sets the parallax layer to a light layer for the Nova Lighting System. (Requires Nova Lighting)
* @parent novaLighting
* @type select
* @default NONE
*
* @option NONE
* @value LIGHT
* @option SHADOW
* @value LIGHT
* @option LIGHT
* @value LIGHT
**/

/*~struct~region:
*
* @param id
* @text Region ID
* @desc ID of the region tile.
*
* @param passability
* @text Passability
* @desc Parameters to determine the passability of the region.
*
* @param block
* @text Block
* @desc Block the character type provided. (All, Player, Event)
* @parent passability
* @type select
* @default none
*
* @option None
* @value none
* @option All
* @value all
* @option Player
* @value player
* @option Event
* @value event
*
*
* @param allow
* @text Allow
* @desc Allow the character type provided. (All, Player, Event)
* @parent passability
* @type select
* @default none
*
* @option None
* @value none
* @option All
* @value all
* @option Player
* @value player
* @option Event
* @value event
*
* @param bush
* @text Bush
* @desc Makes character in bush.
* @type boolean
* @default false
*
* @param reflection
* @text Change Reflection
* @desc Parameters to determine the reflection of the region.
* @type select
* @default none
*
* @option None
* @value none
* @option All
* @value all
* @option Player
* @value player
* @option Event
* @value event
*
* @param disableReflection
* @text Disable Reflection
* @desc Disables the Reflection for specified game object
* @parent reflection
* @type boolean
* @default

*
* @param opacityAmount
* @text Opacity Amount
* @desc Changes the opacity (Default: -1 means disabled)
* @parent reflection
* @default -1
*
* @param offsetY
* @text Offset Y Amount
* @desc Offset the reflection in the Y direction. (Default: -1 means disabled)
* @parent reflection
* @default -1
*
*/
//================================================================================
var PARAMS = MZPlus.PrimeParallax = MZPlus.getParams('<MZPlusPrimeParallax>', {
    organizedSubFolders: false,
    characterReflections: true,
    characterReflectionsVisible: true,
    characterReflectionBlur: true,
    characterReflectionQuality: 'MAX',
    characterReflectionFade: true,
    characterReflectionOpacity: 255,
    fogLayersConfig: [],
    disableAutoShadows: false,
    disableDefaultParallax: false,
    regionInteraction: []
});

($=> {

    window.PARALLAX_LAYERS = $.PARALLAX_LAYERS = {
        REFLECTION: "reflection",
        GROUND: "ground",
        PARALLAX: "parallax",
        SHADOW: "shadow",
        LIGHT: "light"
    };

    window.PARALLAX_TYPE = $.PARALLAX_TYPE = {
        SPRITE: "sprite",
        TILINGSPRITE: "tilingsprite"
    };

    window.SCREEN_TYPE = $.SCREEN_TYPE = {
        SCREEN: "screen",
        MAP: "map"
    };

    window.LIGHT_MODE = $.LIGHT_MODE = {
        NONE: 0,
        SHADOW: 1,
        LIGHT: 2
    };

    window.BLUR_QUALITY = $.BLUR_QUALITY = {
        LOW: 1,
        MEDIUM: 2,
        HIGH: 3,
        MAX: 4
    };

    window.CHARACTER_TYPE = $.CHARACTER_TYPE = {
        NONE: "",
        SELF: "self",
        ALL: "all",
        PLAYER: "player",
        FOLLOWER: "follower",
        EVENT: "event"
    };

})(MZPlus.PrimeParallax);

//=============================================================================
// Custom Classes
//=============================================================================
//-----------------------------------------------------------------------------
// Game_Parallax
($ => {
    class Game_Parallax {

        get id() {
            return this._id;
        }

        get z() {
            return this._z;
        }

        get imageName() {
            return this._imageName;
        }

        get imageFolder() {
            return this._imageFolder;
        }

        get blendMode() {
            return this._blendMode;
        }

        get type() {
            return this._type;
        }

        get scroll() {
            return this._scroll;
        }

        get offset() {
            return this._offset;
        }

        get opacity() {
            return this._opacity;
        }

        get tiling() {
            return this._tiling;
        }

        get tint() {
            return this._tint;
        }

        get visible() {
            return this._visible;
        }

        set visible(value) {
            this._visible = value;
            this.refresh();
        }

        get positionType() {
            return this._positionType;
        }

        get mapRect() {
            return this._mapRect;
        }

        get lightMode() {
            return this._lightMode;
        }

        set lightMode(value) {
            this._lightMode = value;
            this.refresh();
        }


        constructor(data) {
            this.initialize(data);
        }

        initialize(data) {
            this._data = data;
            this._id = "";
            this._imageFolder = "";
            this._imageName = "";
            this._offset = new PIXI.Point();
            this._zero = new PIXI.Point();
            this._scroll = new PIXI.Point();
            this._scrollAmount = new PIXI.Point();
            this._tiling = new PIXI.Point(1, 1);
            this._blendMode = PIXI.BLEND_MODES.NORMAL;
            this._positionType = SCREEN_TYPE.SCREEN; //TODO make this an enum?
            this._lightMode = 0;
            this._mapRect = new PIXI.Rectangle(0, 0, 500, 500);
            this._visible = true;
            this._tint = 0xffffff;
            this._opacity = 255;
            this._z = 3;
            this._type = PARALLAX_TYPE.SPRITE;
            this.setData(data);
        }

        setData(data) {
            this._data = data;
            this._id = data.id;
            this._type = data.type;
            if (data.imageFolder !== "") this._imageFolder = data.imageFolder;
            if (data.imageName !== "") this._imageName = data.imageName;
            if (data.lightMode !== undefined) this._lightMode = LIGHT_MODE[data.lightMode];
            this.refresh();
        }

        update() {
            const data = this._data;


            if (this.hasValue(data.visible)) this._visible = this.convertValue(data.visible);
            if (!this._visible) return;
            if (this.hasValue(data.blendMode)) this._blendMode = this.convertValue(PIXI.BLEND_MODES[data.blendMode]);
            if (this.hasValue(data.tint)) this._tint = this.convertValue(data.tint);
            if (this.hasValue(data.opacity)) this._opacity = this.convertValue(data.opacity);
            if (this.hasValue(data.zIndex)) this._z = this.convertValue(data.zIndex);
            if (this.hasValue(data.offsetX)) this._offset.x = this.convertValue(data.offsetX);
            if (this.hasValue(data.offsetY)) this._offset.y = this.convertValue(data.offsetY);
            if (this.hasValue(data.positionType)) this._positionType = this.convertValue(data.positionType)
            if (this.hasValue(data.mapX)) this._mapRect.x = this.convertValue(data.mapX);
            if (this.hasValue(data.mapY)) this._mapRect.y = this.convertValue(data.mapY);
            if (this.hasValue(data.width)) this._mapRect.width = this.convertValue(data.width);
            if (this.hasValue(data.height)) this._mapRect.height = this.convertValue(data.height);


            if (this._type === PARALLAX_TYPE.TILINGSPRITE) {
                if (this.hasValue(data.tilingX)) this._tiling.x = this.convertValue(data.tilingX);
                if (this.hasValue(data.tilingY)) this._tiling.y = this.convertValue(data.tilingY);
                if (this.hasValue(data.scrollingX)) this._scrollAmount.x = this.convertValue(data.scrollingX);
                if (this.hasValue(data.scrollingY)) this._scrollAmount.y = this.convertValue(data.scrollingY);

                if (!this._scrollAmount.equals(this._zero)) {
                    this._scroll.x += this._scrollAmount.x;
                    this._scroll.y += this._scrollAmount.y;
                }
            }

            const tilemap = $gameMap.getTilemap();
            if (!tilemap) return;
            const tilemapSprite = tilemap.getParallaxLayer(this);
            if (!tilemapSprite) return;
            tilemapSprite.updateData();
        }

        refresh() {
            const tilemap = $gameMap.getTilemap();
            if (!tilemap) return;
            const tilemapSprite = tilemap.getParallaxLayer(this);
            if (!tilemapSprite) return;
            tilemapSprite.refresh();
        }


        hasValue(value) {
            return (value !== "" && value !== void 0);
        }

        convertValue(value) {
            if (isNaN(value) && typeof value === 'object') {
                if (value.variable) return $gameVariables.value(value.variable);
                if (value.switch) return $gameSwitches.value(value.variable);
            } else {
                return value;
            }


        }
    }

    $.Game_Parallax = Game_Parallax;

})(MZPlus.PrimeParallax);


//-----------------------------------------------------------------------------
// Sprite_ParallaxMap
($ => {
    class Sprite_Parallax extends Sprite {
        initialize(data) {
            const bitmap = ImageManager.loadBitmap(data.imageFolder, data.imageName)
            super.initialize(bitmap);
            this._data = data;
            bitmap.addLoadListener(() => this.refresh())
            this.updateData()
        }

        refresh() {
            const data = this._data;
            const bitmap = ImageManager.loadBitmap(data.imageFolder, data.imageName);
            if (this.bitmap !== bitmap) {
                this.bitmap = bitmap;
            }

            this.parentSet();

        }

        parentSet() {
            const tilemap = $gameMap.getTilemap();
            if (!tilemap) return;
            if (this.parent !== tilemap) {
                 tilemap.addChild(this);
            }
        }

        updateData() {
            const data = this._data;

            this.visible = data.visible;

            if (!this.visible) return;
            this.x = data.offset.x - $gameMap.displayX() * $gameMap.tileWidth();
            this.y = data.offset.y - $gameMap.displayY() * $gameMap.tileHeight();

            this.z = data.z;

            this.blendMode = data.blendMode;
            this.opacity = data.opacity;
            this.tint = data.tint;
        }
    }

    $.Sprite_Parallax = Sprite_Parallax;

})(MZPlus.PrimeParallax);

//-----------------------------------------------------------------------------
// Sprite_ParallaxMap
($ => {
    class TilingSprite_Parallax extends TilingSprite {
        initialize(data) {
            const bitmap = ImageManager.loadBitmap(data.imageFolder, data.imageName)
            super.initialize(bitmap);
            this._data = data;
            this.width = Graphics.width;
            this.height = Graphics.height;
            bitmap.addLoadListener(() => this.refresh());
            this.updateData()

        }

        refresh() {
            const data = this._data;
            const bitmap = ImageManager.loadBitmap(data.imageFolder, data.imageName);
            if (this.bitmap !== bitmap) {
                this.bitmap = bitmap;
            }

            this.parentSet();

        }

        parentSet() {
                const tilemap = $gameMap.getTilemap();
                if (!tilemap) return;
                if (this.parent !== tilemap) {
                    return tilemap.addChild(this);
                }
        }

        updateData() {
            const data = this._data;
            this.visible = data.visible;
            if (!this.visible) return;
            if (data.positionType === SCREEN_TYPE.SCREEN) {
                this.origin.x = data.offset.x + $gameMap.displayX() * $gameMap.tileWidth() + data.scroll.x;
                this.origin.y = data.offset.y + $gameMap.displayY() * $gameMap.tileHeight() + data.scroll.y;
            } else {
                const mapRect = data.mapRect;

                if (data.lightMode > 0) {
                    this.x = mapRect.x* $gameMap.tileWidth();
                    this.y = mapRect.y* $gameMap.tileHeight();
                } else {
                    this.x = (mapRect.x - $gameMap.displayX()) * $gameMap.tileWidth();
                    this.y = (mapRect.y - $gameMap.displayY()) * $gameMap.tileHeight();
                }
                this.width = mapRect.width;
                this.height = mapRect.height;
                this.origin.x = (data.offset.x + data.scroll.x);
                this.origin.y = (data.offset.y + data.scroll.y);
            }


            this.z = data.z;
            this.blendMode = data.blendMode;
            this.opacity = data.opacity;
            this.tileScale.copyFrom(data.tiling);
            this.tint = data.tint;
        }

        updateTransform() {
            this.tilePosition.x = -this.origin.x;
            this.tilePosition.y = -this.origin.y;
            PIXI.TilingSprite.prototype.updateTransform.call(this);
        };
    }

    $.TilingSprite_Parallax = TilingSprite_Parallax;

})(MZPlus.PrimeParallax);

//-----------------------------------------------------------------------------
// Sprite_Reflections
($ => {
    const defaultVertex = `
        precision highp float;attribute vec2 aVertexPosition;
        attribute vec2 aTextureCoord;
        attribute vec4 aColor;
        attribute float aRef;
        attribute float aTextureId;
        
        uniform mat3 projectionMatrix;
        uniform mat3 translationMatrix;
        uniform vec4 tint;
        
        varying vec2 vTextureCoord;
        varying vec4 vColor;
        varying float vRef;
        varying float vTextureId;
        
        void main(void){
            gl_Position = vec4((projectionMatrix * translationMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);
        
            vTextureCoord = aTextureCoord;
            vTextureId = aTextureId;
            vColor = aColor * tint;
            vRef = aRef;

}
    `;

    const defaultFragment = `
        varying vec2 vTextureCoord;
        varying vec4 vColor;
        varying float vRef;
        varying float vTextureId;
        uniform sampler2D uSamplers[%count%];
        void main(void){
                    vec4 color;
                    %forloop%
                        gl_FragColor = color * vColor;
                        gl_FragColor *= vRef*vRef;
                        //gl_FragColor = vec4(vRef,vRef,vRef,1.);
                     }
                    
    
    `;

    class FadeBatchGeometry extends PIXI.Geometry {

        constructor(_static = false) {
            super();

            /**
             * Buffer used for position, color, texture IDs
             *
             * @member {PIXI.Buffer}
             * @protected
             */
            this._buffer = new PIXI.Buffer(null, _static, false);

            /**
             * Index buffer data
             *
             * @member {PIXI.Buffer}
             * @protected
             */
            this._indexBuffer = new PIXI.Buffer(null, _static, true);

            this.addAttribute('aVertexPosition', this._buffer, 2, false, PIXI.TYPES.FLOAT)
                .addAttribute('aTextureCoord', this._buffer, 2, false, PIXI.TYPES.FLOAT)
                .addAttribute('aColor', this._buffer, 4, true, PIXI.TYPES.UNSIGNED_BYTE)
                .addAttribute('aRef', this._buffer, 1, true, PIXI.TYPES.FLOAT)
                .addAttribute('aTextureId', this._buffer, 1, true, PIXI.TYPES.FLOAT)
                .addIndex(this._indexBuffer);
        }
    }

    class BatchFadePluginFactory {

        static create(options) {
            const {vertex, fragment, vertexSize, geometryClass} = (Object).assign({
                vertex: defaultVertex,
                fragment: defaultFragment,
                geometryClass: FadeBatchGeometry,
                vertexSize: 7,
            }, options);

            return class BatchFadePlugin extends PIXI.AbstractBatchRenderer {
                constructor(renderer) {
                    super(renderer);

                    this.shaderGenerator = new PIXI.BatchShaderGenerator(vertex, fragment);
                    this.geometryClass = geometryClass;
                    this.vertexSize = vertexSize;
                }

                packInterleavedGeometry(element, attributeBuffer, indexBuffer, aIndex, iIndex) {
                    const uint32View = attributeBuffer.uint32View;
                    const float32View = attributeBuffer.float32View;

                    const packedVertices = aIndex / this.vertexSize;
                    const uvs = element.uvs;
                    const indicies = element.indices;
                    const vertexData = element.vertexData;
                    const textureId = element._texture.baseTexture._batchLocation;

                    const alpha = Math.min(element.worldAlpha, 1.0);
                    const argb = (alpha < 1.0
                        && element._texture.baseTexture.alphaMode)
                        ? PIXI.utils.premultiplyTint(element._tintRGB, alpha)
                        : element._tintRGB + (alpha * 255 << 24);


                    // lets not worry about tint! for now..
                    for (let i = 0; i < vertexData.length; i += 2) {
                        float32View[aIndex++] = vertexData[i];
                        float32View[aIndex++] = vertexData[i + 1];
                        float32View[aIndex++] = uvs[i];
                        float32View[aIndex++] = uvs[i + 1];
                        uint32View[aIndex++] = argb;
                        if (i === 0 || i === 2)
                            float32View[aIndex++] = 0;
                        else
                            float32View[aIndex++] = 1;

                        float32View[aIndex++] = textureId;
                    }

                    for (let i$1 = 0; i$1 < indicies.length; i$1++) {
                        indexBuffer[iIndex++] = packedVertices + indicies[i$1];
                    }
                };
            }
        }
    }

    PIXI.Renderer.registerPlugin('batchFade', BatchFadePluginFactory.create({}));

    /*
    *     */
})(MZPlus.PrimeParallax);

//-----------------------------------------------------------------------------
// Sprite_Reflections
($ => {
    class Sprite_Reflections extends Sprite_Character {
        initialize(character) {
            super.initialize(character);
            this.scale.y = -1;
            if ($.characterReflectionFade)
                this.pluginName = 'batchFade';

            this._opacityScale = character.reflectionOpacity / 255
        }

        updateOther() {
            this._opacityScale = this._character.reflectionOpacity / 255;
            this.opacity = this._character.opacity() * this._opacityScale;
            this.blendMode = this._character.blendMode();
            this._bushDepth = this._character.bushDepth();
        };

        updateVisibility() {
            const reflectionVisible = this._character.reflectionVisible;
            this.visible = reflectionVisible;
            if (this.visible)
                super.updateVisibility();
        };


        createHalfBodySprites() {
        /*    super.createHalfBodySprites();
            if (this._upperBody)
                this._upperBody.pluginName = "batchFade";
            if (this._lowerBody)
                this._lowerBody.pluginName = "batchFade";*/
        };


        updateCharacterFrame() {
            const pw = this.patternWidth();
            const ph = this.patternHeight();
            const sx = (this.characterBlockX() + this.characterPatternX()) * pw;
            const sy = (this.characterBlockY() + this.characterPatternY()) * ph;
            this.setFrame(sx, sy, pw, ph);
        };


        updatePosition() {
            super.updatePosition()
            this.y += this._character.reflectionOffsetY;
            this.y += this._character.jumpHeight();
        };

    }

    $.Sprite_Reflections = Sprite_Reflections;

})(MZPlus.PrimeParallax);


($ => {
    $.registerCommand(Imported.PrimeParallax, 'toggleLayers', function (args) {
        args = MZPlus.argsToData(args);
        $gameMap.toggleLayer(PARALLAX_LAYERS.REFLECTION, args.reflectionEnable);
        $gameMap.toggleLayer(PARALLAX_LAYERS.GROUND, args.groundEnable);
        $gameMap.toggleLayer(PARALLAX_LAYERS.PARALLAX, args.parallaxEnable);
        $gameMap.toggleLayer(PARALLAX_LAYERS.SHADOW, args.shadowEnable);
        $gameMap.toggleLayer(PARALLAX_LAYERS.LIGHT, args.lightEnable);

    });

    $.registerCommand(Imported.PrimeParallax, 'changeLayer', function (args) {
        args = MZPlus.argsToData(args);
        const id = PARALLAX_LAYERS[args.parallaxLayer]
        let layer = $gameMap.getLayer(id);

        const layerData = args.parallaxSettings;
        if (layerData) {
            layerData.id = id;
            layer = $gameMap.addLayer({layerData: layerData});
        }

        if (layer) layer.visible = args.showLayer;
    });

    $.registerCommand(Imported.PrimeParallax, 'resetLayer', function (args) {
        args = MZPlus.argsToData(args);
        const parallaxLayer = args.parallaxLayer;
        if (!parallaxLayer) return;
        console.log(parallaxLayer)
        $gameMap.addLayer({layerDefault: PARALLAX_LAYERS[parallaxLayer]});
    });

    $.registerCommand(Imported.PrimeParallax, 'changeFog', function (args) {
        args = MZPlus.argsToData(args);
        let layer = $gameMap.getLayer(args.fogIndex);
        let layerData;
        if (args.fogChangeSettings === "fogSettings") {
            layerData = args.fogSettings;
        } else {
            if (isNaN(args.presetSettingsID)) return;
            layerData = PARAMS.fogLayersConfig[args.presetSettingsID];
        }
        if (layerData) {
            layerData.id = args.fogIndex;
            layerData.type = PARALLAX_TYPE.TILINGSPRITE;
            layer = $gameMap.addLayer({layerData: layerData});
        }

        if (layer) layer.visible = args.showFog;

    });


    $.registerCommand(Imported.PrimeParallax, 'reflection', function (args) {
        args = MZPlus.argsToData(args);

        const followerID = typeof args.followerID === 'string' ? args.followerID.toLowerCase() : args.followerID;
        const eventID = typeof args.eventID === 'string' ? args.eventID.toLowerCase() : args.eventID;

        let targets = [];

        switch (args.target) {
            case CHARACTER_TYPE.SELF:
                if (this.constructor === Game_Interpreter) {
                    targets.push(this.character());
                } else {
                    targets.push(this);
                }

                break;
            case CHARACTER_TYPE.PLAYER:
                targets.push($gamePlayer);
                break;
            case CHARACTER_TYPE.FOLLOWER:
                const followers = $gamePlayer.followers();
                if (!followers || !followerID) return;
                if (followerID === CHARACTER_TYPE.ALL) {
                    targets.push(...followers.data());

                } else {
                    const id = Number(followerID);
                    if (!isNaN(id) && id > 0)
                        targets.push(followers.follower(followerID));
                }
                break;
            case CHARACTER_TYPE.EVENT:
                if (eventID === CHARACTER_TYPE.ALL) {
                    targets.push(...$gameMap.events());
                }
                else {
                    const id = Number(eventID);
                    if (!isNaN(id) && id > 0)
                        targets.push($gameMap.event(id));

                }
                break;
            case CHARACTER_TYPE.ALL: {
                targets = [$gamePlayer, ...$gameMap.events(), ...$gamePlayer.followers().data()];
            }
        }

        if (targets) {
            for (let i = 0, len = targets.length; i < len; ++i) {
                const target = targets[i];
                console.log(targets, target, i);
                target.reflectionVisible = args.visible;
                target.reflectionOpacity = args.opacity;
                target.reflectionOffsetY = args.offsetY;
                target.updateMove();
            }

        }

    });

})(PluginManager);


//=============================================================================
// Aliased Classes
//=============================================================================
//-----------------------------------------------------------------------------
// DataManager
($ => {
    const Alias_extractSaveContents = $.extractSaveContents;
    $.extractSaveContents = function (contents) {
        Alias_extractSaveContents.call(this, contents);
        //Make sure loaded files get appropriate parallax'
        for (const p in $gameMap.parallaxMaps) {
            Object.setPrototypeOf($gameMap.parallaxMaps[p], MZPlus.PrimeParallax.Game_Parallax.prototype);
        }
    }
})(DataManager);


//-----------------------------------------------------------------------------
// Game_Map
($ => {
    const Alias_setup = $.setup;
    $.setup = function () {
        this._parallaxMaps = {};
        Alias_setup.call(this, ...arguments);
    };

    $.getLayer = function (layerType) {

        return this.parallaxMaps[layerType];
    };

    $.getDefaultLayerData = function (layerType) {
        const layer = PARAMS[`${layerType}Layer`];
        if (!layer || !layer.parallaxSetup) return;
        const data = layer.layerSettings;
        data.id = layerType;
        return data;
    };

    $.getData = function (args) {
        let {layerDefault, layerData} = args;
        if (layerData) {
            const imagePath = layerData.imagePath;
            if (imagePath) {
                const split = imagePath.split('/');
                if (split.length > 1) {
                    layerData.imageName = split.pop();
                    layerData.imageFolder = `img/${split.join('/')}/`;
                }
            }
        } else {
            layerData = this.getDefaultLayerData(layerDefault);
            let path = `img/${PARAMS.folderName}/`;
            const layer = PARAMS[`${layerDefault}Layer`];
            layerData.imageFolder = (PARAMS.organizedSubFolders) ? `${path}${layer.layerName}/` : path;
            layerData.imageName = `${layer.layerName}_${this.mapId()}`;
        }

        return layerData;

    };

    $.addLayer = function (args) {
        const data = this.getData(args);
        if (!data) return;
        let layer = this.parallaxMaps[data.id];
        if (layer) {
            layer.setData(data);
        } else {
            layer = this.parallaxMaps[data.id] = new MZPlus.PrimeParallax.Game_Parallax(data);
        }
        const tilemap = this.getTilemap();
        if (tilemap) tilemap.addParallaxMap(layer);
        return layer;
    };

    $.getTilemap = function () {
        const scene = SceneManager._scene;
        if (!scene) return;
        const spriteset = scene._spriteset;
        if (!spriteset) return;
        const tilemap = spriteset._tilemap;
        return tilemap;
    };

    const Alias_update = $.update;
    $.update = function () {
        Alias_update.call(this, ...arguments);
        for (const i in this.parallaxMaps) {
            this.parallaxMaps[i].update();
        }
    };


    $.toggleLayer = function (layerType, enable) {
        let obj = this.getLayer(layerType);
        if (enable === true && !obj) {
            obj = this.addLayer({layerDefault: layerType});
        }
        if (obj) {
            obj.visible = enable;
        }
    };

    $.getPassabilityRegion = function(id) {
        const regions = PARAMS.regionInteraction;
        for (let i = 0; i <regions.length; ++i) {
            const region = regions[i];
            if (region.id === id) {
                return region;
            }
        }
    };

    const Alias_isBush = $.isBush;

    $.isBush = function (x, y) {
        const isBush = Alias_isBush.call(this, x, y);
        if (isBush) return isBush;
       // const collisionRegion = PARAMS.collisionRegion
      if ( this.isValid(x, y) ) {
          const passabilityRegion = this.getPassabilityRegion($gameMap.regionId(x, y) );
          if (passabilityRegion) {
              if (passabilityRegion.bush === true) {
                  return true;
              }
          }
      }

    };
    Object.defineProperties($, {
        parallaxMaps: {
            get: function () {
                return this._parallaxMaps;
            }
        }
    })

})(Game_Map.prototype);

//-----------------------------------------------------------------------------
// Game_CharacterBase
($ => {
    const Alias_initMembers = $.initMembers;
    $.initMembers = function () {
        Alias_initMembers.call(this);
        this._reflectionVisible = PARAMS.characterReflectionsVisible;
        this._reflectionOpacity = 255;
        this._reflectionOffsetY = 0;

        this._currentReflectionVisible = this._reflectionVisible;
        this._currentReflectionOpacity = this._reflectionOpacity;
        this._currentReflectionOffsetY = this._reflectionOffsetY;

    };

    $.characterType = function() {
        return void 0;
    };
    const Alias_isMapPassable = $.isMapPassable;
    $.isMapPassable = function (x, y, d) {

        const regionId = this.getRegionId(x, y, d);


        if (regionId > 0) {
            if (this.isThrough()) return false;
            const passabilityRegion = $gameMap.getPassabilityRegion(regionId);
            if (passabilityRegion) {
                if (this.regionIsType(passabilityRegion.block))
                    return false;
                if (this.regionIsType(passabilityRegion.allow)) {
                    return true;
                }

            }
        }


        return Alias_isMapPassable.call(this, ...arguments);
    };

    $.regionIsType = function(value) {
        return value === CHARACTER_TYPE.ALL || value === this.characterType()
    }




    $.getRegionId = function (x, y, d) {
        switch (d) {
            case 1:
                x++;
                y--;
                break;
            case 2:
                y++;
                break;
            case 3:
                x++;
                y++;
                break;
            case 4:
                x--;
                break;
            case 6:
                x++;
                break;
            case 7:
                x--;
                y--;
                break;
            case 8:
                y--;
                break;
            case 9:
                x++;
                y--;
                break;
            default:
                break;
        }

        return $gameMap.regionId(x, y);

    };

    const Alias_updateMove = $.updateMove;
    $.updateMove = function() {
        Alias_updateMove.call(this);
        if (!this.isMoving()) {
            this.refreshReflections();
        }
    };

    const Alias_locate = $.locate;
    $.locate = function() {
        Alias_locate.call(this, ...arguments);
        this.refreshReflections();
    };

    $.refreshReflections = function() {
        const regionId = this.getRegionId(this._x, this._y);
        const passabilityRegion = $gameMap.getPassabilityRegion(regionId);
        this._currentReflectionVisible = this._reflectionVisible;
        this._currentReflectionOpacity = this._reflectionOpacity;
        this._currentReflectionOffsetY = this._reflectionOffsetY;
        if (passabilityRegion) {
            const reflection = this.regionIsType(passabilityRegion.reflection);
            if (reflection) {
                const disableReflection = passabilityRegion.disableReflection;
                //if (disableReflection) {
                    this._currentReflectionVisible = !disableReflection;
                    if (this === $gamePlayer) {
                        console.log(passabilityRegion)
                    }
                //}
                const opacityAmount = passabilityRegion.opacityAmount;

                if (opacityAmount !== -1) {
                    this._currentReflectionOpacity = opacityAmount;
                }

                const offsetY = passabilityRegion.offsetY;

                if (offsetY !== -1) {
                    this._currentReflectionOffsetY = offsetY;
                }
            }
        }


    };


    Object.defineProperties($, {
        reflectionVisible: {
            get: function () {
                return this._currentReflectionVisible;
            },

            set: function (value) {
                this._reflectionVisible = value;
            }
        },
        reflectionOpacity: {
            get: function () {
                return this._currentReflectionOpacity;
            },
            set: function (value) {
                this._reflectionOpacity = value
            }
        },
        reflectionOffsetY: {
            get: function() {
                return this._currentReflectionOffsetY;
            },
            set: function(value) {
                this._reflectionOffsetY = value
            }
        }
    })
})(Game_CharacterBase.prototype);


//-----------------------------------------------------------------------------
// Game_Player
($ => {

    $.characterType = function() {
        return CHARACTER_TYPE.PLAYER;
    }
})(Game_Player.prototype);

//-----------------------------------------------------------------------------
// Game_Event
($ => {
    $.characterType = function() {
        return CHARACTER_TYPE.EVENT;
    };

    const Alias_setupPage = $.setupPage;
    $.setupPage = function() {
        Alias_setupPage.call(this);
        this.refreshReflections();
    }
})(Game_Event.prototype);

($=> {
    const Alias_isLandOk = $.isLandOk;
    $.isLandOk = function(x, y, d) {
        const isLand = Alias_isLandOk.call(this, x, y, d);;
        if (this.isAirship()) {
            d = 0;
            $gamePlayer.setThrough(false);
        }
        const passabilityRegion = $gameMap.getPassabilityRegion(this.getRegionId(x, y, d));
        if (passabilityRegion) {
            if (passabilityRegion.block === CHARACTER_TYPE.ALL ||passabilityRegion.block === CHARACTER_TYPE.PLAYER) {
                if (this.isAirship()) $gamePlayer.setThrough(true);
                return false;
            }
            if (passabilityRegion.allow === CHARACTER_TYPE.ALL || passabilityRegion.allow === CHARACTER_TYPE.PLAYER) {
                if (this.isAirship()) $gamePlayer.setThrough(true);
                return true;
            }
        }
        return isLand
    };


})(Game_Vehicle.prototype);

//-----------------------------------------------------------------------------
// Tilemap
($ => {
    const Alias__createLayers = $._createLayers;
    $._createLayers = function () {
        Alias__createLayers.call(this);
        this.createReflectionContainer();
        this.createParallaxMaps();
    };

    $.createReflectionContainer = function () {
        if (!PARAMS.characterReflections) return;
        this._reflectionContainer = new PIXI.Container();
        this._reflectionContainer.z = -2;
        this.addChild(this._reflectionContainer);

        if (!PARAMS.characterReflectionBlur) return;
        if (!MZPlus.PrimeParallax.blurFilter) MZPlus.PrimeParallax.blurFilter = new PIXI.filters.KawaseBlurFilter(1, BLUR_QUALITY[PARAMS.characterReflectionQuality], true);
        this._reflectionContainer.filters = [new PIXI.filters.AlphaFilter(), MZPlus.PrimeParallax.blurFilter];

        this._reflectionContainer.filters[0].alpha = PARAMS.characterReflectionOpacity / 255;

        this._reflectionContainer.filterArea = new Rectangle(0, 0, Graphics.width, Graphics.height)
    };

    $.createParallaxMaps = function () {
        const parallaxMaps = Object.values($gameMap.parallaxMaps);
        this._parallaxMaps = {};
        for (let i = 0; i < parallaxMaps.length; ++i) {
            const data = parallaxMaps[i];
            this.addParallaxMap(data);
        }
    };

    $.addParallaxMap = function (obj) {
        if (!this._parallaxMaps[obj.id]) {
            let parallaxMap;
            if (obj.type === PARALLAX_TYPE.TILINGSPRITE) {
                parallaxMap = new MZPlus.PrimeParallax.TilingSprite_Parallax(obj);
            } else {
                parallaxMap = new MZPlus.PrimeParallax.Sprite_Parallax(obj);

            }
            this.addChild(parallaxMap);
            this._parallaxMaps[obj.id] = parallaxMap;
        }
    };

    $.getParallaxLayer = function (obj) {
        return this._parallaxMaps[obj.id];
    };

    const Alias__addShadow = $._addShadow;
    $._addShadow = function () {
        if (PARAMS.disableAutoShadows) return;
        Alias__addShadow.call(this, ...arguments);
    };


    Object.defineProperties($, {
        parallaxMaps: {
            get: function () {
                return this._parallaxMaps;
            }
        }
    })

})(Tilemap.prototype);

//-----------------------------------------------------------------------------
// Spriteset_Map
($ => {
    const Alias_createLowerLayer = $.createLowerLayer;
    $.createLowerLayer = function () {
        Alias_createLowerLayer.call(this);
        this.createCharacterReflections();
    };

    $.createCharacterReflections = function () {
        this._charReflectSprites = [];
        if (!PARAMS.characterReflections) return;

        for (const event of $gameMap.events()) {
            this._charReflectSprites.push(new MZPlus.PrimeParallax.Sprite_Reflections(event));
        }
        for (const vehicle of $gameMap.vehicles()) {
            this._charReflectSprites.push(new MZPlus.PrimeParallax.Sprite_Reflections(vehicle));
        }
        for (const follower of $gamePlayer.followers().reverseData()) {
            this._charReflectSprites.push(new MZPlus.PrimeParallax.Sprite_Reflections(follower));
        }
        this._charReflectSprites.push(new MZPlus.PrimeParallax.Sprite_Reflections($gamePlayer));
        for (const sprite of this._charReflectSprites) {
            this._tilemap._reflectionContainer.addChild(sprite);
        }
    };

    const Alias_createParallax= $.createParallax;
    $.createParallax = function() {
        if (!PARAMS.disableDefaultParallax) Alias_createParallax.call(this);
    };

    const Alias_updateParallax= $.updateParallax;
    $.updateParallax = function() {
        if (!PARAMS.disableDefaultParallax) Alias_updateParallax.call(this);
    };

    const Alias_update = $.update;
    $.update = function () {
        Alias_update.call(this);
        for (let i = 0; i < this._charReflectSprites.length; ++i) {
            const reflection = this._charReflectSprites[i];
            reflection.update();
        }
    }

})(Spriteset_Map.prototype);