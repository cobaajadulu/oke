/*:
 * @plugindesc Hiển thị thông tin vật phẩm khi thêm vào túi
 * @author ChatGPT
 *
 * @param windowX
 * @text Window X Position
 * @desc Vị trí X của cửa sổ (mặc định: center)
 * @type string
 * @default center
 *
 * @param windowY
 * @text Window Y Position
 * @desc Vị trí Y của cửa sổ
 * @type number
 * @min 0
 * @default 0
 * 
 * @param paddingOffset
 * @text Padding Offset
 * @desc Khoảng trống thêm vào trước icon và sau cùng của thông báo (pixel)
 * @type number
 * @min 0
 * @default 10
 * 
 * @param showQuantity
 * @text Show Quantity
 * @desc Hiển thị số lượng vật phẩm phía sau tên (true) hoặc không (false)
 * @type boolean
 * @default true
 * 
 * @param displayTime
 * @text Display Time
 * @desc Thời gian hiển thị cửa sổ thông báo (tính bằng frame)
 * @type number
 * @min 1
 * @default 120
 *
 * @param sfx
 * @text Popup SFX
 * @desc Âm thanh chơi khi cửa sổ hiện lên
 * @type file
 * @dir audio/se/
 * @require 1
 * @default Cursor1
 * 
 * @param sfxVolume
 * @text SFX Volume
 * @desc Âm lượng của âm thanh (0 - 100)
 * @type number
 * @min 0
 * @max 100
 * @default 100
 * 
 * @param sfxPitch
 * @text SFX Pitch
 * @desc Pitch cuar ama thanh (0 - 100)
 * @type number
 * @min 50
 * @max 150
 * @default 100
 * 
 * @param transparentWindow
 * @text Transparent Window
 * @desc Chọn giữa sử dụng windowskin (false) hoặc làm trong suốt (true)
 * @type boolean
 * @default false
 *
 * @help
 * Script này sẽ hiển thị một cửa sổ mới mỗi khi có bất kì một item nào
 * được thêm vào túi của người chơi. Cửa sổ sẽ hiển thị icon và tên của
 * item vừa mới được thêm vào, sau 3 giây, cửa sổ sẽ biến mất.
 */


(function () {
   const PLUGIN_NAME = "NewItemPopup";
   const parameters = PluginManager.parameters(PLUGIN_NAME);

   const windowX = String(parameters["windowX"] || "center");
   const windowY = Number(parameters["windowY"] || 0);
   const paddingOffset = Number(parameters["paddingOffset"] || 10);
   const showQuantity = parameters["showQuantity"].toLowerCase() === "true";
   const displayTime = Number(parameters["displayTime"] || 120);
   const sfx = String(parameters["sfx"] || "Cursor1");
   const sfxVolume = Number(parameters["sfxVolume"] || 100);
   const sfxPitch = Number(parameters["sfxPitch"] || 100);
   const transparentWindow = parameters["transparentWindow"].toLowerCase() === "true";

   class Window_NewItemPopup extends Window_Base {
      constructor() {
         super(new Rectangle(0, 0, 0, Window_Base.prototype.fittingHeight(1)));
         this.contentsOpacity = 0;
         this.hide();
         if (transparentWindow) {
            this.opacity = 0;
         } else {
            this.opacity = 255;
         }
         this._item = null;
         this._displayTime = 0;
      }

      showNewItem(item) {
         this._item = item;
         this.updateWindowSize();
         this.show();
         this.fadeIn(16);
         AudioManager.playSe({name: sfx, pan: 0, pitch: sfxPitch, volume: sfxVolume});
         this._displayTime = displayTime; // Thời gian hiển thị là 60 frames x 3 = 3 giây
      }

      updateWindowSize() {
         const iconWidth = 32;
         const textWidth = this.textWidth(this._item.name);
         const quantityWidth = showQuantity ? this.textWidth(" x" + $gameParty.numItems(this._item)) : 0;
         const padding = this.padding * 2;
         const offset = 10;
         this.width = iconWidth + textWidth + padding + offset + quantityWidth + (paddingOffset * 2);

         if (windowX === "center") {
            this.x = (Graphics.width - this.width) / 2;
         } else {
            this.x = parseInt(windowX, 10);
         }

         this.y = windowY;

         this.createContents();
      }

      fadeIn(duration) {
         this.contentsOpacity = 0;
         this.show();
         this._fadeInDuration = duration;
      }

      fadeOut(duration) {
         this._fadeOutDuration = duration;
      }

      refresh() {
         this.contents.clear();
         if (this._item) {
            this.drawIcon(this._item.iconIndex, paddingOffset, 0);
            this.drawText(this._item.name, 32 + paddingOffset + 10, 0);
            if (showQuantity) {
               const quantity = " x" + $gameParty.numItems(this._item);
               this.drawText(quantity, this.textWidth(this._item.name) + 42 + paddingOffset, 0);
            }
         }
      }

      update() {
         super.update();
         if (this._fadeInDuration > 0) {
            this.contentsOpacity += 255 / this._fadeInDuration;
            this._fadeInDuration--;
            this.refresh();
         } else if (this._displayTime > 0) {
            this._displayTime--;
            if (this._displayTime === 0) {
               this.fadeOut(16);
            }
         } else if (this._fadeOutDuration > 0) {
            this.contentsOpacity -= 255 / this._fadeOutDuration;
            this.opacity -= 255 / this._fadeOutDuration;
            this._fadeOutDuration--;
            if (this._fadeOutDuration === 0) {
               this.hide();
            }
         }
      }
   }

   let _Scene_Map_createAllWindows = Scene_Map.prototype.createAllWindows;
   Scene_Map.prototype.createAllWindows = function () {
      _Scene_Map_createAllWindows.call(this);
      this._newItemPopupWindow = new Window_NewItemPopup();
      this.addWindow(this._newItemPopupWindow);
   };

   let _Game_Party_gainItem = Game_Party.prototype.gainItem;
   Game_Party.prototype.gainItem = function (item, amount, includeEquip) {
      _Game_Party_gainItem.call(this, item, amount, includeEquip);
      if (SceneManager._scene instanceof Scene_Map && amount > 0) {
         if (item) {
            SceneManager._scene._newItemPopupWindow.showNewItem(item);
         }
      }
   };
})();