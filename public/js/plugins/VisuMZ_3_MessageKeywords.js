//=============================================================================
// VisuStella MZ - Message Keywords
// VisuMZ_3_MessageKeywords.js
//=============================================================================

var Imported = Imported || {};
Imported.VisuMZ_3_MessageKeywords = true;

var VisuMZ = VisuMZ || {};
VisuMZ.MessageKeywords = VisuMZ.MessageKeywords || {};
VisuMZ.MessageKeywords.version = 1.01;

//=============================================================================
 /*:
 * @target MZ
 * @plugindesc [RPG Maker MZ] [Tier 3] [Version 1.01] [MessageKeywords]
 * @author VisuStella
 * @url http://www.yanfly.moe/wiki/Message_Keywords_VisuStella_MZ
 * @base VisuMZ_1_MessageCore
 * @orderAfter VisuMZ_1_MessageCore
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * This plugin adds Keyword support for the Message Window and any others
 * listed in the Plugin Parameters. By having Keyword support, the player can
 * hover their mouse cursor over the Keyword and a tooltip window will appear,
 * explaining further about the Keyword in question. This can be used in the
 * Message Window to explain lore, in the Help Window to go into detail about
 * more complex mechanics, and more!
 *
 * Features include all (but not limited to) the following:
 * 
 * * Setup Keywords within the Plugin Parameters.
 * * Keywords determine how the Keyword marker will be replaced and what kind
 *   of text will be displayed in the tooltip window.
 * * Use Keywords to explain or remind the player about lore heavy topics.
 * * Keywords can be used to explain indepth mechanics inside Help Window.
 * * Alter the tooltip window's settings to your liking.
 *
 * ============================================================================
 * Requirements
 * ============================================================================
 *
 * This plugin is made for RPG Maker MZ. This will not work in other iterations
 * of RPG Maker.
 *
 * ------ Required Plugin List ------
 *
 * * VisuMZ_1_MessageCore
 *
 * This plugin requires the above listed plugins to be installed inside your
 * game's Plugin Manager list in order to work. You cannot start your game with
 * this plugin enabled without the listed plugins.
 *
 * ------ Tier 3 ------
 *
 * This plugin is a Tier 3 plugin. Place it under other plugins of lower tier
 * value on your Plugin Manager list (ie: 0, 1, 2, 3, 4, 5). This is to ensure
 * that your plugins will have the best compatibility with the rest of the
 * VisuStella MZ library.
 * 
 * ============================================================================
 * Instructions
 * ============================================================================
 * 
 * Here are the instructions on how to use this plugin.
 * 
 * ---
 * 
 * Step 1:
 * 
 * - Open up the Plugin Parameters for this plugin.
 * - Open up the "Keyword List" Parameter list.
 * - Add your own or modify existing Keywords.
 *   - The "Keyword" is the Keyword Marker that will be referenced when using a
 *     Keyword inside of the Message Window or Help Window. Remember this.
 *   - The "Replacement Text" is the text that appears in place of the Keyword
 *     Marker. This can be used to color code or as a shortcut for Keywords.
 *     - Replacement text does not have to be exactly the same as the Keyword.
 *   - "Tooltip Text" is the text that appears inside the tooltip window when
 *     the player's mouse cursor hovers over the Keyword.
 * - Save your changes.
 * 
 * ---
 * 
 * Step 2:
 * 
 * - Create a new "Show Message" event command or modify a database object's
 *   help "Description".
 * - Insert the Keyword Marker in the following format: ((Keyword))
 *   - Replace "Keyword" with the Keyword Marker mentioned in Step 1.
 *   - To use the default examples, you can type in ((Example)) or ((Ojima)).
 * - Save the changes.
 * - Go view them in game.
 * - Hover the mouse cursor over the specific Keywords and a tooltip window
 *   should appear.
 * 
 * ---
 * 
 * Tooltip window text does not support Word Wrap. It is simply disabled from
 * the very start so any Word Wrap text codes will not work with it.
 * 
 * ---
 *
 * ============================================================================
 * Extra Features
 * ============================================================================
 *
 * There are some extra features found if other VisuStella MZ plugins are found
 * present in the Plugin Manager list.
 *
 * ---
 * 
 * VisuMZ_1_ElementStatusCore
 * 
 * VisuMZ_1_ItemsEquipsCore
 *
 * VisuMZ_2_QuestSystem
 * 
 * VisuMZ_3_MessageLog
 * 
 * VisuMZ_3_VisualTextWindows
 *
 * - Custom windows provided by these plugins will have Keyword support as long
 * as their respective window names are listed in the Plugin Parameters.
 *
 * ---
 *
 * ============================================================================
 * Available Text Codes
 * ============================================================================
 *
 * The following are text codes that you may use with this plugin. 
 *
 * === Keyword-Related Text Codes ===
 * 
 * ---
 *
 * --------------------   -----------------------------------------------------
 * Text Code              Effect (Supported Message Windows)
 * --------------------   -----------------------------------------------------
 * 
 * ((Keyword))            Replaces the "Keyword" Marker with the Replacement
 *                        Text found in the Message Keywords Plugin Parameters.
 *                        If the player hovers the mouse cursor over a Keyword,
 *                        a tooltip window will appear explaining about the
 *                        Keyword's lore and/or mechanics. The replacement text
 *                        and tooltip text can be modified inside the Message
 *                        Keywords Plugin Parameters.
 * 
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Keyword List
 * ============================================================================
 *
 * This array governs the Keywords that are used for the game.
 *
 * ---
 *
 * Keyword List
 * 
 *   Keyword Marker:
 *   - This is the marker used to determine the tooltip and any associated text
 *   - To use this inside the Message Window or Help Description, type out the
 *     following:
 * 
 *     ((Keyword))
 * 
 *     Where "Keyword" would be the Keyword Marker used. Case does not matter.
 * 
 *   Replacement Text:
 *   - The text displayed as a replacement for the tooltip.
 *   - You may use text codes.
 * 
 *   Tooltip Text:
 *   - The text displayed for this tooltip.
 *   - You may use text codes.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Tooltip Settings
 * ============================================================================
 *
 * Settings for the Message Keyword Tooltips Window.
 *
 * ---
 *
 * Appearance
 * 
 *   Scale:
 *   - What scale size do you want for the tooltip?
 *   - Use 1.0 for normal size.
 * 
 *   Skin Filename:
 *   - What window skin do you want to use for the tooltip?
 * 
 *   Skin Opacity:
 *   - What opacity setting is used for the tooltip?
 *   - Use a number between 0 and 255.
 *
 * ---
 *
 * Offset
 * 
 *   Offset X:
 *   - Offset the tooltip X position from the mouse?
 *   - Negative: left. Positive: right.
 * 
 *   Offset Y:
 *   - Offset the tooltip Y position from the mouse?
 *   - Negative: up. Positive: down.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Supported Windows
 * ============================================================================
 *
 * Message Keyword support will be provided to these windows.
 * Not every window is a valid candidate, however.
 *
 * ---
 *
 * Supported Windows
 * 
 *   String:
 *   - Type in the constructor name of window you wish to add to the supported
 *     Window list.
 *   - Any windows not on the list will not support Keywords in the sense that
 *     tooltips will not appear. However, Keyword Markers can still be used to
 *     offer a quick shortcut to replacement text outside of tooltip windows.
 *   - Any of the windows listed here will have their refresh functions monkey
 *     patched via JavaScript to support Message Keywords.
 *
 * ---
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 *
 * 1. These plugins may be used in free or commercial games provided that they
 * have been acquired through legitimate means at VisuStella.com and/or any
 * other official approved VisuStella sources. Exceptions and special
 * circumstances that may prohibit usage will be listed on VisuStella.com.
 * 
 * 2. All of the listed coders found in the Credits section of this plugin must
 * be given credit in your games or credited as a collective under the name:
 * "VisuStella".
 * 
 * 3. You may edit the source code to suit your needs, so long as you do not
 * claim the source code belongs to you. VisuStella also does not take
 * responsibility for the plugin if any changes have been made to the plugin's
 * code, nor does VisuStella take responsibility for user-provided custom code
 * used for custom control effects including advanced JavaScript notetags
 * and/or plugin parameters that allow custom JavaScript code.
 * 
 * 4. You may NOT redistribute these plugins nor take code from this plugin to
 * use as your own. These plugins and their code are only to be downloaded from
 * VisuStella.com and other official/approved VisuStella sources. A list of
 * official/approved sources can also be found on VisuStella.com.
 *
 * 5. VisuStella is not responsible for problems found in your game due to
 * unintended usage, incompatibility problems with plugins outside of the
 * VisuStella MZ library, plugin versions that aren't up to date, nor
 * responsible for the proper working of compatibility patches made by any
 * third parties. VisuStella is not responsible for errors caused by any
 * user-provided custom code used for custom control effects including advanced
 * JavaScript notetags and/or plugin parameters that allow JavaScript code.
 *
 * 6. If a compatibility patch needs to be made through a third party that is
 * unaffiliated with VisuStella that involves using code from the VisuStella MZ
 * library, contact must be made with a member from VisuStella and have it
 * approved. The patch would be placed on VisuStella.com as a free download
 * to the public. Such patches cannot be sold for monetary gain, including
 * commissions, crowdfunding, and/or donations.
 * 
 * 7. If this VisuStella MZ plugin is a paid product, all project team members
 * must purchase their own individual copies of the paid product if they are to
 * use it. Usage includes working on related game mechanics, managing related
 * code, and/or using related Plugin Commands and features. Redistribution of
 * the plugin and/or its code to other members of the team is NOT allowed
 * unless they own the plugin itself as that conflicts with Article 4.
 * 
 * 8. Any extensions and/or addendums made to this plugin's Terms of Use can be
 * found on VisuStella.com and must be followed.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * 
 * If you are using this plugin, credit the following people in your game:
 * 
 * Team VisuStella
 * * Irina
 * * Arisu
 * * Olivia
 * * Yanfly
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 * 
 * Version 1.01: February 24, 2022
 * * Feature Update!
 * ** Variables are now parsed before and after the parsing of keywords.
 *    Update made by Arisu.
 * 
 * Version 1.00 Official Release Date: December 8, 2021
 * * Finished Plugin!
 *
 * ============================================================================
 * End of Helpfile
 * ============================================================================
 *
 * @ ==========================================================================
 * @ Plugin Parameters
 * @ ==========================================================================
 *
 * @param BreakHead
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param MessageKeywords
 * @default Plugin Parameters
 *
 * @param ATTENTION
 * @default READ THE HELP FILE
 *
 * @param BreakSettings
 * @text --------------------------
 * @default ----------------------------------
 * 
 * @param Keywords:arraystruct
 * @text Keyword List
 * @parent Keywords
 * @type struct<Keyword>[]
 * @desc This is a list of Keywords used for this plugin.
 * @default ["{\"Keyword:str\":\"Example\",\"Text:str\":\"\\\\c[5]Example\\\\c[0]\",\"Tooltip:json\":\"\\\"This is an example to show how \\\\\\\\c[5]Keywords\\\\\\\\c[0] work.\\\\n\\\\nBy typing \\\\\\\\c[6]((Example))\\\\\\\\c[0] in the \\\\\\\\c[4]Message Window\\\\\\\\c[0],\\\\nit creates an area that the player can hover\\\\nthe \\\\\\\\c[4]mouse\\\\\\\\c[0] over.\\\\n\\\\nOnce hovered, a \\\\\\\\c[4]tooltip\\\\\\\\c[0] will appear displaying\\\\nthis text.\\\"\"}","{\"Keyword:str\":\"Ojima\",\"Text:str\":\"\\\\c[6]Yoji Ojima\\\\c[0]\",\"Tooltip:json\":\"\\\"\\\\\\\\c[6]Yoji Ojima\\\\\\\\c[0] is the creator of many \\\\\\\\c[4]RPG Maker\\\\\\\\c[0] iterations\\\\nincluding \\\\\\\\c[4]RPG Maker MZ\\\\\\\\c[0]. Without him, \\\\\\\\c[4]RPG Maker\\\\\\\\c[0] as we\\\\nknow it would be completely different. \\\\\\\\c[4]RPG Maker MZ\\\\\\\\c[0]'s\\\\nbeautiful and clean core scripts is all thanks to this\\\\nvery talented individual.\\\"\"}"]
 *
 * @param Tooltip:struct
 * @text Tooltip Settings
 * @type struct<Tooltip>
 * @desc Settings for the Message Keyword Tooltips Window.
 * @default {"Appearance":"","Scale:num":"0.6","WindowSkin:str":"Window","WindowOpacity:num":"240","Offset":"","OffsetX:num":"+0","OffsetY:num":"+0"}
 * 
 * @param SupportedWindows:arraystr
 * @text Supported Windows
 * @type string[]
 * @desc Message Keyword support will be provided to these windows.
 * Not every window is a valid candidate, however.
 * @default ["Window_Help","Window_SkillStatus","Window_EquipStatus","Window_Status","Window_ShopStatus","Window_Message","Window_NameBox","Window_StatusData","Window_QuestLog","Window_QuestTracker","Window_MessageLog","Window_VisualText"]
 *
 * @param BreakEnd1
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param End Of
 * @default Plugin Parameters
 *
 * @param BreakEnd2
 * @text --------------------------
 * @default ----------------------------------
 *
 */
/* ----------------------------------------------------------------------------
 * Keyword Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Keyword:
 *
 * @param Keyword:str
 * @text Keyword Marker
 * @desc This is the marker used to determine the tooltip and
 * any associated text.
 * @default Untitled
 *
 * @param Text:str
 * @text Replacement Text
 * @type str
 * @desc The text displayed as a replacement for the tooltip.
 * You may use text codes.
 * @default Untitled
 * 
 * @param Tooltip:json
 * @text Tooltip Text
 * @type note
 * @desc The text displayed for this tooltip.
 * You may use text codes.
 * @default ""
 *
 */
/* ----------------------------------------------------------------------------
 * Tooltip Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Tooltip:
 *
 * @param Appearance
 *
 * @param Scale:num
 * @text Scale
 * @parent Appearance
 * @desc What scale size do you want for the tooltip?
 * Use 1.0 for normal size.
 * @default 0.6
 *
 * @param WindowSkin:str
 * @text Skin Filename
 * @parent Appearance
 * @type file
 * @dir img/system/
 * @desc What window skin do you want to use for the tooltip?
 * @default Window
 *
 * @param WindowOpacity:num
 * @text Skin Opacity
 * @parent Appearance
 * @type number
 * @min 0
 * @max 255
 * @desc What opacity setting is used for the tooltip?
 * Use a number between 0 and 255.
 * @default 240
 *
 * @param Offset
 *
 * @param OffsetX:num
 * @text Offset X
 * @parent Offset
 * @desc Offset the tooltip X position from the mouse?
 * Negative: left. Positive: right.
 * @default +0
 *
 * @param OffsetY:num
 * @text Offset Y
 * @parent Offset
 * @desc Offset the tooltip Y position from the mouse?
 * Negative: up. Positive: down.
 * @default +0
 *
 */
//=============================================================================

function _0x42dc(_0x3981bf,_0xa0e156){const _0x4367b6=_0x4367();return _0x42dc=function(_0x42dcce,_0x4e80fd){_0x42dcce=_0x42dcce-0xde;let _0x499640=_0x4367b6[_0x42dcce];return _0x499640;},_0x42dc(_0x3981bf,_0xa0e156);}const _0x1080b1=_0x42dc;(function(_0x5906cc,_0xb5c6d2){const _0x581cca=_0x42dc,_0x4dcebd=_0x5906cc();while(!![]){try{const _0x36b4c5=-parseInt(_0x581cca(0x18d))/0x1+-parseInt(_0x581cca(0x17b))/0x2+-parseInt(_0x581cca(0x16e))/0x3+-parseInt(_0x581cca(0x141))/0x4*(-parseInt(_0x581cca(0x10b))/0x5)+-parseInt(_0x581cca(0x138))/0x6*(-parseInt(_0x581cca(0xf2))/0x7)+-parseInt(_0x581cca(0xe7))/0x8+-parseInt(_0x581cca(0x173))/0x9*(-parseInt(_0x581cca(0x125))/0xa);if(_0x36b4c5===_0xb5c6d2)break;else _0x4dcebd['push'](_0x4dcebd['shift']());}catch(_0x405c63){_0x4dcebd['push'](_0x4dcebd['shift']());}}}(_0x4367,0x677ff));var label=_0x1080b1(0x13a),tier=tier||0x0,dependencies=[_0x1080b1(0x14c)],pluginData=$plugins[_0x1080b1(0x18b)](function(_0x5d7771){const _0x2d3c64=_0x1080b1;return _0x5d7771[_0x2d3c64(0x121)]&&_0x5d7771[_0x2d3c64(0x13c)][_0x2d3c64(0x15f)]('['+label+']');})[0x0];VisuMZ[label][_0x1080b1(0x183)]=VisuMZ[label][_0x1080b1(0x183)]||{},VisuMZ[_0x1080b1(0x14e)]=function(_0x523ba0,_0x3d326f){const _0x354125=_0x1080b1;for(const _0x430686 in _0x3d326f){if(_0x354125(0x108)!==_0x354125(0x108))this[_0x354125(0xea)](...arguments);else{if(_0x430686['match'](/(.*):(.*)/i)){const _0x357832=String(RegExp['$1']),_0x265703=String(RegExp['$2'])['toUpperCase']()[_0x354125(0xf5)]();let _0x15b927,_0x83c1cf,_0x22b326;switch(_0x265703){case _0x354125(0x128):_0x15b927=_0x3d326f[_0x430686]!==''?Number(_0x3d326f[_0x430686]):0x0;break;case _0x354125(0x14a):_0x83c1cf=_0x3d326f[_0x430686]!==''?JSON[_0x354125(0x16d)](_0x3d326f[_0x430686]):[],_0x15b927=_0x83c1cf['map'](_0x5aeb4f=>Number(_0x5aeb4f));break;case _0x354125(0x15b):_0x15b927=_0x3d326f[_0x430686]!==''?eval(_0x3d326f[_0x430686]):null;break;case'ARRAYEVAL':_0x83c1cf=_0x3d326f[_0x430686]!==''?JSON[_0x354125(0x16d)](_0x3d326f[_0x430686]):[],_0x15b927=_0x83c1cf[_0x354125(0x12b)](_0x2b4e30=>eval(_0x2b4e30));break;case _0x354125(0x133):_0x15b927=_0x3d326f[_0x430686]!==''?JSON[_0x354125(0x16d)](_0x3d326f[_0x430686]):'';break;case'ARRAYJSON':_0x83c1cf=_0x3d326f[_0x430686]!==''?JSON[_0x354125(0x16d)](_0x3d326f[_0x430686]):[],_0x15b927=_0x83c1cf['map'](_0x57e8c1=>JSON['parse'](_0x57e8c1));break;case _0x354125(0x143):_0x15b927=_0x3d326f[_0x430686]!==''?new Function(JSON['parse'](_0x3d326f[_0x430686])):new Function('return\x200');break;case _0x354125(0xf7):_0x83c1cf=_0x3d326f[_0x430686]!==''?JSON[_0x354125(0x16d)](_0x3d326f[_0x430686]):[],_0x15b927=_0x83c1cf['map'](_0x3729fb=>new Function(JSON[_0x354125(0x16d)](_0x3729fb)));break;case'STR':_0x15b927=_0x3d326f[_0x430686]!==''?String(_0x3d326f[_0x430686]):'';break;case _0x354125(0xfb):_0x83c1cf=_0x3d326f[_0x430686]!==''?JSON[_0x354125(0x16d)](_0x3d326f[_0x430686]):[],_0x15b927=_0x83c1cf[_0x354125(0x12b)](_0x47a156=>String(_0x47a156));break;case _0x354125(0x15a):_0x22b326=_0x3d326f[_0x430686]!==''?JSON[_0x354125(0x16d)](_0x3d326f[_0x430686]):{},_0x15b927=VisuMZ[_0x354125(0x14e)]({},_0x22b326);break;case _0x354125(0x107):_0x83c1cf=_0x3d326f[_0x430686]!==''?JSON[_0x354125(0x16d)](_0x3d326f[_0x430686]):[],_0x15b927=_0x83c1cf[_0x354125(0x12b)](_0x3c3389=>VisuMZ[_0x354125(0x14e)]({},JSON[_0x354125(0x16d)](_0x3c3389)));break;default:continue;}_0x523ba0[_0x357832]=_0x15b927;}}}return _0x523ba0;},(_0x543919=>{const _0x51de37=_0x1080b1,_0x26f5a8=_0x543919['name'];for(const _0x42023a of dependencies){if('jWDVq'==='jWDVq'){if(!Imported[_0x42023a]){if('dvNBC'===_0x51de37(0x165))return this[_0x51de37(0x151)];else{alert(_0x51de37(0x140)[_0x51de37(0x144)](_0x26f5a8,_0x42023a)),SceneManager[_0x51de37(0x147)]();break;}}}else{const _0x9221ca=new _0x108234(_0x5d9e5b['x'],_0x29677d['y']),_0x5b10c1=this[_0x51de37(0x161)][_0x51de37(0x11c)](_0x9221ca);return this[_0x51de37(0x118)](_0x5b10c1['x'],_0x5b10c1['y']);}}const _0x2c8b9e=_0x543919[_0x51de37(0x13c)];if(_0x2c8b9e[_0x51de37(0x15c)](/\[Version[ ](.*?)\]/i)){const _0x4da72c=Number(RegExp['$1']);_0x4da72c!==VisuMZ[label][_0x51de37(0x14d)]&&(alert(_0x51de37(0x12c)['format'](_0x26f5a8,_0x4da72c)),SceneManager['exit']());}if(_0x2c8b9e[_0x51de37(0x15c)](/\[Tier[ ](\d+)\]/i)){const _0x341e47=Number(RegExp['$1']);_0x341e47<tier?(alert(_0x51de37(0x146)[_0x51de37(0x144)](_0x26f5a8,_0x341e47,tier)),SceneManager['exit']()):tier=Math[_0x51de37(0x170)](_0x341e47,tier);}VisuMZ[_0x51de37(0x14e)](VisuMZ[label][_0x51de37(0x183)],_0x543919['parameters']);})(pluginData),VisuMZ['MessageKeywords'][_0x1080b1(0xe0)]=Scene_Boot[_0x1080b1(0xf9)][_0x1080b1(0x115)],Scene_Boot['prototype'][_0x1080b1(0x115)]=function(){const _0x338876=_0x1080b1;VisuMZ[_0x338876(0x13a)][_0x338876(0xe0)][_0x338876(0xf8)](this),this[_0x338876(0x153)]();},Scene_Boot[_0x1080b1(0xf9)][_0x1080b1(0x153)]=function(){const _0x550b28=_0x1080b1;VisuMZ[_0x550b28(0x13a)][_0x550b28(0x124)](),VisuMZ[_0x550b28(0x13a)]['CreateRefreshPatches']();},VisuMZ[_0x1080b1(0x13a)][_0x1080b1(0x184)]={},VisuMZ[_0x1080b1(0x13a)][_0x1080b1(0x124)]=function(){const _0x4cb08d=_0x1080b1;for(const _0x4f0348 of VisuMZ[_0x4cb08d(0x13a)][_0x4cb08d(0x183)][_0x4cb08d(0x184)]){if('StrwJ'===_0x4cb08d(0x150))_0x29ae47['MessageKeywords'][_0x4cb08d(0x124)](),_0x111afd[_0x4cb08d(0x13a)][_0x4cb08d(0x18e)]();else{if(!_0x4f0348)continue;if(!_0x4f0348[_0x4cb08d(0x16c)])continue;if(_0x4f0348[_0x4cb08d(0x16c)]['trim']()<=0x0)continue;if(_0x4f0348[_0x4cb08d(0x16c)][_0x4cb08d(0x104)]()[_0x4cb08d(0xf5)]()===_0x4cb08d(0x154))continue;_0x4f0348[_0x4cb08d(0x16c)]=_0x4f0348[_0x4cb08d(0x16c)]['toUpperCase']()[_0x4cb08d(0xf5)](),VisuMZ['MessageKeywords'][_0x4cb08d(0x184)][_0x4f0348['Keyword']]=_0x4f0348;}}},VisuMZ['MessageKeywords'][_0x1080b1(0x162)]='\x0a\x20\x20\x20\x20this.clearMessageKeywordSprites();\x0a\x20\x20\x20\x20VisuMZ.MessageKeywords.%1.call(this);\x0a',VisuMZ[_0x1080b1(0x13a)][_0x1080b1(0x18e)]=function(){const _0x35cebf=_0x1080b1,_0x2213b6=Window_MessageKeywordTooltip[_0x35cebf(0x111)];for(const _0x2fefc8 of _0x2213b6){if(_0x35cebf(0x12d)!=='Gmbkf'){if(window[_0x2fefc8]&&window[_0x2fefc8][_0x35cebf(0xf9)][_0x35cebf(0x105)]){const _0x3b5fae='%1_refresh'[_0x35cebf(0x144)](_0x2fefc8);VisuMZ[_0x35cebf(0x13a)][_0x3b5fae]=window[_0x2fefc8][_0x35cebf(0xf9)][_0x35cebf(0x105)];const _0x4efe44=VisuMZ[_0x35cebf(0x13a)][_0x35cebf(0x162)]['format'](_0x3b5fae);window[_0x2fefc8][_0x35cebf(0xf9)][_0x35cebf(0x105)]=new Function(_0x4efe44);}}else{const _0x64c21a=this['_hoverState'];this[_0x35cebf(0x17c)]=this['isBeingTouched'](),this[_0x35cebf(0x17c)]!==_0x64c21a&&(this['_hoverState']?this[_0x35cebf(0x127)]():this['onMouseExit']());}}},VisuMZ[_0x1080b1(0x13a)][_0x1080b1(0x174)]=Scene_Base[_0x1080b1(0xf9)][_0x1080b1(0x139)],Scene_Base[_0x1080b1(0xf9)]['createWindowLayer']=function(){const _0x391f38=_0x1080b1;VisuMZ['MessageKeywords']['Scene_Base_createWindowLayer'][_0x391f38(0xf8)](this),this[_0x391f38(0x176)]();},Scene_Base[_0x1080b1(0xf9)][_0x1080b1(0x176)]=function(){const _0x520ad8=_0x1080b1;this[_0x520ad8(0x157)]=new Window_MessageKeywordTooltip(),this[_0x520ad8(0xdf)](this['_messageKeywordTooltipWindow']);};function Sprite_MessageKeywordTooltip(){const _0x3ce30a=_0x1080b1;this[_0x3ce30a(0xea)](...arguments);}Sprite_MessageKeywordTooltip[_0x1080b1(0xf9)]=Object[_0x1080b1(0x17f)](Sprite_Clickable[_0x1080b1(0xf9)]),Sprite_MessageKeywordTooltip[_0x1080b1(0xf9)][_0x1080b1(0x14f)]=Sprite_MessageKeywordTooltip,Sprite_MessageKeywordTooltip[_0x1080b1(0xf9)][_0x1080b1(0xea)]=function(_0x351ba2,_0x4ee2eb){const _0x3acd06=_0x1080b1;this[_0x3acd06(0x17d)]=_0x351ba2,Sprite_Clickable[_0x3acd06(0xf9)][_0x3acd06(0xea)]['call'](this),this[_0x3acd06(0x149)](0x0,0x0,_0x4ee2eb['width'],_0x4ee2eb[_0x3acd06(0x18c)]),this['x']=_0x4ee2eb['x'],this['y']=_0x4ee2eb['y'];let _0x2624f3=![];_0x2624f3&&(_0x3acd06(0x156)===_0x3acd06(0x11f)?this['backOpacity']=_0x2c5543['WINDOW_SKIN_OPACITY']:(this['bitmap']=new Bitmap(_0x4ee2eb[_0x3acd06(0x12a)],_0x4ee2eb['height']),this[_0x3acd06(0x15d)][_0x3acd06(0x110)](0x0,0x0,_0x4ee2eb[_0x3acd06(0x12a)],_0x4ee2eb[_0x3acd06(0x18c)],_0x3acd06(0xe5)),this[_0x3acd06(0x188)]=0x40));},Sprite_MessageKeywordTooltip[_0x1080b1(0xf9)]['targetWindow']=function(){const _0x179d4d=_0x1080b1;return SceneManager['_scene'][_0x179d4d(0x157)];},Sprite_MessageKeywordTooltip[_0x1080b1(0xf9)]['onMouseEnter']=function(){const _0x5c70e0=_0x1080b1;Sprite_Clickable[_0x5c70e0(0xf9)]['onMouseEnter'][_0x5c70e0(0xf8)](this);const _0x5d0a73=this[_0x5c70e0(0x18a)]();_0x5d0a73&&('RsNGf'===_0x5c70e0(0x11e)?(_0x3407c5[_0x5c70e0(0x13a)]['Scene_Base_createWindowLayer'][_0x5c70e0(0xf8)](this),this['createMessageKeywordTooltipWindow']()):_0x5d0a73[_0x5c70e0(0xde)](this[_0x5c70e0(0x17d)]));},Sprite_MessageKeywordTooltip[_0x1080b1(0xf9)][_0x1080b1(0x123)]=function(){const _0x136f37=_0x1080b1;Sprite_Clickable['prototype'][_0x136f37(0x123)][_0x136f37(0xf8)](this);const _0x14400a=this[_0x136f37(0x18a)]();_0x14400a&&_0x14400a[_0x136f37(0x189)]===this[_0x136f37(0x17d)]&&_0x14400a[_0x136f37(0xde)](null);},Sprite_MessageKeywordTooltip[_0x1080b1(0xf9)][_0x1080b1(0x163)]=function(){const _0x2380b9=_0x1080b1,_0xadbae=this[_0x2380b9(0x17c)];this['_hoverState']=this[_0x2380b9(0x100)]();if(this[_0x2380b9(0x17c)]!==_0xadbae){if(this[_0x2380b9(0x17c)]){if(_0x2380b9(0x16a)!==_0x2380b9(0x158))this[_0x2380b9(0x127)]();else{this[_0x2380b9(0x182)]='';if(!this[_0x2380b9(0x189)])return;this['setupKeywordText'](),this[_0x2380b9(0x182)]=this[_0x2380b9(0x182)][_0x2380b9(0xf5)]();}}else this[_0x2380b9(0x123)]();}},Sprite_MessageKeywordTooltip[_0x1080b1(0xf9)][_0x1080b1(0x100)]=function(){const _0x5f020d=_0x1080b1,_0x2c7e9f=new Point(TouchInput['x'],TouchInput['y']),_0xa9a1c5=this[_0x5f020d(0x161)][_0x5f020d(0x11c)](_0x2c7e9f);return this[_0x5f020d(0x118)](_0xa9a1c5['x'],_0xa9a1c5['y']);},VisuMZ[_0x1080b1(0x13a)][_0x1080b1(0x171)]=Window_Base['prototype'][_0x1080b1(0xea)],Window_Base[_0x1080b1(0xf9)]['initialize']=function(_0x50bfb0){const _0x122c67=_0x1080b1;VisuMZ[_0x122c67(0x13a)][_0x122c67(0x171)][_0x122c67(0xf8)](this,_0x50bfb0),this['clearMessageKeywordSprites']();},Window_Base[_0x1080b1(0xf9)][_0x1080b1(0xf3)]=function(){const _0x431217=_0x1080b1;!this[_0x431217(0x106)]&&(this[_0x431217(0x106)]=new Sprite(),this[_0x431217(0x15e)][_0x431217(0xdf)](this[_0x431217(0x106)]));while(this[_0x431217(0x106)][_0x431217(0x166)][_0x431217(0x131)]>0x0){const _0x2692c6=this[_0x431217(0x106)][_0x431217(0x166)][0x0];this[_0x431217(0x106)][_0x431217(0x120)](_0x2692c6);}const _0x27a3ed=SceneManager[_0x431217(0xe3)][_0x431217(0x157)];_0x27a3ed&&(_0x431217(0x160)!==_0x431217(0x160)?_0x4316ee[_0x431217(0xde)](this[_0x431217(0x17d)]):_0x27a3ed[_0x431217(0xde)](null));},VisuMZ[_0x1080b1(0x13a)][_0x1080b1(0xe4)]=Window_Base[_0x1080b1(0xf9)][_0x1080b1(0x155)],Window_Base[_0x1080b1(0xf9)][_0x1080b1(0x155)]=function(_0x9ec510){const _0x35cb79=_0x1080b1;return _0x9ec510=this['convertMessageKeywords'](_0x9ec510),_0x9ec510=VisuMZ[_0x35cb79(0x13a)][_0x35cb79(0xe4)][_0x35cb79(0xf8)](this,_0x9ec510),_0x9ec510;},Window_Base[_0x1080b1(0xf9)][_0x1080b1(0x101)]=function(_0x10475f){const _0x344d6a=_0x1080b1;return _0x10475f=this[_0x344d6a(0x152)](_0x10475f),_0x10475f=_0x10475f[_0x344d6a(0xe1)](/\(\((.*?)\)\)/gi,(_0x441cec,_0x2b8e7c)=>{const _0x591f3b=_0x344d6a;if(_0x591f3b(0x135)!==_0x591f3b(0xf6)){const _0x570827=String(_0x2b8e7c)[_0x591f3b(0x104)]()[_0x591f3b(0xf5)]();if(VisuMZ['MessageKeywords'][_0x591f3b(0x184)][_0x570827]){if(_0x591f3b(0xfa)==='tZaQf'){const _0x49e568=VisuMZ['MessageKeywords'][_0x591f3b(0x184)][_0x570827],_0x459b21=_0x49e568[_0x591f3b(0x129)];if(this[_0x591f3b(0x16b)]()){if(_0x591f3b(0xfe)!==_0x591f3b(0xfc))return _0x591f3b(0x10f)[_0x591f3b(0x144)](_0x570827,_0x459b21);else _0x3e0b04(_0x591f3b(0x146)['format'](_0x534305,_0x121bd3,_0x472025)),_0x2921fe[_0x591f3b(0x147)]();}else return _0x459b21;}else this[_0x591f3b(0x159)]=![],this[_0x591f3b(0x11d)]=_0x32b75d['x'],this['_createKeywordEndY']=_0x518d81['y'],this[_0x591f3b(0x103)]();}else{if(_0x591f3b(0x181)===_0x591f3b(0x181))return'';else{if(!this[_0x591f3b(0x16b)]())return;const _0x1bf80c=this[_0x591f3b(0x114)];let _0x1865c3=this[_0x591f3b(0x178)],_0x2cfc4c=this[_0x591f3b(0xee)]+0x2,_0x3e08=this[_0x591f3b(0x11d)]-_0x1865c3,_0x2d6a80=this[_0x591f3b(0x164)]-_0x2cfc4c+(this[_0x591f3b(0x11b)]['fontSize']+0xa)-0x4;const _0x135464=new _0x54b2a8(_0x1865c3,_0x2cfc4c,_0x3e08,_0x2d6a80),_0x186b82=new _0xcf99a0(_0x1bf80c,_0x135464);this[_0x591f3b(0x106)][_0x591f3b(0xdf)](_0x186b82);}}}else{const _0x2e2a8e=_0x4b9d03(_0x17d752['$1']);_0x2e2a8e<_0x258eb8?(_0x33fb24('%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.'[_0x591f3b(0x144)](_0x49f74b,_0x2e2a8e,_0x2584a7)),_0x437b47[_0x591f3b(0x147)]()):_0x264405=_0x57eda0[_0x591f3b(0x170)](_0x2e2a8e,_0x478979);}}),_0x10475f;},VisuMZ[_0x1080b1(0x13a)][_0x1080b1(0xed)]=Window_Base['prototype'][_0x1080b1(0x12f)],Window_Base[_0x1080b1(0xf9)][_0x1080b1(0x12f)]=function(_0x86ccb,_0x421653){const _0x4702a5=_0x1080b1;switch(_0x86ccb){case _0x4702a5(0x145):_0x421653[_0x4702a5(0x185)]?this[_0x4702a5(0xef)](_0x421653):this[_0x4702a5(0x117)](_0x421653);break;case _0x4702a5(0x13f):_0x421653[_0x4702a5(0x185)]&&this[_0x4702a5(0x159)]&&this[_0x4702a5(0xeb)](_0x421653);this[_0x4702a5(0x17a)](_0x421653);break;default:return VisuMZ[_0x4702a5(0x13a)][_0x4702a5(0xed)]['call'](this,_0x86ccb,_0x421653);break;}},Window_Base['prototype'][_0x1080b1(0xef)]=function(_0x3327b9){const _0x2ce8de=_0x1080b1,_0x142096=this[_0x2ce8de(0x117)](_0x3327b9);this[_0x2ce8de(0x114)]=_0x142096[_0x2ce8de(0x104)]()[_0x2ce8de(0xf5)](),this[_0x2ce8de(0x159)]=!![],this[_0x2ce8de(0x178)]=_0x3327b9['x'],this[_0x2ce8de(0xee)]=_0x3327b9['y'];},Window_Base[_0x1080b1(0xf9)][_0x1080b1(0xeb)]=function(_0x29729d){const _0x316ca8=_0x1080b1;this[_0x316ca8(0x159)]=![],this[_0x316ca8(0x11d)]=_0x29729d['x'],this[_0x316ca8(0x164)]=_0x29729d['y'],this[_0x316ca8(0x103)]();},VisuMZ[_0x1080b1(0x13a)][_0x1080b1(0x134)]=Window_Base['prototype'][_0x1080b1(0x187)],Window_Base[_0x1080b1(0xf9)][_0x1080b1(0x187)]=function(_0x1ec847){const _0x3a02bf=_0x1080b1;_0x1ec847['drawing']&&this[_0x3a02bf(0x159)]&&(this[_0x3a02bf(0x11d)]=_0x1ec847['x'],this['addMessageKeyword']=_0x1ec847['y'],this['addMessageKeyword']()),VisuMZ[_0x3a02bf(0x13a)][_0x3a02bf(0x134)][_0x3a02bf(0xf8)](this,_0x1ec847),_0x1ec847[_0x3a02bf(0x185)]&&this[_0x3a02bf(0x159)]&&(this['_createKeywordStartX']=_0x1ec847['x'],this[_0x3a02bf(0xee)]=_0x1ec847['y']);},Window_Base[_0x1080b1(0xf9)]['isSupportMessageKeywords']=function(){const _0x5b2eab=_0x1080b1;return Window_MessageKeywordTooltip['SUPPORTED_WINDOWS'][_0x5b2eab(0x15f)](this['constructor'][_0x5b2eab(0x180)]);},Window_Base['prototype']['addMessageKeyword']=function(){const _0x218cf1=_0x1080b1;if(!this[_0x218cf1(0x16b)]())return;const _0x19d506=this[_0x218cf1(0x114)];let _0x1a968f=this[_0x218cf1(0x178)],_0x1e9da5=this[_0x218cf1(0xee)]+0x2,_0x59a90a=this[_0x218cf1(0x11d)]-_0x1a968f,_0x39107a=this[_0x218cf1(0x164)]-_0x1e9da5+(this['contents'][_0x218cf1(0x186)]+0xa)-0x4;const _0x56d946=new Rectangle(_0x1a968f,_0x1e9da5,_0x59a90a,_0x39107a),_0x1ad1bc=new Sprite_MessageKeywordTooltip(_0x19d506,_0x56d946);this['_messageKeywordContainer'][_0x218cf1(0xdf)](_0x1ad1bc);},VisuMZ[_0x1080b1(0x13a)][_0x1080b1(0x130)]=Window_Message[_0x1080b1(0xf9)][_0x1080b1(0xe6)],Window_Message[_0x1080b1(0xf9)]['newPage']=function(_0x9b165f){const _0x5117ea=_0x1080b1;this[_0x5117ea(0xf3)](),VisuMZ[_0x5117ea(0x13a)][_0x5117ea(0x130)]['call'](this,_0x9b165f);},VisuMZ[_0x1080b1(0x13a)][_0x1080b1(0x122)]=Window_Message[_0x1080b1(0xf9)][_0x1080b1(0xe2)],Window_Message['prototype']['terminateMessage']=function(){const _0x8213bf=_0x1080b1;this[_0x8213bf(0xf3)](),VisuMZ[_0x8213bf(0x13a)][_0x8213bf(0x122)][_0x8213bf(0xf8)](this);};function Window_MessageKeywordTooltip(){this['initialize'](...arguments);}function _0x4367(){const _0x25e597=['\x1bMSGKEYWORD<%1>%2\x1bMSGKEYWORDEND[0]','fillRect','SUPPORTED_WINDOWS','MOUSE_OFFSET_Y','SupportedWindows','_createKeywordString','onDatabaseLoaded','visible','obtainEscapeString','hitTest','OffsetY','drawTextEx','contents','applyInverse','_createKeywordEndX','CgNhH','VQQHz','removeChild','status','Window_Message_terminateMessage','onMouseExit','RegisterKeywords','2590EYLOmw','backOpacity','onMouseEnter','NUM','Text','width','map','%1\x27s\x20version\x20does\x20not\x20match\x20plugin\x27s.\x20Please\x20update\x20it\x20in\x20the\x20Plugin\x20Manager.','UlzBL','update','processEscapeCharacter','Window_Message_newPage','length','updateOpacity','JSON','Window_Base_processNewLine','BAMLW','createContents','bNosd','6ajSzeA','createWindowLayer','MessageKeywords','Tooltip','description','textSizeEx','WINDOW_SCALE','MSGKEYWORDEND','%1\x20is\x20missing\x20a\x20required\x20plugin.\x0aPlease\x20install\x20%2\x20into\x20the\x20Plugin\x20Manager.','1530876FDfdiC','loadWindowskin','FUNC','format','MSGKEYWORD','%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.','exit','GNgcT','setFrame','ARRAYNUM','setupText','VisuMZ_1_MessageCore','version','ConvertParams','constructor','TneKg','_wordWrap','convertVariableEscapeCharacters','process_VisuMZ_MessageKeywords','UNTITLED','convertEscapeCharacters','mJkAI','_messageKeywordTooltipWindow','GSGwP','_createKeywordMode','STRUCT','EVAL','match','bitmap','_clientArea','includes','AlmYU','worldTransform','funcPatchCodeFmt','processTouch','_createKeywordEndY','Bhmfs','children','clear','updatePosition','round','QHENA','isSupportMessageKeywords','Keyword','parse','1627680APVenP','updateBackOpacity','max','Window_Base_initialize','loadSystem','76860uiUEIF','Scene_Base_createWindowLayer','scale','createMessageKeywordTooltipWindow','windowskin','_createKeywordStartX','_requestRefresh','obtainEscapeParam','784102AJrZNH','_hoverState','_referenceString','clamp','create','name','CkVqR','_text','Settings','Keywords','drawing','fontSize','processNewLine','opacity','_keyword','targetWindow','filter','height','612016mlqYOc','CreateRefreshPatches','setKeyword','addChild','Scene_Boot_onDatabaseLoaded','replace','terminateMessage','_scene','Window_Base_convertEscapeCharacters','magenta','newPage','5803480fAtoIL','WINDOW_SKIN_FILENAME','resizeWindow','initialize','endKeywordMapping','baseTextRect','Window_Base_processEscapeCharacter','_createKeywordStartY','startKeywordMapping','requestRefresh','WindowSkin','709926DfMtgP','clearMessageKeywordSprites','clampPosition','trim','VdsNS','ARRAYFUNC','call','prototype','tZaQf','ARRAYSTR','SlAXU','setupKeywordText','pLRRO','OffsetX','isBeingTouched','convertMessageKeywords','padding','addMessageKeyword','toUpperCase','refresh','_messageKeywordContainer','ARRAYSTRUCT','hWYeV','Scale','show','5fwhnsR','resetFontSettings','WINDOW_SKIN_OPACITY','hide'];_0x4367=function(){return _0x25e597;};return _0x4367();}Window_MessageKeywordTooltip['prototype']=Object[_0x1080b1(0x17f)](Window_Base[_0x1080b1(0xf9)]),Window_MessageKeywordTooltip['prototype'][_0x1080b1(0x14f)]=Window_MessageKeywordTooltip,Window_MessageKeywordTooltip[_0x1080b1(0x13e)]=VisuMZ[_0x1080b1(0x13a)]['Settings'][_0x1080b1(0x13b)][_0x1080b1(0x109)],Window_MessageKeywordTooltip['WINDOW_SKIN_FILENAME']=VisuMZ[_0x1080b1(0x13a)][_0x1080b1(0x183)]['Tooltip'][_0x1080b1(0xf1)],Window_MessageKeywordTooltip['WINDOW_SKIN_OPACITY']=VisuMZ[_0x1080b1(0x13a)]['Settings']['Tooltip']['WindowOpacity'],Window_MessageKeywordTooltip['MOUSE_OFFSET_X']=VisuMZ[_0x1080b1(0x13a)]['Settings'][_0x1080b1(0x13b)][_0x1080b1(0xff)],Window_MessageKeywordTooltip[_0x1080b1(0x112)]=VisuMZ[_0x1080b1(0x13a)][_0x1080b1(0x183)][_0x1080b1(0x13b)][_0x1080b1(0x119)],Window_MessageKeywordTooltip[_0x1080b1(0x111)]=VisuMZ[_0x1080b1(0x13a)][_0x1080b1(0x183)][_0x1080b1(0x113)],Window_MessageKeywordTooltip[_0x1080b1(0xf9)]['initialize']=function(){const _0x4361a8=_0x1080b1,_0x5dfa2b=new Rectangle(0x0,0x0,Graphics[_0x4361a8(0x12a)],Graphics[_0x4361a8(0x18c)]);Window_Base[_0x4361a8(0xf9)]['initialize'][_0x4361a8(0xf8)](this,_0x5dfa2b),this[_0x4361a8(0x175)]['x']=this['scale']['y']=Window_MessageKeywordTooltip[_0x4361a8(0x13e)],this[_0x4361a8(0x10e)](),this['_keyword']='';},Window_MessageKeywordTooltip[_0x1080b1(0xf9)][_0x1080b1(0x142)]=function(){const _0x109e5b=_0x1080b1;this[_0x109e5b(0x177)]=ImageManager[_0x109e5b(0x172)](Window_MessageKeywordTooltip[_0x109e5b(0xe8)]);},Window_MessageKeywordTooltip[_0x1080b1(0xf9)][_0x1080b1(0x16f)]=function(){const _0x1efedb=_0x1080b1;this[_0x1efedb(0x126)]=Window_MessageKeywordTooltip[_0x1efedb(0x10d)];},Window_MessageKeywordTooltip[_0x1080b1(0xf9)][_0x1080b1(0xde)]=function(_0x45c938){const _0x266d79=_0x1080b1;if(this[_0x266d79(0x189)]===_0x45c938)return;if(_0x45c938!==null&&!VisuMZ[_0x266d79(0x13a)]['Keywords'][_0x45c938])return;this[_0x266d79(0x189)]=_0x45c938||'';if(this[_0x266d79(0x189)][_0x266d79(0xf5)]()[_0x266d79(0x131)]>0x0){if(_0x266d79(0x148)===_0x266d79(0x148))this[_0x266d79(0x105)]();else return![];}else this['hide']();},Window_MessageKeywordTooltip[_0x1080b1(0xf9)][_0x1080b1(0x105)]=function(){const _0x27347a=_0x1080b1;this[_0x27347a(0x11b)][_0x27347a(0x167)](),this[_0x27347a(0x14b)]();if(this[_0x27347a(0x182)][_0x27347a(0x131)]>0x0){this[_0x27347a(0xe9)]();const _0x382950=this[_0x27347a(0xec)]();this[_0x27347a(0x11a)](this[_0x27347a(0x182)],_0x382950['x'],_0x382950['y'],_0x382950[_0x27347a(0x12a)]),this[_0x27347a(0x10a)]();}else _0x27347a(0x137)==='bNosd'?this[_0x27347a(0x10e)]():(_0x4f5538['MessageKeywords'][_0x27347a(0x171)]['call'](this,_0x1d743c),this[_0x27347a(0xf3)]());},Window_MessageKeywordTooltip[_0x1080b1(0xf9)]['isWordWrapEnabled']=function(){const _0x5c2717=_0x1080b1;return this[_0x5c2717(0x151)];},Window_MessageKeywordTooltip['prototype']['convertMessageKeywords']=function(_0x752c18){return _0x752c18;},Window_MessageKeywordTooltip[_0x1080b1(0xf9)][_0x1080b1(0x16b)]=function(){return![];},Window_MessageKeywordTooltip[_0x1080b1(0xf9)][_0x1080b1(0x14b)]=function(){const _0x46c091=_0x1080b1;this[_0x46c091(0x182)]='';if(!this[_0x46c091(0x189)])return;this[_0x46c091(0xfd)](),this[_0x46c091(0x182)]=this[_0x46c091(0x182)][_0x46c091(0xf5)]();},Window_MessageKeywordTooltip[_0x1080b1(0xf9)][_0x1080b1(0xfd)]=function(){const _0x31c1e8=_0x1080b1,_0x450e30=VisuMZ['MessageKeywords'][_0x31c1e8(0x184)][this['_keyword']];this[_0x31c1e8(0x182)]=_0x450e30['Tooltip'];},Window_MessageKeywordTooltip[_0x1080b1(0xf9)]['resizeWindow']=function(){const _0x57f32e=_0x1080b1,_0x95afd=this[_0x57f32e(0x13d)](this[_0x57f32e(0x182)]);this[_0x57f32e(0x12a)]=_0x95afd[_0x57f32e(0x12a)]+(this['itemPadding']()+this[_0x57f32e(0x102)])*0x2,this[_0x57f32e(0x18c)]=_0x95afd[_0x57f32e(0x18c)]+this[_0x57f32e(0x102)]*0x2,this[_0x57f32e(0x136)](),this[_0x57f32e(0x10c)]();},Window_MessageKeywordTooltip['prototype'][_0x1080b1(0x12e)]=function(){const _0x468c24=_0x1080b1;Window_Base[_0x468c24(0xf9)][_0x468c24(0x12e)][_0x468c24(0xf8)](this),this['_requestRefresh']&&(this[_0x468c24(0x179)]=![],this[_0x468c24(0x105)]()),this[_0x468c24(0x168)](),this[_0x468c24(0x132)]();},Window_MessageKeywordTooltip[_0x1080b1(0xf9)][_0x1080b1(0xf0)]=function(){const _0x568238=_0x1080b1;this[_0x568238(0x179)]=!![];},Window_MessageKeywordTooltip['prototype'][_0x1080b1(0x168)]=function(){const _0x1796e5=_0x1080b1;if(!this[_0x1796e5(0x116)])return;this['x']=TouchInput['x']+Window_MessageKeywordTooltip['MOUSE_OFFSET_X'],this['y']=TouchInput['y']+Window_MessageKeywordTooltip[_0x1796e5(0x112)],this[_0x1796e5(0xf4)]();},Window_MessageKeywordTooltip[_0x1080b1(0xf9)]['clampPosition']=function(){const _0x2d7515=_0x1080b1,_0x2df0ea=this[_0x2d7515(0x12a)]*(Window_MessageKeywordTooltip[_0x2d7515(0x13e)]||0.01),_0xd3e7ac=this[_0x2d7515(0x18c)]*(Window_MessageKeywordTooltip[_0x2d7515(0x13e)]||0.01);this['x']=Math[_0x2d7515(0x169)](this['x'][_0x2d7515(0x17e)](0x0,Graphics[_0x2d7515(0x12a)]-_0x2df0ea)),this['y']=Math[_0x2d7515(0x169)](this['y'][_0x2d7515(0x17e)](0x0,Graphics['height']-_0xd3e7ac));},Window_MessageKeywordTooltip[_0x1080b1(0xf9)]['updateOpacity']=function(){const _0x9f455=_0x1080b1;let _0x5288f9=0xff;if(TouchInput['x']<=0x0)_0x5288f9=0x0;if(TouchInput['x']>=Graphics['width'])_0x5288f9=0x0;if(TouchInput['y']<=0x0)_0x5288f9=0x0;if(TouchInput['y']>=Graphics['height'])_0x5288f9=0x0;this[_0x9f455(0x188)]=_0x5288f9;};