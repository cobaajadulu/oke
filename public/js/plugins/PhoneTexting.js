//=============================================================================
// PhoneTextingPlugin
//=============================================================================

/*:
 * @plugindesc A plugin for creating a phone texting system in RPG Maker MZ.
 * @author YourName
 *
 * @help
 * This plugin allows you to create a phone texting system with a two-person conversation.
 * Use the following plugin commands to control the phone texting:
 *   OpenPhoneTexting - Opens the phone texting screen.
 *   ClosePhoneTexting - Closes the phone texting screen.
 *   AddTextMessage sender message - Adds a text message to the conversation.
 */

(function() {
    var pluginName = 'PhoneTextingPlugin';

    var phoneTextingWindow = null;
    var conversationTexts = [];

    PluginManager.registerCommand(pluginName, 'OpenPhoneTexting', function() {
        if (!phoneTextingWindow) {
            phoneTextingWindow = new Window_PhoneTexting();
            SceneManager._scene.addWindow(phoneTextingWindow);
        }
    });

    PluginManager.registerCommand(pluginName, 'ClosePhoneTexting', function() {
        if (phoneTextingWindow) {
            phoneTextingWindow.close();
            phoneTextingWindow = null;
        }
    });

    PluginManager.registerCommand(pluginName, 'AddTextMessage', function(args) {
        var sender = args.sender;
        var message = args.message;
        conversationTexts.push({ sender: sender, message: message });
        if (phoneTextingWindow) {
            phoneTextingWindow.refresh();
        }
    });

    function Window_PhoneTexting() {
        this.initialize.apply(this, arguments);
    }

    Window_PhoneTexting.prototype = Object.create(Window_Base.prototype);
    Window_PhoneTexting.prototype.constructor = Window_PhoneTexting;

    Window_PhoneTexting.prototype.initialize = function() {
        var width = Graphics.boxWidth;
        var height = Graphics.boxHeight;
        Window_Base.prototype.initialize.call(this, 0, 0, width, height);
        this.refresh();
    };

    Window_PhoneTexting.prototype.refresh = function() {
        this.contents.clear();
        var y = 0;
        for (var i = 0; i < conversationTexts.length; i++) {
            var text = conversationTexts[i];
            var sender = text.sender;
            var message = text.message;
            this.drawTextEx(sender + ': ' + message, 0, y);
            y += this.lineHeight();
        }
    };

    // Add more methods and properties as needed

})();