//=============================================================================
// VisuStella MZ - Message Log
// VisuMZ_3_MessageLog.js
//=============================================================================

var Imported = Imported || {};
Imported.VisuMZ_3_MessageLog = true;

var VisuMZ = VisuMZ || {};
VisuMZ.MessageLog = VisuMZ.MessageLog || {};
VisuMZ.MessageLog.version = 1.05;

//=============================================================================
 /*:
 * @target MZ
 * @plugindesc [RPG Maker MZ] [Tier 3] [Version 1.05] [MessageLog]
 * @author VisuStella
 * @url http://www.yanfly.moe/wiki/Message_Log_VisuStella_MZ
 * @base VisuMZ_1_MessageCore
 * @orderAfter VisuMZ_1_MessageCore
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * The Message Log plugin will take and record any Show Message entries played
 * on the map screen so that players can go back to them and review them at a
 * later point in time when needed. This is helpful for players who may have
 * missed important information that would have been displayed or those who
 * would like to review what was said previously. The Message Log will not
 * record any of the text displayed in the battle scene in order to preserve
 * the data to one specific scene.
 *
 * Features include all (but not limited to) the following:
 * 
 * * Record messages written out in the "Show Text" command while the player is
 *   on the map screen.
 * * Players can access the Message Log through either the Main Menu or by a
 *   shortcut key whenever the Message Window is open.
 * * Faces and speaker names will also be recorded.
 * * Choice List selections, Number Inputs, and selected Event Items will also
 *   be recorded.
 * * Those using the Extended Message Functionality plugin can also bind this
 *   effect to the Button Console.
 *
 * ============================================================================
 * Requirements
 * ============================================================================
 *
 * This plugin is made for RPG Maker MZ. This will not work in other iterations
 * of RPG Maker.
 *
 * ------ Required Plugin List ------
 *
 * * VisuMZ_1_MessageCore
 *
 * This plugin requires the above listed plugins to be installed inside your
 * game's Plugin Manager list in order to work. You cannot start your game with
 * this plugin enabled without the listed plugins.
 *
 * ------ Tier 3 ------
 *
 * This plugin is a Tier 3 plugin. Place it under other plugins of lower tier
 * value on your Plugin Manager list (ie: 0, 1, 2, 3, 4, 5). This is to ensure
 * that your plugins will have the best compatibility with the rest of the
 * VisuStella MZ library.
 *
 * ============================================================================
 * Major Changes
 * ============================================================================
 *
 * This plugin adds some new hard-coded features to RPG Maker MZ's functions.
 * The following is a list of them.
 *
 * ---
 *
 * Replaced Message Text Codes
 * 
 * Some text codes are not compatible with the Message Log when viewed such as
 * wait commands, showing the gold window, etc. When that happens, those text
 * codes will be removed from visibility in the Message Log in order to prevent
 * any problems. The following is a list of the Message Text Codes that will
 * not appear in the Message Log:
 * 
 *   --------------------
 *   Default RPG Maker MZ
 *   --------------------
 *   \$
 *   \.
 *   \|
 *   \!
 *   \>
 *   \<
 *   \^
 * 
 *   --------------------
 *   VisuMZ_1_MessageCore
 *   --------------------
 *   \Picture<x>
 *   \CenterPicture<x>
 *   \CommonEvent[x]
 *   \Wait[x]
 *   \NormalBG
 *   \DimBG
 *   \TransparentBG
 *   \WindowMoveTo: ?>
 *   \WindowMoveBy: ?>
 *   \WindowReset
 *   \TroopMember[x]
 *   \TroopNameMember[x]
 *   \ChangeFace<?>
 *   \FaceIndex[x]
 *   <Auto>
 *   <Auto Width>
 *   <Auto Height>
 *   <Auto Actor: x>
 *   <Auto Party: x>
 *   <Auto Enemy: x>
 *   <Auto Event: x>
 *   <Auto Player>
 *   <Show>
 *   <Show Switch: x>
 *   <Show All Switches: x,x,x>
 *   <Show Any Switches: x,x,x>
 *   <Hide>
 *   <Hide Switch: x>
 *   <Hide All Switches: x,x,x>
 *   <Hide Any Switches: x,x,x>
 *   <Enable>
 *   <Enable Switch: x>
 *   <Enable All Switches: x,x,x>
 *   <Enable Any Switches: x,x,x>
 *   <Disable>
 *   <Disable Switch: x>
 *   <Disable All Switches: x,x,x>
 *   <Disable Any Switches: x,x,x>
 *   <Position: ?>
 *   <Coordinates: ?>
 *   <Dimensions: ?>
 * 
 *   -----------------------
 *   VisuMZ_2_ExtMessageFunc
 *   -----------------------
 *   <Hide Buttons>
 * 
 *   -----------------------
 *   VisuMZ_2_PictureChoices
 *   -----------------------
 *   <Bind Picture: id>
 *   <Hide Choice Window>
 * 
 *   ----------------------
 *   VisuMZ_3_ChoiceCmnEvts
 *   ----------------------
 *   <Choice Common Event: id>
 * 
 *   -------------------
 *   VisuMZ_3_MessageLog
 *   -------------------
 *   <Bypass Message Log>
 * 
 *   ----------------------
 *   VisuMZ_3_MessageSounds
 *   ----------------------
 *   <Letter Sound On>
 *   <Letter Sound Off>
 *   \LetterSoundName<filename>
 *   \LetterSoundVolume[x]
 *   \LetterSoundPitch[x]
 *   \LetterSoundPan[x]
 *   \LetterSoundVolumeVar[x]
 *   \LetterSoundPitchVar[x]
 *   \LetterSoundPanVar[x]
 *   \LSON
 *   \LSOFF
 *   \LSN<filename>
 *   \LSV[x]
 *   \LSPI[x]
 *   \LSPA[x]
 *   \LSVV[x]
 *   \LSPIV[x]
 *   \LSPAV[x]
 * 
 *   ------------------------
 *   VisuMZ_4_EventTitleScene
 *   ------------------------
 *   <Continue>
 *
 * ---
 *
 * ============================================================================
 * VisuStella MZ Compatibility
 * ============================================================================
 *
 * While this plugin is compatible with the majority of the VisuStella MZ
 * plugin library, it is not compatible with specific plugins or specific
 * features. This section will highlight the main plugins/features that will
 * not be compatible with this plugin or put focus on how the make certain
 * features compatible.
 *
 * ---
 * 
 * VisuMZ_2_ExtMessageFunc
 * 
 * The Extended Message Functionality plugin enables the "Log" button found in
 * the Button Console to let the player go and review the text that has been
 * displayed in the map scene. This does not include the text found in battle
 * to avoid conflicting logged messages across different situations.
 * 
 * ---
 *
 * ============================================================================
 * Available Text Codes
 * ============================================================================
 *
 * The following are text codes that you may use with this plugin. 
 *
 * === Log-Related Text Codes ===
 * 
 * ---
 *
 * --------------------   -----------------------------------------------------
 * Text Code              Effect
 * --------------------   -----------------------------------------------------
 * 
 * <Bypass Message Log>   Prevents the specific "Show Text" window from being
 *                        recorded into the Message Log.
 * 
 * ---
 *
 * ============================================================================
 * Plugin Commands
 * ============================================================================
 *
 * The following are Plugin Commands that come with this plugin. They can be
 * accessed through the Plugin Command event command.
 *
 * ---
 * 
 * === Bypass Plugin Commands ===
 * 
 * ---
 *
 * Bypass: Message Logging?
 * - Bypass message logging until turned off.
 *
 *   Bypass?:
 *   - Bypasses Message Logging until turned off.
 *
 * ---
 * 
 * === System Plugin Commands ===
 * 
 * ---
 *
 * System: Enable Message Log in Menu?
 * - Enables/disables Message Log menu inside the main menu.
 *
 *   Enable/Disable?:
 *   - Enables/disables Message Log menu inside the main menu.
 *
 * ---
 *
 * System: Show Message Log in Menu?
 * - Shows/hides Message Log menu inside the main menu.
 *
 *   Show/Hide?:
 *   - Shows/hides Message Log menu inside the main menu.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: General Settings
 * ============================================================================
 *
 * General Settings for the Message Log.
 *
 * ---
 *
 * Settings
 * 
 *   Entry Limit:
 *   - How many message entries will be stored before the game will start
 *     trimming them?
 * 
 *   Shortcut Key:
 *   - This is the key used for opening the Message Log scene.
 *   - Does not work in battle!
 * 
 *   Show Faces?
 *   - Show face graphics in the Message Log?
 * 
 *   Pad Sides?
 *   - Pad the sides of the screen even without faces?
 *   - Ignore if the screen is too small.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Main Menu Settings
 * ============================================================================
 *
 * Main Menu settings for Message Log.
 *
 * ---
 *
 * Settings
 * 
 *   Command Name:
 *   - Name of the 'Message Log' option in the Main Menu.
 * 
 *   Show in Main Menu?:
 *   - Add the 'Message Log' option to the Main Menu by default?
 * 
 *   Enable in Main Menu?:
 *   - Enable the 'Message Log' option to the Main Menu by default?
 *   - This will be automatically disabled if there are no entries available.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Background Settings
 * ============================================================================
 *
 * Background settings for Scene_MessageLog.
 *
 * ---
 *
 * Settings
 * 
 *   Snapshop Opacity:
 *   - Snapshot opacity for the scene.
 * 
 *   Background 1:
 *   - Filename used for the bottom background image.
 *   - Leave empty if you don't wish to use one.
 * 
 *   Background 2:
 *   - Filename used for the upper background image.
 *   - Leave empty if you don't wish to use one.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Vocabulary Settings
 * ============================================================================
 *
 * These settings let you adjust the text displayed for this plugin.
 *
 * ---
 *
 * ExtMessageFunc
 * 
 *   Button Name:
 *   - How is this option's text displayed in-game?
 *   - Requires VisuMZ_2_ExtMessageFunc!
 *
 * ---
 *
 * Button Assist Window
 * 
 *   Slow Scroll:
 *   - Text used for slow scrolling.
 * 
 *   Fast Scroll:
 *   - Text used for fast scrolling.
 *
 * ---
 *
 * Choice Window Logging
 * 
 *   Text Format:
 *   - Text format for logging the selected choice text.
 *   - %1 - Selected Choice Text
 * 
 *   Cancel:
 *   - Text used when cancel branch is selected.
 *
 * ---
 *
 * Number Input Logging
 * 
 *   Text Format:
 *   - Text format for logging the inputted number value.
 *   - %1 - Number Value
 *
 * ---
 *
 * Event Item Logging
 * 
 *   Text Format:
 *   - Text format for logging the selected event Item.
 *   - %1 - Selected Event Item Text
 * 
 *   Name Format:
 *   - Text format for how item names are displayed.
 *   - %1 - Item Icon, %2 - Item Name
 * 
 *   No Item:
 *   - Text used when no item is selected.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Window Settings
 * ============================================================================
 *
 * Window settings for Scene_MessageLog.
 *
 * ---
 *
 * Message Log Window
 * 
 *   Background Type:
 *   - Select background type for this window.
 * 
 *   JS: X, Y, W, H:
 *   - Code used to determine the dimensions for this window.
 *
 * ---
 *
 * Appearance
 * 
 *   Speaker Name X:
 *   - What X coordinate do you want the speaker name to appear at?
 *
 * ---
 *
 * Color Lock
 * 
 *   Choices:
 *   - Color lock the logged choices?
 * 
 *   Number Inputs:
 *   - Color lock the logged Number Inputs?
 * 
 *   Event Item:
 *   - Color lock the logged selected Event Item?
 *
 * ---
 *
 * Scrolling > Slow
 * 
 *   Scroll Speed:
 *   - What speed will Up/Down scroll the window at?
 *   - Lower is slower. Higher is faster.
 * 
 *   Sound Frequency:
 *   - How frequent will Up/Down scrolling make sounds?
 *   - Lower is quicker. Higher is later.
 *
 * ---
 *
 * Scrolling > Fast
 * 
 *   Scroll Speed:
 *   - What speed will PageUp/PageDn scroll the window at?
 *   - Lower is slower. Higher is faster.
 * 
 *   Sound Frequency:
 *   - How frequent will PageUp/PageDn scrolling make sounds?
 *   - Lower is quicker. Higher is later.
 *
 * ---
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 *
 * 1. These plugins may be used in free or commercial games provided that they
 * have been acquired through legitimate means at VisuStella.com and/or any
 * other official approved VisuStella sources. Exceptions and special
 * circumstances that may prohibit usage will be listed on VisuStella.com.
 * 
 * 2. All of the listed coders found in the Credits section of this plugin must
 * be given credit in your games or credited as a collective under the name:
 * "VisuStella".
 * 
 * 3. You may edit the source code to suit your needs, so long as you do not
 * claim the source code belongs to you. VisuStella also does not take
 * responsibility for the plugin if any changes have been made to the plugin's
 * code, nor does VisuStella take responsibility for user-provided custom code
 * used for custom control effects including advanced JavaScript notetags
 * and/or plugin parameters that allow custom JavaScript code.
 * 
 * 4. You may NOT redistribute these plugins nor take code from this plugin to
 * use as your own. These plugins and their code are only to be downloaded from
 * VisuStella.com and other official/approved VisuStella sources. A list of
 * official/approved sources can also be found on VisuStella.com.
 *
 * 5. VisuStella is not responsible for problems found in your game due to
 * unintended usage, incompatibility problems with plugins outside of the
 * VisuStella MZ library, plugin versions that aren't up to date, nor
 * responsible for the proper working of compatibility patches made by any
 * third parties. VisuStella is not responsible for errors caused by any
 * user-provided custom code used for custom control effects including advanced
 * JavaScript notetags and/or plugin parameters that allow JavaScript code.
 *
 * 6. If a compatibility patch needs to be made through a third party that is
 * unaffiliated with VisuStella that involves using code from the VisuStella MZ
 * library, contact must be made with a member from VisuStella and have it
 * approved. The patch would be placed on VisuStella.com as a free download
 * to the public. Such patches cannot be sold for monetary gain, including
 * commissions, crowdfunding, and/or donations.
 * 
 * 7. If this VisuStella MZ plugin is a paid product, all project team members
 * must purchase their own individual copies of the paid product if they are to
 * use it. Usage includes working on related game mechanics, managing related
 * code, and/or using related Plugin Commands and features. Redistribution of
 * the plugin and/or its code to other members of the team is NOT allowed
 * unless they own the plugin itself as that conflicts with Article 4.
 * 
 * 8. Any extensions and/or addendums made to this plugin's Terms of Use can be
 * found on VisuStella.com and must be followed.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * 
 * If you are using this plugin, credit the following people in your game:
 * 
 * Team VisuStella
 * * Irina
 * * Trihan
 * * Arisu
 * * Olivia
 * * Yanfly
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 * 
 * Version 1.05: September 14, 2023
 * * Bug Fixes!
 * ** Fixed a bug where text macros from VisuMZ Message Core did not convert
 *    properly into the Message Log. Fix made by Irina.
 * * Documentation Update!
 * ** Help file updated for new features.
 * * New Features!
 * ** New Plugin Parameter added by Irina.
 * ** Plugin Parameters > General > Pad Sides?
 * *** Pad the sides of the screen even without faces?
 * *** Ignore if the screen is too small.
 * 
 * Version 1.04: March 16, 2023
 * * Compatibility Update!
 * ** Added compatibility for the recent Message Core additions.
 * ** Added compatibility functionality for future plugins.
 * 
 * Version 1.03: October 7, 2021
 * * Bug Fixes!
 * ** Message Log should now work with automatic word wrap. Fix by Irina.
 * 
 * Version 1.02: September 3, 2021
 * * Bug Fixes!
 * ** Fixed a crash pertaining to specific message windows that haven't
 *    declared a speaker name from an older RPG Maker version. Fix by Irina.
 * 
 * Version 1.01: August 6, 2021
 * * Documentation Update!
 * ** Plugin URL now updated to most recent one.
 *
 * Version 1.00 Official Release Date: August 4, 2021
 * * Finished Plugin!
 *
 * ============================================================================
 * End of Helpfile
 * ============================================================================
 *
 * @ --------------------------------------------------------------------------
 *
 * @command BypassMessageLogging
 * @text Bypass: Message Logging?
 * @desc Bypass message logging until turned off.
 *
 * @arg Bypass:eval
 * @text Bypass?
 * @type boolean
 * @on Bypass
 * @off Enable
 * @desc Bypasses Message Logging until turned off.
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SystemEnableMessageLogMenu
 * @text System: Enable Message Log in Menu?
 * @desc Enables/disables Message Log menu inside the main menu.
 *
 * @arg Enable:eval
 * @text Enable/Disable?
 * @type boolean
 * @on Enable
 * @off Disable
 * @desc Enables/disables Message Log menu inside the main menu.
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SystemShowMessageLogMenu
 * @text System: Show Message Log in Menu?
 * @desc Shows/hides Message Log menu inside the main menu.
 *
 * @arg Show:eval
 * @text Show/Hide?
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Shows/hides Message Log menu inside the main menu.
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @ ==========================================================================
 * @ Plugin Parameters
 * @ ==========================================================================
 *
 * @param BreakHead
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param MessageLog
 * @default Plugin Parameters
 *
 * @param ATTENTION
 * @default READ THE HELP FILE
 *
 * @param BreakSettings
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param General:struct
 * @text General Settings
 * @type struct<General>
 * @desc General Settings for the Message Log.
 * @default {"EntryLimit:num":"50","ShortcutKey:str":"pageup","ShowFaces:eval":"true"}
 *
 * @param MainMenu:struct
 * @text Main Menu Settings
 * @type struct<MainMenu>
 * @desc Main Menu settings for Message Log.
 * @default {"Name:str":"Message Log","ShowMainMenu:eval":"true","EnableMainMenu:eval":"true"}
 *
 * @param BgSettings:struct
 * @text Background Settings
 * @type struct<BgSettings>
 * @desc Background settings for Scene_MessageLog.
 * @default {"SnapshotOpacity:num":"192","BgFilename1:str":"","BgFilename2:str":""}
 *
 * @param Vocab:struct
 * @text Vocabulary Settings
 * @type struct<Vocab>
 * @desc These settings let you adjust the text displayed for this plugin.
 * @default {"ExtMessageFunc":"","ButtonName:str":"LOG","ButtonAssist":"","SlowScroll:str":"Scroll","FastScroll:str":"Fast Scroll","ChoiceLogging":"","ChoiceFmt:str":"\\C[4]Choice >\\C[0] %1","ChoiceCancel:str":"Cancel","NumberLogging":"","NumberFmt:str":"\\C[4]Amount >\\C[0] %1","EventItemLogging":"","ItemFmt:str":"\\C[4]Choice >\\C[0] %1","ItemNameFmt:str":"%1%2","NoItem:str":"Nothing"}
 *
 * @param Window:struct
 * @text Window Settings
 * @type struct<Window>
 * @desc Window settings for Scene_MessageLog.
 * @default {"MessageLogWindow":"","MessageLogMenu_BgType:num":"0","MessageLogMenu_RectJS:func":"\"const wx = 0;\\nconst wy = this.mainAreaTop();\\nconst ww = Graphics.boxWidth;\\nconst wh = this.mainAreaHeight();\\n\\nreturn new Rectangle(wx, wy, ww, wh);\"","Appearance":"","SpeakerNameX:num":"128","ColorLock":"","ColorLockChoice:eval":"false","ColorLockNumber:eval":"true","ColorLockItem:eval":"true","Scrolling":"","Slow":"","SlowScrollSpeed:num":"8","SlowSoundFreq:num":"8","Fast":"","FastScrollSpeed:num":"32","FastSoundFreq:num":"4"}
 *
 * @param BreakEnd1
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param End Of
 * @default Plugin Parameters
 *
 * @param BreakEnd2
 * @text --------------------------
 * @default ----------------------------------
 *
 */
/* ----------------------------------------------------------------------------
 * General Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~General:
 *
 * @param EntryLimit:num
 * @text Entry Limit
 * @parent General
 * @type number
 * @min 1
 * @max 999
 * @desc How many message entries will be stored before the game
 * will start trimming them?
 * @default 50
 *
 * @param ShortcutKey:str
 * @text Shortcut Key
 * @parent General
 * @type combo
 * @option none
 * @option tab
 * @option shift
 * @option control
 * @option pageup
 * @option pagedown
 * @desc This is the key used for opening the Message Log scene.
 * Does not work in battle!
 * @default pageup
 *
 * @param ShowFaces:eval
 * @text Show Faces?
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show face graphics in the Message Log?
 * @default true
 *
 * @param PadSides:eval
 * @text Pad Sides?
 * @type boolean
 * @on Pad
 * @off Don't Pad
 * @desc Pad the sides of the screen even without faces?
 * Ignore if the screen is too small.
 * @default true
 *
 */
/* ----------------------------------------------------------------------------
 * MainMenu Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~MainMenu:
 *
 * @param Name:str
 * @text Command Name
 * @parent Options
 * @desc Name of the 'Message Log' option in the Main Menu.
 * @default Message Log
 *
 * @param ShowMainMenu:eval
 * @text Show in Main Menu?
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Add the 'Message Log' option to the Main Menu by default?
 * @default true
 *
 * @param EnableMainMenu:eval
 * @text Enable in Main Menu?
 * @type boolean
 * @on Enable
 * @off Disable
 * @desc Enable the 'Message Log' option to the Main Menu by default?
 * @default true
 *
 */
/* ----------------------------------------------------------------------------
 * Background Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~BgSettings:
 *
 * @param SnapshotOpacity:num
 * @text Snapshop Opacity
 * @type number
 * @min 0
 * @max 255
 * @desc Snapshot opacity for the scene.
 * @default 192
 *
 * @param BgFilename1:str
 * @text Background 1
 * @type file
 * @dir img/titles1/
 * @require 1
 * @desc Filename used for the bottom background image.
 * Leave empty if you don't wish to use one.
 * @default 
 *
 * @param BgFilename2:str
 * @text Background 2
 * @type file
 * @dir img/titles2/
 * @require 1
 * @desc Filename used for the upper background image.
 * Leave empty if you don't wish to use one.
 * @default 
 *
 */
/* ----------------------------------------------------------------------------
 * Vocabulary Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Vocab:
 * 
 * @param ExtMessageFunc
 *
 * @param ButtonName:str
 * @text Button Name
 * @parent ExtMessageFunc
 * @desc How is this option's text displayed in-game?
 * Requires VisuMZ_2_ExtMessageFunc!
 * @default LOG
 *
 * @param ButtonAssist
 * @text Button Assist Window
 *
 * @param SlowScroll:str
 * @text Slow Scroll
 * @parent ButtonAssist
 * @desc Text used for slow scrolling.
 * @default Scroll
 *
 * @param FastScroll:str
 * @text Fast Scroll
 * @parent ButtonAssist
 * @desc Text used for fast scrolling.
 * @default Fast Scroll
 *
 * @param ChoiceLogging
 * @text Choice Window Logging
 *
 * @param ChoiceFmt:str
 * @text Text Format
 * @parent ChoiceLogging
 * @desc Text format for logging the selected choice text.
 * %1 - Selected Choice Text
 * @default \C[4]Choice >\C[0] %1
 *
 * @param ChoiceCancel:str
 * @text Cancel
 * @parent ChoiceLogging
 * @desc Text used when cancel branch is selected.
 * @default Cancel
 *
 * @param NumberLogging
 * @text Number Input Logging
 *
 * @param NumberFmt:str
 * @text Text Format
 * @parent NumberLogging
 * @desc Text format for logging the inputted number value.
 * %1 - Number Value
 * @default \C[4]Amount >\C[0] %1
 *
 * @param EventItemLogging
 * @text Event Item Logging
 *
 * @param ItemFmt:str
 * @text Text Format
 * @parent EventItemLogging
 * @desc Text format for logging the selected event Item.
 * %1 - Selected Event Item Text
 * @default \C[4]Choice >\C[0] %1
 *
 * @param ItemNameFmt:str
 * @text Name Format
 * @parent EventItemLogging
 * @desc Text format for how item names are displayed.
 * %1 - Item Icon, %2 - Item Name
 * @default %1%2
 *
 * @param NoItem:str
 * @text No Item
 * @parent EventItemLogging
 * @desc Text used when no item is selected.
 * @default Nothing
 *
 */
/* ----------------------------------------------------------------------------
 * Window Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Window:
 * 
 * @param MessageLogWindow
 * @text Message Log Window
 *
 * @param MessageLogMenu_BgType:num
 * @text Background Type
 * @parent MessageLogWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param MessageLogMenu_RectJS:func
 * @text JS: X, Y, W, H
 * @parent MessageLogWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const wx = 0;\nconst wy = this.mainAreaTop();\nconst ww = Graphics.boxWidth;\nconst wh = this.mainAreaHeight();\n\nreturn new Rectangle(wx, wy, ww, wh);"
 *
 * @param Appearance
 *
 * @param SpeakerNameX:num
 * @text Speaker Name X
 * @parent Appearance
 * @type number
 * @min 0
 * @desc What X coordinate do you want the speaker name to appear at?
 * @default 128
 *
 * @param ColorLock
 * @text Color Lock
 *
 * @param ColorLockChoice:eval
 * @text Choices
 * @parent ColorLock
 * @type boolean
 * @on Color Lock
 * @off Don't Color Lock
 * @desc Color lock the logged choices?
 * @default false
 *
 * @param ColorLockNumber:eval
 * @text Number Inputs
 * @parent ColorLock
 * @type boolean
 * @on Color Lock
 * @off Don't Color Lock
 * @desc Color lock the logged Number Inputs?
 * @default true
 *
 * @param ColorLockItem:eval
 * @text Event Item
 * @parent ColorLock
 * @type boolean
 * @on Color Lock
 * @off Don't Color Lock
 * @desc Color lock the logged selected Event Item?
 * @default true
 *
 * @param Scrolling
 *
 * @param Slow
 * @parent Scrolling
 *
 * @param SlowScrollSpeed:num
 * @text Scroll Speed
 * @parent Slow
 * @type number
 * @min 1
 * @desc What speed will Up/Down scroll the window at?
 * Lower is slower. Higher is faster.
 * @default 8
 *
 * @param SlowSoundFreq:num
 * @text Sound Frequency
 * @parent Slow
 * @type number
 * @min 1
 * @desc How frequent will Up/Down scrolling make sounds?
 * Lower is quicker. Higher is later.
 * @default 8
 *
 * @param Fast
 * @parent Scrolling
 *
 * @param FastScrollSpeed:num
 * @text Scroll Speed
 * @parent Fast
 * @type number
 * @min 1
 * @desc What speed will PageUp/PageDn scroll the window at?
 * Lower is slower. Higher is faster.
 * @default 32
 *
 * @param FastSoundFreq:num
 * @text Sound Frequency
 * @parent Fast
 * @type number
 * @min 1
 * @desc How frequent will PageUp/PageDn scrolling make sounds?
 * Lower is quicker. Higher is later.
 * @default 4
 *
 */
//=============================================================================

function _0x54b6(_0x23e945,_0x5bb7d1){const _0x24efd4=_0x24ef();return _0x54b6=function(_0x54b6da,_0x441c0f){_0x54b6da=_0x54b6da-0x1cb;let _0x561556=_0x24efd4[_0x54b6da];return _0x561556;},_0x54b6(_0x23e945,_0x5bb7d1);}const _0x2d49ad=_0x54b6;(function(_0x4d9d2a,_0x3daa0d){const _0x3674c1=_0x54b6,_0x171093=_0x4d9d2a();while(!![]){try{const _0x435162=parseInt(_0x3674c1(0x1e3))/0x1*(parseInt(_0x3674c1(0x1d6))/0x2)+-parseInt(_0x3674c1(0x24d))/0x3*(parseInt(_0x3674c1(0x228))/0x4)+-parseInt(_0x3674c1(0x2bd))/0x5*(-parseInt(_0x3674c1(0x2f6))/0x6)+parseInt(_0x3674c1(0x251))/0x7*(-parseInt(_0x3674c1(0x1ef))/0x8)+parseInt(_0x3674c1(0x2cc))/0x9*(parseInt(_0x3674c1(0x1f8))/0xa)+-parseInt(_0x3674c1(0x1f2))/0xb+-parseInt(_0x3674c1(0x2a5))/0xc*(-parseInt(_0x3674c1(0x21f))/0xd);if(_0x435162===_0x3daa0d)break;else _0x171093['push'](_0x171093['shift']());}catch(_0x5904ae){_0x171093['push'](_0x171093['shift']());}}}(_0x24ef,0xa181f));var label=_0x2d49ad(0x2de),tier=tier||0x0,dependencies=[],pluginData=$plugins[_0x2d49ad(0x27f)](function(_0x2ebbe5){const _0x469fbb=_0x2d49ad;return _0x2ebbe5['status']&&_0x2ebbe5[_0x469fbb(0x2ee)][_0x469fbb(0x230)]('['+label+']');})[0x0];VisuMZ[label]['Settings']=VisuMZ[label][_0x2d49ad(0x256)]||{},VisuMZ['ConvertParams']=function(_0x308d24,_0x486d7b){const _0x48120c=_0x2d49ad;for(const _0xdc9a10 in _0x486d7b){if(_0xdc9a10[_0x48120c(0x2f2)](/(.*):(.*)/i)){if(_0x48120c(0x1df)===_0x48120c(0x1df)){const _0x36c21f=String(RegExp['$1']),_0x269626=String(RegExp['$2'])['toUpperCase']()['trim']();let _0x40b52d,_0x5d48e1,_0x2255b6;switch(_0x269626){case _0x48120c(0x305):_0x40b52d=_0x486d7b[_0xdc9a10]!==''?Number(_0x486d7b[_0xdc9a10]):0x0;break;case _0x48120c(0x1d9):_0x5d48e1=_0x486d7b[_0xdc9a10]!==''?JSON['parse'](_0x486d7b[_0xdc9a10]):[],_0x40b52d=_0x5d48e1[_0x48120c(0x297)](_0x54c8d0=>Number(_0x54c8d0));break;case _0x48120c(0x203):_0x40b52d=_0x486d7b[_0xdc9a10]!==''?eval(_0x486d7b[_0xdc9a10]):null;break;case _0x48120c(0x2ac):_0x5d48e1=_0x486d7b[_0xdc9a10]!==''?JSON[_0x48120c(0x291)](_0x486d7b[_0xdc9a10]):[],_0x40b52d=_0x5d48e1[_0x48120c(0x297)](_0x1b6301=>eval(_0x1b6301));break;case'JSON':_0x40b52d=_0x486d7b[_0xdc9a10]!==''?JSON[_0x48120c(0x291)](_0x486d7b[_0xdc9a10]):'';break;case'ARRAYJSON':_0x5d48e1=_0x486d7b[_0xdc9a10]!==''?JSON[_0x48120c(0x291)](_0x486d7b[_0xdc9a10]):[],_0x40b52d=_0x5d48e1[_0x48120c(0x297)](_0x37c64e=>JSON['parse'](_0x37c64e));break;case _0x48120c(0x2f8):_0x40b52d=_0x486d7b[_0xdc9a10]!==''?new Function(JSON[_0x48120c(0x291)](_0x486d7b[_0xdc9a10])):new Function(_0x48120c(0x266));break;case _0x48120c(0x1dd):_0x5d48e1=_0x486d7b[_0xdc9a10]!==''?JSON['parse'](_0x486d7b[_0xdc9a10]):[],_0x40b52d=_0x5d48e1[_0x48120c(0x297)](_0x59fae6=>new Function(JSON[_0x48120c(0x291)](_0x59fae6)));break;case _0x48120c(0x1de):_0x40b52d=_0x486d7b[_0xdc9a10]!==''?String(_0x486d7b[_0xdc9a10]):'';break;case'ARRAYSTR':_0x5d48e1=_0x486d7b[_0xdc9a10]!==''?JSON[_0x48120c(0x291)](_0x486d7b[_0xdc9a10]):[],_0x40b52d=_0x5d48e1[_0x48120c(0x297)](_0x245b79=>String(_0x245b79));break;case'STRUCT':_0x2255b6=_0x486d7b[_0xdc9a10]!==''?JSON['parse'](_0x486d7b[_0xdc9a10]):{},_0x40b52d=VisuMZ['ConvertParams']({},_0x2255b6);break;case _0x48120c(0x2c4):_0x5d48e1=_0x486d7b[_0xdc9a10]!==''?JSON[_0x48120c(0x291)](_0x486d7b[_0xdc9a10]):[],_0x40b52d=_0x5d48e1['map'](_0x56c630=>VisuMZ['ConvertParams']({},JSON[_0x48120c(0x291)](_0x56c630)));break;default:continue;}_0x308d24[_0x36c21f]=_0x40b52d;}else this[_0x48120c(0x1d8)](!![]);}}return _0x308d24;},(_0x3f115d=>{const _0x11d981=_0x2d49ad,_0x24667e=_0x3f115d['name'];for(const _0x6bc6c4 of dependencies){if(!Imported[_0x6bc6c4]){alert(_0x11d981(0x2f7)[_0x11d981(0x2be)](_0x24667e,_0x6bc6c4)),SceneManager[_0x11d981(0x255)]();break;}}const _0x3a9880=_0x3f115d[_0x11d981(0x2ee)];if(_0x3a9880[_0x11d981(0x2f2)](/\[Version[ ](.*?)\]/i)){const _0x200835=Number(RegExp['$1']);_0x200835!==VisuMZ[label][_0x11d981(0x30a)]&&(alert(_0x11d981(0x2ca)[_0x11d981(0x2be)](_0x24667e,_0x200835)),SceneManager['exit']());}if(_0x3a9880[_0x11d981(0x2f2)](/\[Tier[ ](\d+)\]/i)){if(_0x11d981(0x1d1)!=='cqxJN')return 0x0;else{const _0x39e870=Number(RegExp['$1']);_0x39e870<tier?(alert(_0x11d981(0x272)['format'](_0x24667e,_0x39e870,tier)),SceneManager[_0x11d981(0x255)]()):tier=Math[_0x11d981(0x2fb)](_0x39e870,tier);}}VisuMZ[_0x11d981(0x2fc)](VisuMZ[label]['Settings'],_0x3f115d[_0x11d981(0x2fa)]);})(pluginData),PluginManager[_0x2d49ad(0x29a)](pluginData[_0x2d49ad(0x1e7)],_0x2d49ad(0x287),_0x113522=>{const _0x21af85=_0x2d49ad;VisuMZ[_0x21af85(0x2fc)](_0x113522,_0x113522),$gameSystem[_0x21af85(0x288)](_0x113522[_0x21af85(0x26e)]);}),PluginManager['registerCommand'](pluginData[_0x2d49ad(0x1e7)],_0x2d49ad(0x293),_0x5e7f08=>{const _0x39dcfd=_0x2d49ad;VisuMZ[_0x39dcfd(0x2fc)](_0x5e7f08,_0x5e7f08),$gameSystem['setMainMenuMessageLogEnabled'](_0x5e7f08[_0x39dcfd(0x303)]);}),PluginManager['registerCommand'](pluginData[_0x2d49ad(0x1e7)],_0x2d49ad(0x30c),_0x21241d=>{const _0x28aa65=_0x2d49ad;VisuMZ[_0x28aa65(0x2fc)](_0x21241d,_0x21241d),$gameSystem['setMainMenuMessageLogVisible'](_0x21241d[_0x28aa65(0x221)]);}),TextManager['MessageLogMenuCommand']=VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x256)][_0x2d49ad(0x2d3)][_0x2d49ad(0x1e5)],TextManager[_0x2d49ad(0x250)]=VisuMZ['MessageLog']['Settings'][_0x2d49ad(0x1da)]['ButtonName'],TextManager[_0x2d49ad(0x2d0)]=VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x256)][_0x2d49ad(0x1da)][_0x2d49ad(0x21b)],TextManager[_0x2d49ad(0x1e8)]=VisuMZ['MessageLog'][_0x2d49ad(0x256)][_0x2d49ad(0x1da)][_0x2d49ad(0x1fb)],TextManager[_0x2d49ad(0x2af)]=VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x256)][_0x2d49ad(0x1da)]['ChoiceFmt'],TextManager[_0x2d49ad(0x20b)]=VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x256)][_0x2d49ad(0x1da)][_0x2d49ad(0x2b0)],TextManager[_0x2d49ad(0x2f4)]=VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x256)]['Vocab'][_0x2d49ad(0x235)],TextManager[_0x2d49ad(0x2a0)]=VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x256)][_0x2d49ad(0x1da)][_0x2d49ad(0x1eb)],TextManager[_0x2d49ad(0x209)]=VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x256)][_0x2d49ad(0x1da)]['ItemNameFmt'],TextManager[_0x2d49ad(0x212)]=VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x256)][_0x2d49ad(0x1da)][_0x2d49ad(0x1ec)];TextManager[_0x2d49ad(0x1e6)]&&(VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x2a1)]=TextManager[_0x2d49ad(0x1e6)],TextManager[_0x2d49ad(0x1e6)]=function(_0x4d77a8){const _0x17df00=_0x2d49ad;if([_0x17df00(0x2e7),_0x17df00(0x1f1)]['includes'](_0x4d77a8)){if('lQjuN'!==_0x17df00(0x1ea))this[_0x17df00(0x306)]=_0x4820c3[_0x17df00(0x1cb)](_0x50c6d1[_0x17df00(0x2c2)]),_0x31deb8[_0x17df00(0x271)][_0x17df00(0x280)][_0x17df00(0x2e5)](this,_0x3d9ea7),this['_allTextHeight']=0x0,this[_0x17df00(0x275)](),this[_0x17df00(0x2c8)]();else return TextManager[_0x17df00(0x250)];}return VisuMZ[_0x17df00(0x2de)]['TextManager_msgButtonConsole'][_0x17df00(0x2e5)](this,_0x4d77a8);});;SceneManager[_0x2d49ad(0x2f9)]=function(){const _0x20719f=_0x2d49ad;return this[_0x20719f(0x29c)]&&this[_0x20719f(0x29c)][_0x20719f(0x253)]===Scene_Map;},VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x239)]=SceneManager['push'],SceneManager[_0x2d49ad(0x2d1)]=function(_0x5f3ba1){const _0x3e5cdd=_0x2d49ad;_0x5f3ba1===Scene_MessageLog&&this['prepareMessageLogFaces'](),VisuMZ[_0x3e5cdd(0x2de)][_0x3e5cdd(0x239)][_0x3e5cdd(0x2e5)](this,_0x5f3ba1);},SceneManager[_0x2d49ad(0x1fc)]=function(){const _0x4cda1c=_0x2d49ad,_0x2c67ce=$gameSystem[_0x4cda1c(0x2bc)]();for(const _0x484f69 of _0x2c67ce){if(!_0x484f69)continue;const _0x3d50d0=_0x484f69[_0x4cda1c(0x1fa)];_0x3d50d0!==''&&ImageManager[_0x4cda1c(0x30f)](_0x3d50d0);}},VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x1d4)]=Game_System[_0x2d49ad(0x271)][_0x2d49ad(0x280)],Game_System['prototype'][_0x2d49ad(0x280)]=function(){const _0x24064f=_0x2d49ad;VisuMZ[_0x24064f(0x2de)][_0x24064f(0x1d4)]['call'](this),this[_0x24064f(0x270)](),this[_0x24064f(0x260)]();},Game_System[_0x2d49ad(0x271)][_0x2d49ad(0x270)]=function(){const _0x236a3a=_0x2d49ad;this[_0x236a3a(0x2cf)]={'shown':VisuMZ['MessageLog'][_0x236a3a(0x256)]['MainMenu'][_0x236a3a(0x1f7)],'enabled':VisuMZ[_0x236a3a(0x2de)]['Settings'][_0x236a3a(0x2d3)][_0x236a3a(0x308)]},this[_0x236a3a(0x22f)]=![];},Game_System[_0x2d49ad(0x271)][_0x2d49ad(0x247)]=function(){const _0x37a50b=_0x2d49ad;if(this[_0x37a50b(0x2cf)]===undefined)this[_0x37a50b(0x270)]();return this[_0x37a50b(0x2cf)][_0x37a50b(0x26b)];},Game_System[_0x2d49ad(0x271)][_0x2d49ad(0x2c1)]=function(_0x406ff5){const _0x1ff3dd=_0x2d49ad;if(this[_0x1ff3dd(0x2cf)]===undefined)this[_0x1ff3dd(0x270)]();this[_0x1ff3dd(0x2cf)][_0x1ff3dd(0x26b)]=_0x406ff5;},Game_System[_0x2d49ad(0x271)]['isMainMenuMessageLogEnabled']=function(){const _0x2db8c7=_0x2d49ad;if(this[_0x2db8c7(0x2cf)]===undefined)this[_0x2db8c7(0x270)]();if(this[_0x2db8c7(0x2bc)]()['length']<=0x0)return![];return this[_0x2db8c7(0x2cf)][_0x2db8c7(0x2db)];},Game_System[_0x2d49ad(0x271)][_0x2d49ad(0x29b)]=function(_0x18853d){const _0x214d98=_0x2d49ad;if(this['_MessageLog_MainMenu']===undefined)this[_0x214d98(0x270)]();this[_0x214d98(0x2cf)][_0x214d98(0x2db)]=_0x18853d;},Game_System[_0x2d49ad(0x271)]['isBypassMessageLogging']=function(){const _0x1ec6c2=_0x2d49ad;if(this[_0x1ec6c2(0x22f)]===undefined)this[_0x1ec6c2(0x270)]();return this[_0x1ec6c2(0x22f)];},Game_System[_0x2d49ad(0x271)]['setBypassMessageLogging']=function(_0xd4d1c1){const _0x25e4a9=_0x2d49ad;if(this['_MessageLog_Bypass']===undefined)this[_0x25e4a9(0x270)]();this[_0x25e4a9(0x22f)]=_0xd4d1c1;},Game_System[_0x2d49ad(0x271)][_0x2d49ad(0x260)]=function(){const _0x4d64c0=_0x2d49ad;this['_loggedMessages']=[],this[_0x4d64c0(0x1f4)]();},Game_System[_0x2d49ad(0x271)][_0x2d49ad(0x2bc)]=function(){const _0x2877fc=_0x2d49ad;return this[_0x2877fc(0x1f3)]===undefined&&this['initMessageLogSettings'](),this[_0x2877fc(0x1f3)];},Game_System[_0x2d49ad(0x271)]['createNewLoggedMessageEntry']=function(){const _0x17258a=_0x2d49ad;this[_0x17258a(0x20d)]={'speaker':'','faceName':'','faceIndex':0x0,'messageBody':''};},Game_System['prototype'][_0x2d49ad(0x2e9)]=function(){const _0x4094a1=_0x2d49ad,_0x4472a0=this['getLoggedMessages'](),_0x124bfd=this[_0x4094a1(0x2d2)]();if(this[_0x4094a1(0x229)]())return;_0x124bfd[_0x4094a1(0x2a6)]=_0x124bfd[_0x4094a1(0x2a6)]||'';if(_0x124bfd['messageBody']['match'](/<BYPASS MESSAGE LOG>/i))return;if(_0x124bfd[_0x4094a1(0x2a6)]['trim']()[_0x4094a1(0x241)]<=0x0)return;const _0x2f5a20=_0x4472a0[_0x4472a0[_0x4094a1(0x241)]-0x1];if(JSON['stringify'](_0x124bfd)===JSON[_0x4094a1(0x243)](_0x2f5a20))return;_0x4472a0[_0x4094a1(0x2d1)](_0x124bfd);while(_0x4472a0[_0x4094a1(0x241)]>Window_MessageLog[_0x4094a1(0x2da)]){if(_0x4094a1(0x21c)===_0x4094a1(0x21c))_0x4472a0['shift']();else{_0x1bccbf[_0x4094a1(0x1f4)]();const _0x5b0d47=_0x2843f3[_0x4094a1(0x2a0)];let _0x547260='';if(_0x1d7ea7){let _0x965c1e=_0x5b66da[_0x4094a1(0x209)],_0x59b518=_0x965c1e[_0x4094a1(0x2be)](_0x4094a1(0x27c)['format'](_0x46cfd6[_0x4094a1(0x23c)]),_0x3d5570[_0x4094a1(0x1e7)]);_0x14de05['COLOR_LOCK_ITEM']&&(_0x59b518='<ColorLock>%1</ColorLock>'[_0x4094a1(0x2be)](_0x59b518)),_0x547260=_0x5b0d47[_0x4094a1(0x2be)](_0x59b518);}else _0x547260=_0x5b0d47[_0x4094a1(0x2be)](_0x56412f[_0x4094a1(0x212)]);_0x2c3bb4[_0x4094a1(0x23e)](_0x547260),_0x40ed7c[_0x4094a1(0x2e9)]();}}},Game_System[_0x2d49ad(0x271)][_0x2d49ad(0x2d2)]=function(){const _0x5ef5cb=_0x2d49ad;if(this[_0x5ef5cb(0x20d)]===undefined){if(_0x5ef5cb(0x2b9)===_0x5ef5cb(0x2b9))this[_0x5ef5cb(0x1f4)]();else{if([_0x5ef5cb(0x2e7),'log'][_0x5ef5cb(0x230)](_0x6cc639))return _0x43b64a['MessageLogButtonName'];return _0x5407c3[_0x5ef5cb(0x2de)][_0x5ef5cb(0x2a1)]['call'](this,_0x4661ba);}}return this[_0x5ef5cb(0x20d)];},Game_System[_0x2d49ad(0x271)][_0x2d49ad(0x23e)]=function(_0x369c45){const _0x2db2db=_0x2d49ad,_0x14085b=this[_0x2db2db(0x2d2)]();this['isMessageWindowWordWrap']()&&(_0x2db2db(0x20c)===_0x2db2db(0x2b6)?_0x24ab47[_0x2db2db(0x30f)](_0x351107):_0x369c45=_0x2db2db(0x309)+_0x369c45);_0x369c45=this[_0x2db2db(0x289)](_0x369c45);Imported[_0x2db2db(0x2e6)]&&(_0x369c45=this['convertVoiceActControlLines'](_0x369c45,_0x14085b));if(_0x14085b[_0x2db2db(0x2a6)][_0x2db2db(0x241)]>0x0)_0x14085b['messageBody']+='\x0a';_0x14085b['messageBody']+=_0x369c45;},Game_System['prototype'][_0x2d49ad(0x289)]=function(_0x1cd4fb){const _0x33172a=_0x2d49ad;return _0x1cd4fb=this['convertMessageLogTextMacros'](_0x1cd4fb),_0x1cd4fb=this[_0x33172a(0x2a3)](_0x1cd4fb),_0x1cd4fb=this[_0x33172a(0x264)](_0x1cd4fb),_0x1cd4fb=this[_0x33172a(0x300)](_0x1cd4fb),_0x1cd4fb=this[_0x33172a(0x2a3)](_0x1cd4fb),_0x1cd4fb;},Game_System['prototype'][_0x2d49ad(0x1cf)]=function(_0x1a72ee){const _0x9c027f=_0x2d49ad;for(const _0xf63b0b of VisuMZ['MessageCore']['Settings'][_0x9c027f(0x21a)]){if(_0x1a72ee['match'](_0xf63b0b['textCodeCheck'])){if(_0x9c027f(0x238)!=='mRpMM')this[_0x9c027f(0x2f0)]=!![],_0x1a72ee=_0x1a72ee[_0x9c027f(0x24a)](_0xf63b0b[_0x9c027f(0x265)],_0xf63b0b[_0x9c027f(0x30e)]['bind'](this));else return'';}}return _0x1a72ee;},Game_System[_0x2d49ad(0x271)][_0x2d49ad(0x2a3)]=function(_0xfa1f63){const _0x2d381c=_0x2d49ad;while(_0xfa1f63['match'](/\\V\[(\d+)\]/gi)){'XQJIH'===_0x2d381c(0x2a2)?_0x470003=_0x3c7e38[_0x2d381c(0x2fb)](_0x2a7eda,_0x1c4d4e):_0xfa1f63=_0xfa1f63[_0x2d381c(0x24a)](/\\V\[(\d+)\]/gi,(_0x50e8e8,_0x2db85a)=>$gameVariables[_0x2d381c(0x26d)](parseInt(_0x2db85a)));}return _0xfa1f63;},Game_System[_0x2d49ad(0x271)]['convertMessageLogTextReplacement']=function(_0x79fac1){const _0x3fc859=_0x2d49ad,_0x2ffd4a=this[_0x3fc859(0x2d2)]();return _0x79fac1=_0x79fac1[_0x3fc859(0x24a)](/\\ItemQuantity\[(\d+)\]/gi,(_0x80f285,_0x513ae7)=>$gameParty[_0x3fc859(0x1ed)]($dataItems[Number(_0x513ae7)])||0x0),_0x79fac1=_0x79fac1['replace'](/\\WeaponQuantity\[(\d+)\]/gi,(_0x314aa2,_0x2e0e77)=>$gameParty[_0x3fc859(0x1ed)]($dataWeapons[Number(_0x2e0e77)])||0x0),_0x79fac1=_0x79fac1[_0x3fc859(0x24a)](/\\ArmorQuantity\[(\d+)\]/gi,(_0x1e4916,_0xd93b33)=>$gameParty['numItems']($dataArmors[Number(_0xd93b33)])||0x0),_0x79fac1=_0x79fac1[_0x3fc859(0x24a)](/\\ArmorQuantity\[(\d+)\]/gi,(_0x10b720,_0x33479e)=>$gameParty[_0x3fc859(0x1ed)]($dataArmors[Number(_0x33479e)])||0x0),_0x79fac1=_0x79fac1[_0x3fc859(0x24a)](/\\LastGainObjQuantity/gi,Window_Base[_0x3fc859(0x271)][_0x3fc859(0x28a)]()),_0x79fac1=_0x79fac1[_0x3fc859(0x24a)](/\\LastGainObjName/gi,Window_Base[_0x3fc859(0x271)][_0x3fc859(0x254)](![])),_0x79fac1=_0x79fac1[_0x3fc859(0x24a)](/\\LastGainObj/gi,Window_Base[_0x3fc859(0x271)][_0x3fc859(0x254)](!![])),_0x79fac1=_0x79fac1[_0x3fc859(0x24a)](/\\ActorFace\[(\d+)\]/gi,(_0x36ad23,_0x1a446f)=>{const _0x4d5ba3=_0x3fc859,_0x43f5c7=$gameActors[_0x4d5ba3(0x2e1)](Number(_0x1a446f));if(_0x43f5c7){if(_0x4d5ba3(0x2d7)===_0x4d5ba3(0x2ad)){if(this[_0x4d5ba3(0x2cf)]===_0x4fd80e)this[_0x4d5ba3(0x270)]();return this[_0x4d5ba3(0x2cf)][_0x4d5ba3(0x26b)];}else _0x2ffd4a[_0x4d5ba3(0x1fa)]=_0x43f5c7['faceName'](),_0x2ffd4a[_0x4d5ba3(0x257)]=_0x43f5c7[_0x4d5ba3(0x257)]();}return'';}),_0x79fac1=_0x79fac1[_0x3fc859(0x24a)](/\\PartyFace\[(\d+)\]/gi,(_0x324262,_0x1a6d9e)=>{const _0xe7216d=_0x3fc859,_0x54eadc=$gameParty[_0xe7216d(0x1d7)]()[Number(_0x1a6d9e)-0x1];return _0x54eadc&&(_0x2ffd4a['faceName']=_0x54eadc[_0xe7216d(0x1fa)](),_0x2ffd4a['faceIndex']=_0x54eadc[_0xe7216d(0x257)]()),'';}),_0x79fac1;},Game_System[_0x2d49ad(0x271)]['convertMessageLogTextRemoval']=function(_0x411be8){const _0x2d6b09=_0x2d49ad;_0x411be8=_0x411be8[_0x2d6b09(0x24a)](/\x1b/gi,'\x5c');const _0x3e5241=this[_0x2d6b09(0x202)]();for(const _0x360f22 of _0x3e5241){if(_0x2d6b09(0x302)!==_0x2d6b09(0x302)){const _0x55f969=this[_0x2d6b09(0x2d2)]();this[_0x2d6b09(0x27d)]()&&(_0x359fc3=_0x2d6b09(0x309)+_0xf2af33);_0x1d4a42=this[_0x2d6b09(0x289)](_0x2dda03);_0x285094['VisuMZ_2_VoiceActControl']&&(_0x3a1ca6=this[_0x2d6b09(0x206)](_0x532412,_0x55f969));if(_0x55f969[_0x2d6b09(0x2a6)][_0x2d6b09(0x241)]>0x0)_0x55f969[_0x2d6b09(0x2a6)]+='\x0a';_0x55f969[_0x2d6b09(0x2a6)]+=_0x888a62;}else _0x411be8=_0x411be8[_0x2d6b09(0x24a)](_0x360f22,'');}return _0x411be8;},Game_System[_0x2d49ad(0x271)]['getRemovedMessageLogTextCodes']=function(){const _0x181f21=_0x2d49ad;let _0x5ab42d=[];return _0x5ab42d[_0x181f21(0x2d1)](/\\$/gi,/\\\./gi,/\\\|/gi,/\\\!/gi),_0x5ab42d['push'](/\\>/gi,/\\</gi,/\\\^/gi),_0x5ab42d[_0x181f21(0x2d1)](/\\(?:Picture|CenterPicture)<(.*?)>/gi),_0x5ab42d[_0x181f21(0x2d1)](/\\COMMONEVENT\[(\d+)\]>/gi,/\\WAIT\[(\d+)\]/gi),_0x5ab42d[_0x181f21(0x2d1)](/<(?:AUTO|AUTOSIZE|AUTO SIZE)>/gi,/<(?:AUTOWIDTH|AUTO WIDTH)>/gi,/<(?:AUTOHEIGHT|AUTO HEIGHT)>/gi),_0x5ab42d[_0x181f21(0x2d1)](/<(?:AUTOACTOR|AUTO ACTOR):[ ](.*?)>/gi,/<(?:AUTOPARTY|AUTO PARTY):[ ](.*?)>/gi,/<(?:AUTOENEMY|AUTO ENEMY):[ ](.*?)>/gi),_0x5ab42d[_0x181f21(0x2d1)](/<(?:AUTOPLAYER|AUTO PLAYER)>/gi,/<(?:AUTOEVENT|AUTO EVENT):[ ](.*?)>/gi),_0x5ab42d['push'](/<SHOW>/gi,/<SHOW[ ](?:SW|SWITCH|SWITCHES):[ ]*(.*?)>/gi,/<SHOW ALL[ ](?:SW|SWITCH|SWITCHES):[ ]*(.*?)>/gi,/<SHOW ANY[ ](?:SW|SWITCH|SWITCHES):[ ]*(.*?)>/gi),_0x5ab42d['push'](/<HIDE>/gi,/<HIDE[ ](?:SW|SWITCH|SWITCHES):[ ]*(.*?)>/gi,/<HIDE ALL[ ](?:SW|SWITCH|SWITCHES):[ ]*(.*?)>/gi,/<HIDE ANY[ ](?:SW|SWITCH|SWITCHES):[ ]*(.*?)>/gi),_0x5ab42d[_0x181f21(0x2d1)](/<ENABLE>/gi,/<ENABLE[ ](?:SW|SWITCH|SWITCHES):[ ]*(.*?)>/gi,/<ENABLE ALL[ ](?:SW|SWITCH|SWITCHES):[ ]*(.*?)>/gi,/<ENABLE ANY[ ](?:SW|SWITCH|SWITCHES):[ ]*(.*?)>/gi),_0x5ab42d[_0x181f21(0x2d1)](/<DISABLE>/gi,/<DISABLE[ ](?:SW|SWITCH|SWITCHES):[ ]*(.*?)>/gi,/<DISABLE ALL[ ](?:SW|SWITCH|SWITCHES):[ ]*(.*?)>/gi,/<DISABLE ANY[ ](?:SW|SWITCH|SWITCHES):[ ]*(.*?)>/gi),_0x5ab42d[_0x181f21(0x2d1)](/\\NormalBG/gi,/\\DimBG/gi,/\\TransparentBG/gi),_0x5ab42d[_0x181f21(0x2d1)](/<POSITION:[ ]*(.*?)>/gi,/<COORDINATES:[ ]*(.*?)>/gi,/<DIMENSIONS:[ ]*(.*?)>/gi),_0x5ab42d['push'](/\\(?:WindowMoveTo|WindowMoveBy):[ ]*(.*?)/gi,/\\WindowReset/gi),_0x5ab42d[_0x181f21(0x2d1)](/\\(?:TroopMember|TroopNameMember)\[(\d+)\]/gi),_0x5ab42d[_0x181f21(0x2d1)](/\\ChangeFace<(.*?)>/gi,/\\FaceIndex\[(\d+)\]/gi),_0x5ab42d[_0x181f21(0x2d1)](/<(?:BGCOLOR|BG COLOR):[ ](.*?)>/gi),_0x5ab42d[_0x181f21(0x2d1)](/<OFFSET:[ ]*(.*?)>/gi),_0x5ab42d[_0x181f21(0x2d1)](/<(?:HELP|HELP DESCRIPTION|DESCRIPTION)>\s*([\s\S]*)\s*<\/(?:HELP|HELP DESCRIPTION|DESCRIPTION)>/i),_0x5ab42d[_0x181f21(0x2d1)](/<ENABLE IF CAN CREATE CHARACTER>/gi),_0x5ab42d[_0x181f21(0x2d1)](/<ENABLE IF CAN DISMISS CHARACTERS>/gi),_0x5ab42d[_0x181f21(0x2d1)](/<ENABLE IF CAN RETRAIN CHARACTERS>/gi),_0x5ab42d[_0x181f21(0x2d1)](/<ENABLE IF HAVE CREATED CHARACTERS>/gi),_0x5ab42d['push'](/<HIDE (?:BUTTON CONSOLE|CONSOLE|BUTTONS)>/gi),_0x5ab42d['push'](/<HIDE CHOICE WINDOW>/gi,/<BIND (?:PICTURE|PICTURES):[ ](\d+)>/gi),_0x5ab42d[_0x181f21(0x2d1)](/<(?:CHOICE|SELECT) (?:COMMON EVENT|EVENT|COMMONEVENT):[ ](\d+)>/gi),_0x5ab42d[_0x181f21(0x2d1)](/\\(?:LSON|LSOFF|LETTER SOUND ON|LETTERSOUNDON|LETTER SOUND OFF|LETTERSOUNDOFF)/gi),_0x5ab42d[_0x181f21(0x2d1)](/\\(?:LETTERSOUNDNAME|LSN)<(.*?)>/gi),_0x5ab42d[_0x181f21(0x2d1)](/\\(?:LETTERSOUNDINTERVAL|LSI)\[(\d+)\]/gi),_0x5ab42d[_0x181f21(0x2d1)](/\\(?:LETTERSOUNDVOLUME|LSV)\[(\d+)\]/gi),_0x5ab42d[_0x181f21(0x2d1)](/\\(?:LETTERSOUNDPITCH|LSPI)\[(\d+)\]/gi),_0x5ab42d[_0x181f21(0x2d1)](/\\(?:LETTERSOUNDPAN|LSPA)\[(\d+)\]/gi),_0x5ab42d[_0x181f21(0x2d1)](/\\(?:LETTERSOUNDVOLUMEVARIANCE|LETTERSOUNDVOLUMEVAR|LSVV)\[(\d+)\]/gi),_0x5ab42d[_0x181f21(0x2d1)](/\\(?:LETTERSOUNDPITCHVARIANCE|LETTERSOUNDPITCHVAR|LSPIV)\[(\d+)\]/gi),_0x5ab42d[_0x181f21(0x2d1)](/\\(?:LETTERSOUNDPANVARIANCE|LETTERSOUNDPANVAR|LSPAV)\[(\d+)\]/gi),_0x5ab42d['push'](/<CONTINUE>/gi),_0x5ab42d;},Game_System[_0x2d49ad(0x271)]['convertVoiceActControlLines']=function(_0x15b65b,_0xc2fe39){const _0x1c683c=_0x2d49ad,_0x43b1be=VisuMZ[_0x1c683c(0x261)][_0x1c683c(0x2d4)];return _0x15b65b=_0x15b65b[_0x1c683c(0x24a)](_0x43b1be['MsgVoiceSfx'],(_0x422097,_0x5715f1)=>{const _0x354190=_0x1c683c;if(_0x354190(0x2b1)===_0x354190(0x2b1)){_0x5715f1=_0x5715f1[_0x354190(0x24a)](/\x1bWrapBreak\[0\]/gi,'\x20');const _0x9286c8=_0x5715f1[_0x354190(0x313)](',')[_0x354190(0x297)](_0x1e1ebc=>_0x1e1ebc[_0x354190(0x2c3)]()),_0x4c0b2d={'name':String(_0x9286c8[0x0]??''),'volume':Number(_0x9286c8[0x1]??0x64)['clamp'](0x0,0x64),'pitch':Number(_0x9286c8[0x2]??0x64)[_0x354190(0x29f)](0x0,0x64),'pan':Number(_0x9286c8[0x3]??0x0)[_0x354190(0x29f)](-0x64,0x64)};return _0xc2fe39[_0x354190(0x224)]=_0x4c0b2d,'';}else this[_0x354190(0x296)][_0x354190(0x2cd)](_0x4724c7),this[_0x354190(0x29e)][_0x354190(0x274)](_0x30c761),_0x4456f2[_0x354190(0x2d1)](_0xcd6b88);}),_0x15b65b;},Game_System['prototype'][_0x2d49ad(0x279)]=function(_0x37f144){const _0x4e549a=_0x2d49ad,_0x1595ed=this[_0x4e549a(0x2d2)]();_0x37f144=this[_0x4e549a(0x310)](_0x37f144),_0x37f144=this[_0x4e549a(0x289)](_0x37f144),_0x1595ed[_0x4e549a(0x205)]=_0x37f144||'';},Game_System[_0x2d49ad(0x271)]['convertMessageLogNameRemoval']=function(_0x3f24fd){const _0x24c285=_0x2d49ad;if(!_0x3f24fd)return'';const _0x335299=[/<LEFT>/gi,/<CENTER>/gi,/<RIGHT>/gi,/<\/LEFT>/gi,/<\/CENTER>/gi,/<\/RIGHT>/gi,/<POSITION:[ ](\d+)>/gi];for(const _0x3a7693 of _0x335299){'wHzov'===_0x24c285(0x28c)?_0x15c05e=_0x24c285(0x225)['format'](_0x1311e2):_0x3f24fd=_0x3f24fd[_0x24c285(0x24a)](_0x3a7693,'');}return _0x3f24fd;},Game_System['prototype'][_0x2d49ad(0x2d8)]=function(_0x473f2b,_0x509ed2){const _0x1c64c5=_0x2d49ad,_0x356ba9=this[_0x1c64c5(0x2d2)]();_0x356ba9[_0x1c64c5(0x1fa)]=_0x473f2b||'',_0x356ba9['faceIndex']=_0x509ed2||0x0;},VisuMZ['MessageLog'][_0x2d49ad(0x2cb)]=Game_Message['prototype']['add'],Game_Message[_0x2d49ad(0x271)]['add']=function(_0x3c3ba7){const _0x48995e=_0x2d49ad;VisuMZ[_0x48995e(0x2de)][_0x48995e(0x2cb)][_0x48995e(0x2e5)](this,_0x3c3ba7),$gameSystem['addTextToMessageLog'](_0x3c3ba7);},VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x269)]=Game_Message[_0x2d49ad(0x271)][_0x2d49ad(0x25a)],Game_Message[_0x2d49ad(0x271)][_0x2d49ad(0x25a)]=function(_0x33c690){const _0x570088=_0x2d49ad;VisuMZ[_0x570088(0x2de)][_0x570088(0x269)][_0x570088(0x2e5)](this,_0x33c690),$gameSystem[_0x570088(0x279)](_0x33c690);},VisuMZ['MessageLog']['Game_Message_setFaceImage']=Game_Message['prototype'][_0x2d49ad(0x227)],Game_Message['prototype'][_0x2d49ad(0x227)]=function(_0x3606d5,_0x571498){const _0x28c532=_0x2d49ad;VisuMZ[_0x28c532(0x2de)]['Game_Message_setFaceImage']['call'](this,_0x3606d5,_0x571498),$gameSystem[_0x28c532(0x2d8)](_0x3606d5,_0x571498);},VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x2c9)]=Game_Interpreter[_0x2d49ad(0x271)][_0x2d49ad(0x27a)],Game_Interpreter[_0x2d49ad(0x271)][_0x2d49ad(0x27a)]=function(_0x25dd29){const _0x454ec5=_0x2d49ad;(SceneManager[_0x454ec5(0x2f9)]()||!Window_MessageLog[_0x454ec5(0x298)])&&$gameSystem[_0x454ec5(0x1f4)]();let _0x10e302=VisuMZ[_0x454ec5(0x2de)][_0x454ec5(0x2c9)][_0x454ec5(0x2e5)](this,_0x25dd29);return(SceneManager[_0x454ec5(0x2f9)]()||!Window_MessageLog[_0x454ec5(0x298)])&&$gameSystem[_0x454ec5(0x2e9)](),_0x10e302;},VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x242)]=Scene_Menu['prototype'][_0x2d49ad(0x278)],Scene_Menu[_0x2d49ad(0x271)][_0x2d49ad(0x278)]=function(){const _0xecb680=_0x2d49ad;VisuMZ[_0xecb680(0x2de)]['Scene_Menu_createCommandWindow'][_0xecb680(0x2e5)](this);const _0x59953c=this[_0xecb680(0x2ec)];_0x59953c[_0xecb680(0x26f)](_0xecb680(0x2a7),this['commandMessageLog'][_0xecb680(0x24c)](this));},Scene_Menu[_0x2d49ad(0x271)]['commandMessageLog']=function(){const _0x2317c7=_0x2d49ad;SceneManager[_0x2317c7(0x2d1)](Scene_MessageLog);};function Scene_MessageLog(){const _0x251dce=_0x2d49ad;this[_0x251dce(0x280)](...arguments);}Scene_MessageLog[_0x2d49ad(0x271)]=Object[_0x2d49ad(0x1ee)](Scene_MenuBase['prototype']),Scene_MessageLog['prototype']['constructor']=Scene_MessageLog,Scene_MessageLog[_0x2d49ad(0x271)]['initialize']=function(){const _0x2cc2e9=_0x2d49ad;Scene_MenuBase['prototype']['initialize'][_0x2cc2e9(0x2e5)](this);},Scene_MessageLog[_0x2d49ad(0x271)]['helpAreaHeight']=function(){return 0x0;},Scene_MessageLog[_0x2d49ad(0x271)][_0x2d49ad(0x1ee)]=function(){const _0x2be435=_0x2d49ad;Scene_MenuBase[_0x2be435(0x271)][_0x2be435(0x1ee)][_0x2be435(0x2e5)](this),this['createMessageLogWindow']();},Scene_MessageLog[_0x2d49ad(0x271)][_0x2d49ad(0x2bf)]=function(){const _0x4d5a79=_0x2d49ad,_0x3991cf=this[_0x4d5a79(0x25f)]();this['_messageLogWindow']=new Window_MessageLog(_0x3991cf),this[_0x4d5a79(0x2b7)](this[_0x4d5a79(0x2c7)]),this[_0x4d5a79(0x2c7)][_0x4d5a79(0x26f)]('cancel',this[_0x4d5a79(0x226)][_0x4d5a79(0x24c)](this)),this[_0x4d5a79(0x2c7)][_0x4d5a79(0x286)](VisuMZ[_0x4d5a79(0x2de)]['Settings']['Window'][_0x4d5a79(0x2ea)]);},Scene_MessageLog[_0x2d49ad(0x271)][_0x2d49ad(0x25f)]=function(){const _0x30fd3d=_0x2d49ad,_0x444120=VisuMZ['MessageLog'][_0x30fd3d(0x256)][_0x30fd3d(0x312)][_0x30fd3d(0x1cd)];if(_0x444120)return _0x444120['call'](this);const _0x3bc9f0=0x0,_0x324eac=this[_0x30fd3d(0x292)](),_0x576a5f=Graphics['boxWidth'],_0x2e2f74=this[_0x30fd3d(0x307)]();return new Rectangle(_0x3bc9f0,_0x324eac,_0x576a5f,_0x2e2f74);},Scene_MessageLog[_0x2d49ad(0x271)][_0x2d49ad(0x27e)]=function(){const _0xeed8e5=_0x2d49ad;Scene_MenuBase[_0xeed8e5(0x271)][_0xeed8e5(0x27e)]['call'](this),this['setBackgroundOpacity'](this[_0xeed8e5(0x295)]()),this[_0xeed8e5(0x240)]();},Scene_MessageLog[_0x2d49ad(0x271)]['getBackgroundOpacity']=function(){const _0x3cd861=_0x2d49ad;return VisuMZ[_0x3cd861(0x2de)][_0x3cd861(0x256)][_0x3cd861(0x23a)]['SnapshotOpacity'];},Scene_MessageLog[_0x2d49ad(0x271)][_0x2d49ad(0x240)]=function(){const _0x38af7d=_0x2d49ad,_0x4d1ff8=VisuMZ[_0x38af7d(0x2de)][_0x38af7d(0x256)]['BgSettings'];_0x4d1ff8&&(_0x4d1ff8['BgFilename1']!==''||_0x4d1ff8[_0x38af7d(0x249)]!=='')&&(this[_0x38af7d(0x1e0)]=new Sprite(ImageManager[_0x38af7d(0x24b)](_0x4d1ff8[_0x38af7d(0x2ff)])),this[_0x38af7d(0x24e)]=new Sprite(ImageManager[_0x38af7d(0x2dc)](_0x4d1ff8[_0x38af7d(0x249)])),this[_0x38af7d(0x248)](this['_backSprite1']),this['addChild'](this['_backSprite2']),this[_0x38af7d(0x1e0)][_0x38af7d(0x285)][_0x38af7d(0x21d)](this[_0x38af7d(0x2b2)][_0x38af7d(0x24c)](this,this[_0x38af7d(0x1e0)])),this[_0x38af7d(0x24e)]['bitmap']['addLoadListener'](this[_0x38af7d(0x2b2)][_0x38af7d(0x24c)](this,this[_0x38af7d(0x24e)])));},Scene_MessageLog[_0x2d49ad(0x271)]['adjustSprite']=function(_0x3442b4){const _0x568608=_0x2d49ad;this['scaleSprite'](_0x3442b4),this[_0x568608(0x294)](_0x3442b4);},Scene_MessageLog['prototype'][_0x2d49ad(0x2e8)]=function(){const _0x330a65=_0x2d49ad;return TextManager[_0x330a65(0x1d0)]('pageup',_0x330a65(0x29d));},Scene_MessageLog[_0x2d49ad(0x271)][_0x2d49ad(0x22b)]=function(){const _0x227a03=_0x2d49ad;return TextManager[_0x227a03(0x1d0)]('up','down');},Scene_MessageLog['prototype'][_0x2d49ad(0x211)]=function(){return'';},Scene_MessageLog['prototype'][_0x2d49ad(0x1f0)]=function(){const _0x1d560e=_0x2d49ad;return TextManager[_0x1d560e(0x1e8)];},Scene_MessageLog[_0x2d49ad(0x271)][_0x2d49ad(0x290)]=function(){const _0x43d000=_0x2d49ad;return TextManager[_0x43d000(0x2d0)];},VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x2eb)]=Window_MenuCommand['prototype'][_0x2d49ad(0x1e4)],Window_MenuCommand[_0x2d49ad(0x271)][_0x2d49ad(0x1e4)]=function(){const _0x297669=_0x2d49ad;VisuMZ[_0x297669(0x2de)][_0x297669(0x2eb)]['call'](this),this[_0x297669(0x28e)]();},Window_MenuCommand[_0x2d49ad(0x271)][_0x2d49ad(0x28e)]=function(){const _0x793961=_0x2d49ad;if(!this[_0x793961(0x258)]())return;if(!this[_0x793961(0x1db)]())return;const _0x2c39a7=TextManager[_0x793961(0x284)],_0x89576a=this[_0x793961(0x1f5)]();this[_0x793961(0x236)](_0x2c39a7,'messageLog',_0x89576a);},Window_MenuCommand[_0x2d49ad(0x271)][_0x2d49ad(0x258)]=function(){return Imported['VisuMZ_1_MainMenuCore']?![]:!![];},Window_MenuCommand['prototype'][_0x2d49ad(0x1db)]=function(){const _0x28b8f6=_0x2d49ad;return $gameSystem[_0x28b8f6(0x247)]();},Window_MenuCommand['prototype'][_0x2d49ad(0x1f5)]=function(){const _0x18da77=_0x2d49ad;return $gameSystem[_0x18da77(0x215)]();},VisuMZ['MessageLog'][_0x2d49ad(0x220)]=Window_ChoiceList[_0x2d49ad(0x271)][_0x2d49ad(0x301)],Window_ChoiceList[_0x2d49ad(0x271)][_0x2d49ad(0x301)]=function(){const _0x77eda3=_0x2d49ad;(SceneManager[_0x77eda3(0x2f9)]()||!Window_MessageLog[_0x77eda3(0x298)])&&this['addMessageLogEntry'](!![]),VisuMZ[_0x77eda3(0x2de)][_0x77eda3(0x220)][_0x77eda3(0x2e5)](this);},VisuMZ[_0x2d49ad(0x2de)]['Window_ChoiceList_callCancelHandler']=Window_ChoiceList['prototype'][_0x2d49ad(0x201)],Window_ChoiceList[_0x2d49ad(0x271)][_0x2d49ad(0x201)]=function(){const _0x461d17=_0x2d49ad;(SceneManager[_0x461d17(0x2f9)]()||!Window_MessageLog[_0x461d17(0x298)])&&this[_0x461d17(0x1d8)](![]),VisuMZ['MessageLog'][_0x461d17(0x237)][_0x461d17(0x2e5)](this);},Window_ChoiceList[_0x2d49ad(0x271)]['addMessageLogEntry']=function(_0xb936b7){const _0xb79cc8=_0x2d49ad;$gameSystem[_0xb79cc8(0x1f4)]();const _0x3491c2=TextManager[_0xb79cc8(0x2af)];let _0x4d616b='';if(_0xb936b7){if(_0xb79cc8(0x28b)===_0xb79cc8(0x28b)){let _0x44a4ba=this[_0xb79cc8(0x2fe)](),_0x50f20a=$gameMessage[_0xb79cc8(0x216)]()[_0x44a4ba];Window_MessageLog[_0xb79cc8(0x2fd)]&&(_0x50f20a=_0xb79cc8(0x225)[_0xb79cc8(0x2be)](_0x50f20a)),_0x4d616b=_0x3491c2['format'](_0x50f20a);}else{if(this[_0xb79cc8(0x2cf)]===_0x528f71)this['initMessageLogMainMenu']();this['_MessageLog_MainMenu'][_0xb79cc8(0x2db)]=_0x5e0996;}}else{if(!_0xb936b7&&!this[_0xb79cc8(0x245)]()){let _0x56c2ae=$gameMessage[_0xb79cc8(0x2a9)](),_0x20f9eb=$gameMessage[_0xb79cc8(0x216)]()[_0x56c2ae];if(Window_MessageLog['COLOR_LOCK_CHOICE']){if('txptp'==='jWVmZ')return this[_0xb79cc8(0x29c)]&&this[_0xb79cc8(0x29c)]['constructor']===_0x3ee451;else _0x20f9eb=_0xb79cc8(0x225)['format'](_0x20f9eb);}_0x4d616b=_0x3491c2['format'](_0x20f9eb);}else{if(_0xb79cc8(0x213)===_0xb79cc8(0x213))_0x4d616b=_0x3491c2[_0xb79cc8(0x2be)](TextManager['MessageLogChoiceCancel']);else{this['origin']['y']+=_0x5db068;let _0x2adcc9=_0x3b39a9['max'](0x0,this['_allTextHeight']-this[_0xb79cc8(0x2f1)]);this['origin']['y']=this[_0xb79cc8(0x24f)]['y']['clamp'](0x0,_0x2adcc9);}}}$gameSystem[_0xb79cc8(0x23e)](_0x4d616b),$gameSystem[_0xb79cc8(0x2e9)]();},VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x2d9)]=Window_NumberInput[_0x2d49ad(0x271)]['processOk'],Window_NumberInput['prototype'][_0x2d49ad(0x25d)]=function(){const _0xb6a23=_0x2d49ad;(SceneManager[_0xb6a23(0x2f9)]()||!Window_MessageLog['SCENE_MAP_ONLY'])&&this[_0xb6a23(0x1d8)](),VisuMZ['MessageLog'][_0xb6a23(0x2d9)][_0xb6a23(0x2e5)](this);},Window_NumberInput[_0x2d49ad(0x271)]['addMessageLogEntry']=function(){const _0xc6a561=_0x2d49ad;$gameSystem[_0xc6a561(0x1f4)]();const _0x50c801=TextManager[_0xc6a561(0x2f4)];let _0xd89c0d=this['_number'];Window_MessageLog[_0xc6a561(0x28d)]&&(_0xd89c0d='<ColorLock>%1</ColorLock>'[_0xc6a561(0x2be)](_0xd89c0d));let _0x371e9d=_0x50c801[_0xc6a561(0x2be)](_0xd89c0d);$gameSystem[_0xc6a561(0x23e)](_0x371e9d),$gameSystem[_0xc6a561(0x2e9)]();},VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x219)]=Window_EventItem[_0x2d49ad(0x271)][_0x2d49ad(0x26c)],Window_EventItem['prototype'][_0x2d49ad(0x26c)]=function(){const _0x39b4b8=_0x2d49ad;(SceneManager[_0x39b4b8(0x2f9)]()||!Window_MessageLog[_0x39b4b8(0x298)])&&this[_0x39b4b8(0x1d8)](this[_0x39b4b8(0x2d5)]()),VisuMZ[_0x39b4b8(0x2de)]['Window_EventItem_onOk'][_0x39b4b8(0x2e5)](this);},VisuMZ['MessageLog'][_0x2d49ad(0x2e3)]=Window_EventItem[_0x2d49ad(0x271)][_0x2d49ad(0x2c5)],Window_EventItem[_0x2d49ad(0x271)]['onCancel']=function(){const _0x1d1680=_0x2d49ad;(SceneManager[_0x1d1680(0x2f9)]()||!Window_MessageLog[_0x1d1680(0x298)])&&this['addMessageLogEntry'](![]),VisuMZ[_0x1d1680(0x2de)]['Window_EventItem_onCancel'][_0x1d1680(0x2e5)](this);},Window_EventItem[_0x2d49ad(0x271)][_0x2d49ad(0x1d8)]=function(_0x1367ba){const _0x4ee591=_0x2d49ad;$gameSystem[_0x4ee591(0x1f4)]();const _0x46d3c3=TextManager[_0x4ee591(0x2a0)];let _0x548461='';if(_0x1367ba){if(_0x4ee591(0x233)!==_0x4ee591(0x2e0)){let _0x40b832=TextManager[_0x4ee591(0x209)],_0x471f1c=_0x40b832[_0x4ee591(0x2be)](_0x4ee591(0x27c)[_0x4ee591(0x2be)](_0x1367ba[_0x4ee591(0x23c)]),_0x1367ba[_0x4ee591(0x1e7)]);Window_MessageLog['COLOR_LOCK_ITEM']&&(_0x471f1c=_0x4ee591(0x225)['format'](_0x471f1c)),_0x548461=_0x46d3c3['format'](_0x471f1c);}else _0x506fa0('%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.'[_0x4ee591(0x2be)](_0x5aaaa5,_0x3b32ba,_0x3562d1)),_0x59768a[_0x4ee591(0x255)]();}else _0x548461=_0x46d3c3['format'](TextManager[_0x4ee591(0x212)]);$gameSystem['addTextToMessageLog'](_0x548461),$gameSystem['addNewLoggedMessageEntry']();},VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x276)]=Window_Base[_0x2d49ad(0x271)][_0x2d49ad(0x2bb)],Window_Base[_0x2d49ad(0x271)][_0x2d49ad(0x2bb)]=function(_0x35c1d2){const _0x11f110=_0x2d49ad;return _0x35c1d2=VisuMZ['MessageLog'][_0x11f110(0x276)][_0x11f110(0x2e5)](this,_0x35c1d2),_0x35c1d2=_0x35c1d2[_0x11f110(0x24a)](/<BYPASS MESSAGE LOG>/i,''),_0x35c1d2;},VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x23f)]=Window_Message[_0x2d49ad(0x271)]['isTriggered'],Window_Message[_0x2d49ad(0x271)][_0x2d49ad(0x259)]=function(){const _0x3f32ea=_0x2d49ad;let _0xfc97f=VisuMZ['MessageLog'][_0x3f32ea(0x23f)][_0x3f32ea(0x2e5)](this);if(this[_0x3f32ea(0x2ab)]()&&Input['isTriggered'](Window_MessageLog['SHORTCUT_KEY']))return this['callMessageLog'](),![];else{if(_0x3f32ea(0x277)==='BPqfd'){if(this[_0x3f32ea(0x2cf)]===_0x1c836e)this[_0x3f32ea(0x270)]();this[_0x3f32ea(0x2cf)][_0x3f32ea(0x26b)]=_0x5ee7b9;}else return _0xfc97f;}},Window_Message[_0x2d49ad(0x271)][_0x2d49ad(0x2ab)]=function(){const _0x5565c9=_0x2d49ad;return SceneManager['isSceneMap']()&&$gameSystem[_0x5565c9(0x215)]();},Window_Message[_0x2d49ad(0x271)][_0x2d49ad(0x30b)]=function(){const _0x8a5321=_0x2d49ad;this[_0x8a5321(0x20e)](),SceneManager['push'](Scene_MessageLog);};function _0x24ef(){const _0x525d4f=['setSpeakerName','HORZ_LINE_THICKNESS','drawHorzLine','processOk','down','messageLogWindowRect','initMessageLogSettings','VoiceActControl','SlowScrollSpeed','_replayVoiceSprites','convertMessageLogTextReplacement','textCodeCheck','return\x200','resetWordWrap','isAutoColorAffected','Game_Message_setSpeakerName','_allTextHeight','shown','onOk','value','Bypass','setHandler','initMessageLogMainMenu','prototype','%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.','DkpHy','removeChild','refresh','Window_Base_preConvertEscapeCharacters','ecIsl','createCommandWindow','setSpeakerToMessageLog','command101','sqlqi','\x5cI[%1]','isMessageWindowWordWrap','createBackground','filter','initialize','addReplayVoiceSprite','COLOR_LOCK_ITEM','forceNameColor','MessageLogMenuCommand','bitmap','setBackgroundType','BypassMessageLogging','setBypassMessageLogging','convertMessageLogTextCodes','lastGainedObjectQuantity','jkqhY','DMeiV','COLOR_LOCK_NUMBER','addMessageLogCommand','addInnerChild','buttonAssistText3','parse','mainAreaTop','SystemEnableMessageLogMenu','centerSprite','getBackgroundOpacity','_innerChildren','map','SCENE_MAP_ONLY','home','registerCommand','setMainMenuMessageLogEnabled','_scene','pagedown','_clientArea','clamp','MessageLogEventItemFmt','TextManager_msgButtonConsole','TKyvT','convertMessageLogVariableTextCodes','changeTextColor','158820lbSOuJ','messageBody','messageLog','FAST_SCROLL_SPEED','choiceCancelType','smxrM','canCallMessageLog','ARRAYEVAL','HnAxK','ShowFaces','MessageLogChoiceListFmt','ChoiceCancel','aVWMV','adjustSprite','drawAllText','GiDbL','ColorLockNumber','mlGvF','addWindow','bmMBA','ThRcH','scrollToTop','preConvertEscapeCharacters','getLoggedMessages','155JrQzuA','format','createMessageLogWindow','setXyPosition','setMainMenuMessageLogVisible','SHORTCUT_KEY','trim','ARRAYSTRUCT','onCancel','drawing','_messageLogWindow','activate','Game_Interpreter_command101','%1\x27s\x20version\x20does\x20not\x20match\x20plugin\x27s.\x20Please\x20update\x20it\x20in\x20the\x20Plugin\x20Manager.','Game_Message_add','363249jhJioS','remove','SPEAKER_NAME_X','_MessageLog_MainMenu','MessageLogScroll','push','getLatestMessageLogEntry','MainMenu','RegExp','item','iconWidth','JyyAD','setFaceToMessageLog','Window_NumberInput_processOk','ENTRY_LIMIT','enabled','loadTitle2','aEWpl','MessageLog','faceWidth','RJgAJ','actor','NameBoxWindowDefaultColor','Window_EventItem_onCancel','calculateTextHeight','call','VisuMZ_2_VoiceActControl','backlog','buttonAssistKey1','addNewLoggedMessageEntry','MessageLogMenu_BgType','Window_MenuCommand_addOriginalCommands','_commandWindow','kXCeN','description','resetFontSettings','_textMacroFound','innerHeight','match','drawMessageText','MessageLogNumberInputFmt','processFastScroll','30594rwXbIH','%1\x20is\x20missing\x20a\x20required\x20plugin.\x0aPlease\x20install\x20%2\x20into\x20the\x20Plugin\x20Manager.','FUNC','isSceneMap','parameters','max','ConvertParams','COLOR_LOCK_CHOICE','currentExt','BgFilename1','convertMessageLogTextRemoval','callOkHandler','rpukt','Enable','textSizeEx','NUM','_isHotKeyPressed','mainAreaHeight','EnableMainMenu','<WORDWRAP>','version','callMessageLog','SystemShowMessageLogMenu','SLOW_SCROLL_SPEED','textCodeResult','loadFace','convertMessageLogNameRemoval','MessageCore','Window','split','isPressed','scrollToBottom','MessageLogMenu_RectJS','OrxHY','convertMessageLogTextMacros','getInputMultiButtonStrings','cqxJN','LkRyn','ShortcutKey','Game_System_initialize','processCursorMove','1004hVyBgG','members','addMessageLogEntry','ARRAYNUM','Vocab','isMessageLogCommandVisible','_forcedNameColor','ARRAYFUNC','STR','qJnjm','_backSprite1','innerWidth','SpeakerNameX','1042QusBrv','addOriginalCommands','Name','msgButtonConsole','name','MessageLogFastScroll','clearNameColor','lQjuN','ItemFmt','NoItem','numItems','create','54192HtXJMi','buttonAssistText1','log','9366247ZVAmiy','_loggedMessages','createNewLoggedMessageEntry','isMessageLogCommandEnabled','outputHeight','ShowMainMenu','50ekkRsW','eTsSG','faceName','FastScroll','prepareMessageLogFaces','drawTextEx','drawFace','height','outputWidth','callCancelHandler','getRemovedMessageLogTextCodes','EVAL','FastScrollSpeed','speaker','convertVoiceActControlLines','textColor','lineHeight','MessageLogItemNameFmt','updateOrigin','MessageLogChoiceCancel','fFoXK','_newMessageLogEntry','playOkSound','frameCount','processSlowScroll','buttonAssistKey4','MessageLogItemNothing','GZfQx','SHOW_FACES','isMainMenuMessageLogEnabled','choices','General','end','Window_EventItem_onOk','TextMacros','SlowScroll','MtYIh','addLoadListener','removeAllReplayVoiceSprites','1339cEfXZi','Window_ChoiceList_callOkHandler','Show','SLcKZ','boxWidth','replayVoiceSound','<ColorLock>%1</ColorLock>','popScene','setFaceImage','510652TPYUvz','isBypassMessageLogging','SlowSoundFreq','buttonAssistKey3','playCursorSound','setSound','drawRect','_MessageLog_Bypass','includes','EntryLimit','_lineY','HRUcg','SLOW_SOUND_FREQUENCY','NumberFmt','addCommand','Window_ChoiceList_callCancelHandler','Txjrg','SceneManager_push','BgSettings','processAllText','iconIndex','smoothScrollBy','addTextToMessageLog','Window_Message_isTriggered','createCustomBackgroundImages','length','Scene_Menu_createCommandWindow','stringify','faceHeight','needsCancelButton','PAD_SIDES','isMainMenuMessageLogVisible','addChild','BgFilename2','replace','loadTitle1','bind','15jpyenH','_backSprite2','origin','MessageLogButtonName','98suhFmT','FAST_SOUND_FREQUENCY','constructor','lastGainedObjectName','exit','Settings','faceIndex','addMessageLogCommandAutomatically','isTriggered'];_0x24ef=function(){return _0x525d4f;};return _0x24ef();}function Window_MessageLog(){this['initialize'](...arguments);}Window_MessageLog[_0x2d49ad(0x271)]=Object[_0x2d49ad(0x1ee)](Window_Selectable['prototype']),Window_MessageLog[_0x2d49ad(0x271)][_0x2d49ad(0x253)]=Window_MessageLog,Window_MessageLog[_0x2d49ad(0x298)]=!![],Window_MessageLog[_0x2d49ad(0x2da)]=VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x256)][_0x2d49ad(0x217)][_0x2d49ad(0x231)],Window_MessageLog[_0x2d49ad(0x2c2)]=VisuMZ[_0x2d49ad(0x2de)]['Settings']['General'][_0x2d49ad(0x1d3)],Window_MessageLog[_0x2d49ad(0x214)]=VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x256)][_0x2d49ad(0x217)][_0x2d49ad(0x2ae)]??!![],Window_MessageLog[_0x2d49ad(0x246)]=VisuMZ['MessageLog'][_0x2d49ad(0x256)][_0x2d49ad(0x217)]['PadSides']??!![],Window_MessageLog['HORZ_LINE_THICKNESS']=0x4,Window_MessageLog[_0x2d49ad(0x2ce)]=VisuMZ['MessageLog'][_0x2d49ad(0x256)]['Window'][_0x2d49ad(0x1e2)],Window_MessageLog[_0x2d49ad(0x2fd)]=VisuMZ['MessageLog']['Settings'][_0x2d49ad(0x312)]['ColorLockChoice'],Window_MessageLog[_0x2d49ad(0x28d)]=VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x256)]['Window'][_0x2d49ad(0x2b5)],Window_MessageLog[_0x2d49ad(0x282)]=VisuMZ[_0x2d49ad(0x2de)]['Settings']['Window']['ColorLockItem'],Window_MessageLog[_0x2d49ad(0x30d)]=VisuMZ['MessageLog'][_0x2d49ad(0x256)][_0x2d49ad(0x312)][_0x2d49ad(0x262)],Window_MessageLog[_0x2d49ad(0x2a8)]=VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x256)]['Window'][_0x2d49ad(0x204)],Window_MessageLog[_0x2d49ad(0x234)]=VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x256)][_0x2d49ad(0x312)][_0x2d49ad(0x22a)],Window_MessageLog['FAST_SOUND_FREQUENCY']=VisuMZ[_0x2d49ad(0x2de)][_0x2d49ad(0x256)][_0x2d49ad(0x312)]['FastSoundFreq'],Window_MessageLog[_0x2d49ad(0x271)]['initialize']=function(_0x50893c){const _0x16689d=_0x2d49ad;this[_0x16689d(0x306)]=Input['isPressed'](Window_MessageLog['SHORTCUT_KEY']),Window_Selectable[_0x16689d(0x271)][_0x16689d(0x280)][_0x16689d(0x2e5)](this,_0x50893c),this['_allTextHeight']=0x0,this[_0x16689d(0x275)](),this[_0x16689d(0x2c8)]();},Window_MessageLog[_0x2d49ad(0x271)][_0x2d49ad(0x268)]=function(){return!![];},Window_MessageLog['prototype'][_0x2d49ad(0x275)]=function(){const _0x46cf91=_0x2d49ad;this[_0x46cf91(0x21e)](),this['calculateTextHeight'](),this['createContents'](),this[_0x46cf91(0x2b3)]();},Window_MessageLog[_0x2d49ad(0x271)]['removeAllReplayVoiceSprites']=function(){const _0x326ae2=_0x2d49ad;if(!Imported[_0x326ae2(0x2e6)])return;this['_replayVoiceSprites']=this[_0x326ae2(0x263)]||[];const _0x4c7ebb=[];for(const _0x20a10a of this[_0x326ae2(0x263)]){this['_innerChildren'][_0x326ae2(0x2cd)](_0x20a10a),this[_0x326ae2(0x29e)][_0x326ae2(0x274)](_0x20a10a),_0x4c7ebb[_0x326ae2(0x2d1)](_0x20a10a);}for(const _0x16d45a of _0x4c7ebb){this['_replayVoiceSprites'][_0x326ae2(0x2cd)](_0x16d45a);}},Window_MessageLog[_0x2d49ad(0x271)][_0x2d49ad(0x2e4)]=function(){const _0xa66a1b=_0x2d49ad,_0x4c75f0=this[_0xa66a1b(0x208)](),_0x636ab5=$gameSystem['getLoggedMessages']();this[_0xa66a1b(0x26a)]=_0x4c75f0;for(const _0x24a377 of _0x636ab5){if(!_0x24a377)continue;if(_0x24a377[_0xa66a1b(0x205)]!=='')this[_0xa66a1b(0x26a)]+=this[_0xa66a1b(0x304)](_0x24a377[_0xa66a1b(0x205)])[_0xa66a1b(0x1ff)];let _0xcc8d9=_0x24a377['faceName']!==''&&Window_MessageLog[_0xa66a1b(0x214)]?ImageManager['faceHeight']:0x0,_0xf28182=this[_0xa66a1b(0x304)](_0x24a377[_0xa66a1b(0x2a6)])[_0xa66a1b(0x1ff)];this[_0xa66a1b(0x26a)]+=Math[_0xa66a1b(0x2fb)](_0xcc8d9,_0xf28182),this[_0xa66a1b(0x26a)]+=_0x4c75f0;}},Window_MessageLog[_0x2d49ad(0x271)]['contentsHeight']=function(){const _0xa3d2f8=_0x2d49ad;return Math[_0xa3d2f8(0x2fb)](this[_0xa3d2f8(0x26a)],0x1);},Window_MessageLog['prototype']['drawAllText']=function(){const _0x3e6afb=_0x2d49ad;this['_lineY']=0x0,this[_0x3e6afb(0x25c)]();const _0x93013c=$gameSystem[_0x3e6afb(0x2bc)]();for(const _0x36bade of _0x93013c){if(!_0x36bade)continue;this[_0x3e6afb(0x2ef)](),this[_0x3e6afb(0x2f3)](_0x36bade),this['resetWordWrap']();}this['scrollToBottom']();},Window_MessageLog[_0x2d49ad(0x271)][_0x2d49ad(0x25c)]=function(){const _0x375ef8=_0x2d49ad,_0x796856=new Rectangle(0x4,this['_lineY'],this['innerWidth']-0x8,this[_0x375ef8(0x208)]());this['resetFontSettings']();const _0x2ee45a=Window_MessageLog[_0x375ef8(0x25b)],_0x53d064=_0x796856['y']+(_0x796856[_0x375ef8(0x1ff)]-_0x2ee45a)/0x2;this[_0x375ef8(0x22e)](_0x796856['x'],_0x53d064,_0x796856['width'],_0x2ee45a),this[_0x375ef8(0x232)]+=this[_0x375ef8(0x208)]();},Window_MessageLog[_0x2d49ad(0x271)][_0x2d49ad(0x304)]=function(_0x17f65b){const _0x5b5620=_0x2d49ad;let _0x5dea2f=this[_0x5b5620(0x1e1)]-(ImageManager['faceWidth']+0x18)*0x2;if(Graphics['boxWidth']<=0x330){if(_0x5b5620(0x27b)!=='Kclxv')_0x5dea2f=this['innerWidth']-0x8;else return!![];}this[_0x5b5620(0x2ef)](),this['resetWordWrap']();const _0x54b7e1=this['createTextState'](_0x17f65b,0x0,0x0,_0x5dea2f);return _0x54b7e1[_0x5b5620(0x2c6)]=![],this[_0x5b5620(0x23b)](_0x54b7e1),this[_0x5b5620(0x267)](),{'width':_0x54b7e1[_0x5b5620(0x200)],'height':_0x54b7e1[_0x5b5620(0x1f6)]};},Window_MessageLog['prototype'][_0x2d49ad(0x2f3)]=function(_0x2601b6){const _0x460ce9=_0x2d49ad;let _0x3d95cf=0x4,_0x53c0ec=ImageManager[_0x460ce9(0x2df)]+0x14,_0x5ca0e=this['innerWidth']-(_0x53c0ec+0x4)*0x2;_0x2601b6[_0x460ce9(0x205)]!==''&&(this[_0x460ce9(0x283)](),this['drawTextEx'](_0x2601b6[_0x460ce9(0x205)],Window_MessageLog[_0x460ce9(0x2ce)],this[_0x460ce9(0x232)],_0x5ca0e),this[_0x460ce9(0x232)]+=this['textSizeEx'](_0x2601b6['speaker'])['height'],this[_0x460ce9(0x1e9)]());this[_0x460ce9(0x2ef)](),this[_0x460ce9(0x267)]();if(Window_MessageLog[_0x460ce9(0x214)]&&_0x2601b6[_0x460ce9(0x1fa)]){if('kXCeN'!==_0x460ce9(0x2ed)){if(!this['addMessageLogCommandAutomatically']())return;if(!this['isMessageLogCommandVisible']())return;const _0x3179ff=_0x496648[_0x460ce9(0x284)],_0x559eab=this[_0x460ce9(0x1f5)]();this[_0x460ce9(0x236)](_0x3179ff,'messageLog',_0x559eab);}else{let _0x389b3c=_0x3d95cf,_0x26fae0=this['_lineY'],_0x359b2f=ImageManager[_0x460ce9(0x2df)],_0x17882e=ImageManager[_0x460ce9(0x244)];this[_0x460ce9(0x1fe)](_0x2601b6[_0x460ce9(0x1fa)],_0x2601b6[_0x460ce9(0x257)],_0x389b3c,_0x26fae0,_0x359b2f,_0x17882e);if(Graphics[_0x460ce9(0x223)]<=0x330){if('aEWpl'!==_0x460ce9(0x2dd)){let _0x2bc5a9=_0x3f239e,_0x2d8894=this[_0x460ce9(0x232)],_0x319d2d=_0x3e4c2f[_0x460ce9(0x2df)],_0x4f4fee=_0xf07e57[_0x460ce9(0x244)];this[_0x460ce9(0x1fe)](_0x4f769b[_0x460ce9(0x1fa)],_0x46746d[_0x460ce9(0x257)],_0x2bc5a9,_0x2d8894,_0x319d2d,_0x4f4fee),_0x4a7ca0[_0x460ce9(0x223)]<=0x330&&(_0x178180=this[_0x460ce9(0x1e1)]-(_0x456aa7+0x4));}else _0x5ca0e=this[_0x460ce9(0x1e1)]-(_0x53c0ec+0x4);}}}else{if(!Window_MessageLog[_0x460ce9(0x246)]||Graphics[_0x460ce9(0x223)]<=0x330){_0x53c0ec=0x4,_0x5ca0e=this['innerWidth']-0x8;if(Imported[_0x460ce9(0x2e6)]){if(_0x460ce9(0x2b8)!=='hZGyZ')_0x2601b6[_0x460ce9(0x224)]&&(_0x53c0ec+=ImageManager['iconWidth']+0x4,_0x5ca0e-=ImageManager[_0x460ce9(0x2d6)]+0x4);else return _0x20f5c2[_0x460ce9(0x1e8)];}}}this[_0x460ce9(0x1fd)](_0x2601b6[_0x460ce9(0x2a6)],_0x53c0ec,this[_0x460ce9(0x232)],_0x5ca0e);let _0x12cb11=this[_0x460ce9(0x304)](_0x2601b6[_0x460ce9(0x2a6)])['height'],_0x3fcaba=_0x2601b6['faceName']!==''&&Window_MessageLog[_0x460ce9(0x214)]?ImageManager[_0x460ce9(0x244)]:0x0;Imported[_0x460ce9(0x2e6)]&&this[_0x460ce9(0x281)](_0x2601b6,_0x3d95cf,this['_lineY']),this[_0x460ce9(0x232)]+=Math['max'](_0x3fcaba,_0x12cb11),this[_0x460ce9(0x2ef)](),this[_0x460ce9(0x267)](),this[_0x460ce9(0x25c)]();},Window_MessageLog[_0x2d49ad(0x271)][_0x2d49ad(0x283)]=function(){const _0x1a6288=_0x2d49ad,_0x35a203=VisuMZ[_0x1a6288(0x311)]['Settings'][_0x1a6288(0x217)][_0x1a6288(0x2e2)];this[_0x1a6288(0x1dc)]=ColorManager[_0x1a6288(0x207)](_0x35a203);},Window_MessageLog[_0x2d49ad(0x271)][_0x2d49ad(0x1e9)]=function(){this['_forcedNameColor']=undefined;},Window_MessageLog[_0x2d49ad(0x271)]['resetTextColor']=function(){const _0x232063=_0x2d49ad;Window_Selectable[_0x232063(0x271)]['resetTextColor'][_0x232063(0x2e5)](this);if(this[_0x232063(0x1dc)]){if(_0x232063(0x2aa)!==_0x232063(0x2aa)){_0x3c164a=_0x2816b4[_0x232063(0x24a)](/\x1b/gi,'\x5c');const _0x5b84a7=this[_0x232063(0x202)]();for(const _0x4b236c of _0x5b84a7){_0x5a6fe2=_0x38751d['replace'](_0x4b236c,'');}return _0x5e1a98;}else this[_0x232063(0x2a4)](this[_0x232063(0x1dc)]);}},Window_MessageLog[_0x2d49ad(0x271)]['addReplayVoiceSprite']=function(_0x55cc8a,_0x5aa965,_0xdfcceb){const _0x23e16b=_0x2d49ad;if(!_0x55cc8a['replayVoiceSound'])return;this[_0x23e16b(0x263)]=this[_0x23e16b(0x263)]||[];(Window_MessageLog[_0x23e16b(0x214)]&&_0x55cc8a[_0x23e16b(0x1fa)]||Graphics[_0x23e16b(0x223)]>0x330)&&(_0x23e16b(0x222)!==_0x23e16b(0x2b4)?_0x5aa965+=ImageManager[_0x23e16b(0x2df)]-ImageManager[_0x23e16b(0x2d6)]:this[_0x23e16b(0x210)](!![]));const _0x3562d9=new Sprite_ReplayVoiceLine();_0x3562d9[_0x23e16b(0x22d)](_0x55cc8a[_0x23e16b(0x224)]),_0x3562d9[_0x23e16b(0x2c0)](_0x5aa965,_0xdfcceb),this['_replayVoiceSprites'][_0x23e16b(0x2d1)](_0x3562d9),this[_0x23e16b(0x28f)](_0x3562d9);},Window_MessageLog['prototype'][_0x2d49ad(0x20a)]=function(){},Window_MessageLog[_0x2d49ad(0x271)][_0x2d49ad(0x1d5)]=function(){const _0x50f1bb=_0x2d49ad;if(Input[_0x50f1bb(0x1cb)](Window_MessageLog[_0x50f1bb(0x2c2)])&&this[_0x50f1bb(0x306)])return;this[_0x50f1bb(0x306)]=![];if(Input[_0x50f1bb(0x1cb)](_0x50f1bb(0x25e))){if(_0x50f1bb(0x273)!==_0x50f1bb(0x1ce))this[_0x50f1bb(0x210)](!![]);else return _0x2a33dc[_0x50f1bb(0x2fb)](this['_allTextHeight'],0x1);}else{if(Input[_0x50f1bb(0x1cb)]('up'))this['processSlowScroll'](![]);else{if(Input[_0x50f1bb(0x1cb)]('pagedown'))_0x50f1bb(0x1d2)===_0x50f1bb(0x1d2)?this['processFastScroll'](!![]):this[_0x50f1bb(0x1d8)](![]);else{if(Input[_0x50f1bb(0x1cb)]('pageup'))_0x50f1bb(0x1f9)===_0x50f1bb(0x1f9)?this[_0x50f1bb(0x2f5)](![]):(_0xb05e36[_0x50f1bb(0x1fa)]=_0x4fd9f6[_0x50f1bb(0x1fa)](),_0x6dc11['faceIndex']=_0x57d972['faceIndex']());else{if(Input[_0x50f1bb(0x259)](_0x50f1bb(0x299))){if('ZpAKQ'==='ZpAKQ')this[_0x50f1bb(0x2ba)](!![]);else{let _0xdc0d9f=this['origin']['y'];this[_0x50f1bb(0x24f)]['y']+=(_0x28f711?0x1:-0x1)*_0x3bd928[_0x50f1bb(0x2a8)];let _0xc7b26a=_0x5a4bc4[_0x50f1bb(0x2fb)](0x0,this[_0x50f1bb(0x26a)]-this[_0x50f1bb(0x2f1)]);this['origin']['y']=this['origin']['y'][_0x50f1bb(0x29f)](0x0,_0xc7b26a);if(_0xdc0d9f!==this[_0x50f1bb(0x24f)]['y']&&_0x200094[_0x50f1bb(0x20f)]%_0x24af8b[_0x50f1bb(0x252)]===0x0)this[_0x50f1bb(0x22c)]();}}else Input[_0x50f1bb(0x259)](_0x50f1bb(0x218))&&this['scrollToBottom'](!![]);}}}}},Window_MessageLog[_0x2d49ad(0x271)]['processSlowScroll']=function(_0xfc6e5a){const _0x2dcea1=_0x2d49ad;let _0x5f5794=this[_0x2dcea1(0x24f)]['y'];this[_0x2dcea1(0x24f)]['y']+=(_0xfc6e5a?0x1:-0x1)*Window_MessageLog[_0x2dcea1(0x30d)];let _0x1883fe=Math['max'](0x0,this[_0x2dcea1(0x26a)]-this[_0x2dcea1(0x2f1)]);this[_0x2dcea1(0x24f)]['y']=this[_0x2dcea1(0x24f)]['y']['clamp'](0x0,_0x1883fe);if(_0x5f5794!==this[_0x2dcea1(0x24f)]['y']&&Graphics[_0x2dcea1(0x20f)]%Window_MessageLog[_0x2dcea1(0x234)]===0x0)this['playCursorSound']();},Window_MessageLog['prototype']['processFastScroll']=function(_0x57da54){const _0x93d62b=_0x2d49ad;let _0xc27041=this[_0x93d62b(0x24f)]['y'];this[_0x93d62b(0x24f)]['y']+=(_0x57da54?0x1:-0x1)*Window_MessageLog[_0x93d62b(0x2a8)];let _0x501637=Math[_0x93d62b(0x2fb)](0x0,this[_0x93d62b(0x26a)]-this[_0x93d62b(0x2f1)]);this[_0x93d62b(0x24f)]['y']=this[_0x93d62b(0x24f)]['y'][_0x93d62b(0x29f)](0x0,_0x501637);if(_0xc27041!==this[_0x93d62b(0x24f)]['y']&&Graphics[_0x93d62b(0x20f)]%Window_MessageLog[_0x93d62b(0x252)]===0x0)this['playCursorSound']();},Window_MessageLog[_0x2d49ad(0x271)][_0x2d49ad(0x2ba)]=function(_0x41af52){const _0x31105d=_0x2d49ad;let _0x3e8273=this[_0x31105d(0x24f)]['y'];this[_0x31105d(0x24f)]['y']=0x0;if(_0x41af52&&_0x3e8273!==this[_0x31105d(0x24f)]['y'])this[_0x31105d(0x22c)]();},Window_MessageLog['prototype'][_0x2d49ad(0x1cc)]=function(_0x1e4957){const _0x462198=_0x2d49ad;let _0x394d3b=this[_0x462198(0x24f)]['y'],_0x1fc542=Math[_0x462198(0x2fb)](0x0,this[_0x462198(0x26a)]-this['innerHeight']);this[_0x462198(0x24f)]['y']=_0x1fc542;if(_0x1e4957&&_0x394d3b!==this[_0x462198(0x24f)]['y'])this[_0x462198(0x22c)]();},Window_MessageLog['prototype'][_0x2d49ad(0x23d)]=function(_0x2b8129,_0x103069){const _0x1bc40c=_0x2d49ad;this['origin']['y']+=_0x103069;let _0x275bad=Math[_0x1bc40c(0x2fb)](0x0,this[_0x1bc40c(0x26a)]-this[_0x1bc40c(0x2f1)]);this[_0x1bc40c(0x24f)]['y']=this['origin']['y']['clamp'](0x0,_0x275bad);},Window_MessageLog[_0x2d49ad(0x271)]['setScrollAccel']=function(_0x2b4440,_0x365f7b){const _0x2470d2=_0x2d49ad;this['origin']['y']+=_0x365f7b;let _0x15060b=Math[_0x2470d2(0x2fb)](0x0,this['_allTextHeight']-this['innerHeight']);this['origin']['y']=this[_0x2470d2(0x24f)]['y'][_0x2470d2(0x29f)](0x0,_0x15060b);};